function [all_in_vals_xy, all_out_vals_xy, mean_val_xy] = in_out_pts_cuts_prof( all_x_Prof, all_y_Prof, X_meanProf, Y_meanProf, girlsNames ,numOfCuts, num_of_samples) % X_meanProf, Y_meanProf, 
%UNTITLED Summary of this function goes here

dist_fun = @( x, y, ref_xy ) ((x - ref_xy(1))^2 + (y - ref_xy(2))^2)^0.5;

threshold_distance = 5;
all_in_vals_xy = cell(1,length(girlsNames));
all_out_vals_xy = cell(1,length(girlsNames));
mean_val_xy = cell(1,length(girlsNames));
numOfGirls = length(girlsNames);
for j=1:numOfGirls       
   %numberof vessels for each girl might be different
   N = length(all_x_Prof{j});
   all_in_vals_xy{j} = cell(1,N);
   all_out_vals_xy{j} = cell(1,N);
   mean_val_xy{j} = cell(1,N);
   
   for i=1:N
       all_in_vals_xy{j}{i} = zeros(2,num_of_samples);
       all_out_vals_xy{j}{i} = zeros(2,num_of_samples);
       mean_val_xy{j}{i} = zeros(2,num_of_samples);
       [max_val, max_idx] = max(all_y_Prof{j}{i});
       ref_point_xy = [ (X_meanProf{j}{i}(end) + X_meanProf{j}{i}(1))/2, (Y_meanProf{j}{i}(end) + Y_meanProf{j}{i}(1))/2 ];
       figure;
       plot(X_meanProf{j}{i}, Y_meanProf{j}{i}, 'g.'); hold on;
       plot(ref_point_xy(1), ref_point_xy(2), 'go');
       for smpl_idx = 1:num_of_samples
           in_xy = [all_x_Prof{j}{i}{1}(smpl_idx), all_y_Prof{j}{i}{1}(smpl_idx)];
           out_xy = [all_x_Prof{j}{i}{1}(smpl_idx), all_y_Prof{j}{i}{1}(smpl_idx)];
           mean_val_xy{j}{i} = [all_x_Prof{j}{i}{1}(smpl_idx), all_y_Prof{j}{i}{1}(smpl_idx)];
           max_dist = dist_fun(in_xy(1), in_xy(2), ref_point_xy);
           min_dist = max_dist;
           plot(all_x_Prof{j}{i}{1}, all_y_Prof{j}{i}{1}, 'color', [0.5,0.5,0.5]);
           plot(in_xy(1), in_xy(2), 'b.');
           cuts_to_count = 1;
           for cut_idx = 2:numOfCuts
               % inside = max distance among all that are: under or left untill max_s and under or right after max_s
               % outside = max distance among all that are: above or right untill max_s and above or left after max_s
               cur_x = all_x_Prof{j}{i}{cut_idx}(smpl_idx);
               cur_y = all_y_Prof{j}{i}{cut_idx}(smpl_idx);
               plot(all_x_Prof{j}{i}{cut_idx}, all_y_Prof{j}{i}{cut_idx}, 'color', [0.5,0.5,0.5]);
               plot(cur_x, cur_y, '.', 'color', [0.5,0.5,0.5]);
               cur_dist = dist_fun(cur_x, cur_y, ref_point_xy);
               dist_to_count = dist_fun(cur_x, cur_y, mean_val_xy{j}{i}/(cuts_to_count));
               if dist_to_count > threshold_distance
                   continue;
               end
               
               if(smpl_idx < max_idx)
                  % check inside: left or under
                  if (( cur_x < mean_val_xy{j}{i}(1)) || cur_y < mean_val_xy{j}{i}(2) )
                      in = true;
                  % check outside: right or above
                  else %
                      out = true;
                  end
               else
                  if ( cur_x > mean_val_xy{j}{i}(1) || cur_y < mean_val_xy{j}{i}(2) )
                      in = true;
                  % check outside: right or above
                  else %
                      out = true;
                  end
               end
               mean_val_xy{j}{i}(1) = mean_val_xy{j}{i}(1) + cur_x;
               mean_val_xy{j}{i}(2) = mean_val_xy{j}{i}(2) + cur_y;
               plot(cur_x, cur_y, 'b.');
               cuts_to_count = cuts_to_count + 1;
               
               if cur_dist < min_dist
                   in_xy = [cur_x, cur_y];
                   min_dist = cur_dist;
               end

               if cur_dist > max_dist
                   out_xy = [cur_x, cur_y];
                   max_dist = cur_dist;
               end
           end
           all_in_vals_xy{j}{i}(:,smpl_idx) = in_xy;
           all_out_vals_xy{j}{i}(:,smpl_idx) = out_xy;
           mean_val_xy{j}{i} = mean_val_xy{j}{i}./cuts_to_count;
           
           plot(in_xy(1), in_xy(2), 'c*');
           plot(out_xy(1), out_xy(2),'k*');
           plot(mean_val_xy{j}{i}(1), mean_val_xy{j}{i}(2), 'r*');
       end
       hold off;
   end
   end

   
