function [all_inner_hist, all_outer_hist, pt_inner, pt_outer] = calc_var_on_ys_all_obj( x_out,y_out, x_in,y_in, x_avg,y_avg, min_s,max_s, sample_num_y )
%   find statistics about the histogram of variance as a function of the height
%   sample_num_y: divide model horizontally to this number of samples
%   x_in, y_in, x_out, y_out: each is nested parameters {girl}{object}{num_of_cut}.
%     each cell contain a list of x or y values (of the profile of the current cut)
%   x_avg, y_avg: are nested parameters {girl}{object}, each contain a lists of the avarage profile.
%   min_s, max_s: are nested parameters {girl}{object}, each contain a list of values of the min\max heigt
%   I can use for each object (it is the common area between all the cut-profiles of the same object)
%   NOTE: All params are of the same object!!

    num_of_girls = length(x_out);
    
    all_inner_hist = cell(1,num_of_girls);
    all_outer_hist = cell(1,num_of_girls);
    pt_inner = cell(1,num_of_girls);
    pt_outer = cell(1,num_of_girls);
    
    for j = 1:num_of_girls
        N = length(x_out{j});
        all_inner_hist{j} = cell(1,N);
        all_outer_hist{j} = cell(1,N);
        pt_inner{j} = cell(1,N);
        pt_outer{j} = cell(1,N);
        for i = 1:N
            [in_var, out_var, pt_in, pt_out] = calc_var_on_ys(x_out{j}{i},y_out{j}{i}, x_in{j}{i},y_in{j}{i}, x_avg{j}{i},y_avg{j}{i}, min_s{j}{i}, max_s{j}{i}, sample_num_y);
            all_inner_hist{j}{i} = in_var;
            all_outer_hist{j}{i} = out_var;
            pt_inner{j}{i} = pt_in;
            pt_outer{j}{i} = pt_out;
        end   
    end
end