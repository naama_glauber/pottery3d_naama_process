function V=Uzy_transformation(v,phi,teta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should rotate an object which is defind be 3D points (v) by
% angle teta around x-axis and angle phi around z-axis. teta should belong to
% [0 pi]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D=[cos(phi) -sin(phi) 0;sin(phi) cos(phi) 0;0 0 1];
C=[1 0 0;0 cos(teta) sin(teta);0 -sin(teta) cos(teta)];
B=[cos(phi) sin(phi) 0;-sin(phi) cos(phi) 0;0 0 1];
A=B*C*D;
V=[A*v']';