%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%           LOAD SPECIFIC .MAT FILE & CREATE MEAN PROFILE             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('I:\labs\ng6767535\MatLab-M-Filles\matPottersFiles\Adi\AF 13.mat')
j=2;
i=13;

V_lst{j}{i} = V;
f_lst{j}{i} = f;
n_lst{j}{i} = N;
skn_lst{j}{i} = skn;

z = V_lst{j}{i}(:,3); y = V_lst{j}{i}(:,2); x = V_lst{j}{i}(:,1);
smpl_dist = find_sampling_distance(x,y,z,f_lst{j}{i});
[V_fixed,n_fixed] = self_alignment_sections(V_lst{j}{i},f_lst{j}{i},n_lst{j}{i}, skn_lst{j}{i}, smpl_dist);
[X, Y,~] = arrange_mean_profile13(V_fixed, n_fixed, 0.15, 0.6);
mean_xy_prof_no_trim{j}{i} = zeros(length(X),2);
mean_xy_prof_no_trim{j}{i}(:,1) = X;
mean_xy_prof_no_trim{j}{i}(:,2) = Y;

figure;
plot(mean_xy_prof_no_trim{j}{i}(:,1), mean_xy_prof_no_trim{j}{i}(:,2), 'r', mean_xy_prof_no_trim{j}{i}(:,1), mean_xy_prof_no_trim{j}{i}(:,2), 'r.');% hold on;

[newV_cells, newVidx_cells] = cakeCutModelAftAlign(V_fixed, numOfCuts);
%all_x_Prof{j}{i} = cell(1,numOfCuts);
%all_y_Prof{j}{i} = cell(1,numOfCuts);
for cutIdx = 1:numOfCuts         
   V_lst_cut    = V_fixed(newVidx_cells{cutIdx},:);
   n_lst_cut    = n_fixed{j}{i}(newVidx_cells{cutIdx},:);
   [all_x_Prof{j}{i}{cutIdx}, all_y_Prof{j}{i}{cutIdx}, ~] = arrange_mean_profile13(V_lst_cut, n_lst_cut, 0.15, 0.6);
   all_xy_prof_no_trim{j}{i}{cutIdx} = zeros(length(all_x_Prof{j}{i}{cutIdx}),2);
   all_xy_prof_no_trim{j}{i}{cutIdx}(:,1) = all_x_Prof{j}{i}{cutIdx};
   all_xy_prof_no_trim{j}{i}{cutIdx}(:,2) = all_y_Prof{j}{i}{cutIdx};
   %plot(all_xy_prof_no_trim{j}{i}{cutIdx}(:,1), all_xy_prof_no_trim{j}{i}{cutIdx}(:,2), 'b'); hold on;
end
clear ('skn', 'f', 'V', 'X','Y', 'x', 'y', 'z', 'VC', 'HL', 'V_fixed', 'n_fixed', 'V_lst_cut', 'n_lst_cut', 'Material', 'DefiniteDiameter', 'datasource', 'ext_prof', 'XaxisSet', 'selectquery', 'newVidx_cells', 'newV_cells', 'files_path');

[mean_xy_prof, all_xy_prof, ~, ~] = cut_prof_buttom_by_min_height(mean_xy_prof_no_trim, all_xy_prof_no_trim, numOfGirls, numOfCuts);

sample_num_y = 100;
[xy_out, xy_in, all_max_s, min_s, max_s, min_avg_s, max_avg_s, max_mean_s_idx, max_all_s_idx, diff_in_out] = in_out_prof( all_xy_prof, mean_xy_prof, sample_num_y );

%%% 3. resampling the curve for unify vectors' length
num_of_samples = 100;
num_of_vectors = 0;
[smpl_all_xy{j}{i}, smpl_xy_mean_prof{j}{i}, mean_vars{j}{i}] = resample_curve_calc_variance( mean_xy_prof{j}{i}, all_xy_prof{j}{i},  max_mean_s_idx{j}{i}, max_all_s_idx{j}{i}, num_of_samples );



[all_in_vals_xy, all_out_vals_xy, ~] = in_out_pts_cuts_prof2( smpl_all_xy, smpl_xy_mean_prof , girlsNames ,numOfCuts, num_of_samples);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                          RUN REFERENCE (after Load)                 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z=V_ref(:,3);y=V_ref(:,2);x=V_ref(:,1);
smpl_dist = find_sampling_distance(x,y,z,f_ref);
[V_fixed,n_fixed] = self_alignment_sections(V_ref,f_ref,n_ref, skn_ref, smpl_dist);
%%% try instead of "self_alignment_sections"
%[V_fixed, n_fixed, X_meanProf{j}{i}, Y_meanProf{j}{i}] = simplyfied_self_alignment(V_lst{j}{i},f_lst{j}{i},n_lst{j}{i}, skn_lst{j}{i});

%%% 2. find the mean profile after alignment            %%%
[X, Y,~] = arrange_mean_profile13(V_fixed, n_fixed, 0.15, 0.6);
mean_xy_prof_ref = zeros(length(X),2);
mean_xy_prof_ref(:,1) = X;
mean_xy_prof_ref(:,2) = Y;

[newV_cells, newVidx_cells] = cakeCutModelAftAlign(V_ref, numOfCuts);
       
%%% 4. find the mean profile of each cut
all_xy_prof_ref = cell(1,numOfCuts);
for cutIdx = 1:numOfCuts         
   V_lst_cut    = V_ref(newVidx_cells{cutIdx},:);
   n_lst_cut    = n_ref(newVidx_cells{cutIdx},:);
   [Xs, Ys, ~] = arrange_mean_profile13(V_lst_cut, n_lst_cut, 0.15, 1.0);
   all_xy_prof_ref{cutIdx} = zeros(length(Xs),2);
   all_xy_prof_ref{cutIdx}(:,1) = Xs;
   all_xy_prof_ref{cutIdx}(:,2) = Ys;
end
clear ('skn', 'f', 'V', 'X','Y', 'x', 'y', 'z', 'VC', 'HL', 'V_fixed', 'n_fixed', 'V_lst_cut', 'n_lst_cut', 'Material', 'DefiniteDiameter', 'datasource', 'ext_prof', 'XaxisSet', 'selectquery', 'newVidx_cells', 'newV_cells', 'files_path');
