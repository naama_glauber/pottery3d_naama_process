% save all profiles for each potter in files
% all profiles are after icp!!

% create folder for each potter:
potter_filenames = cell(2,1);
potter_filenames{1} = cell(9,1);
potter_filenames{1} = {'Lashman', 'Jera-Ram', 'Mangilal', 'Pemaram', 'Devilal', 'Govindram', 'Hajiram', 'Udaram', 'Zorilal'};
potter_filenames{2} = cell(14,1);
potter_filenames{2} = {'Anwar-Khan', 'Ramzan', 'Sakur', 'Sokat', 'Azruddin', 'Ramdayal', 'Aksar', 'Razak', 'Samir', 'Youssouf', 'Fazrudin', 'Mazid', 'Amin', 'Gani'};

for p_i=1:length(potters_names)
    dir_path = strcat('./Mean profile/no_icp_profs4rhino_groups/', potters_names{p_i});
    mkdir(dir_path);
    
    % go over the resulted profiles: trm_icp_smpl_prof
    for f_i=1:length(all_profs{p_i})
        %if f_i < 10
        %    str_num = sprintf('%02d',f_i);
        %else
        %    str_num = num2str(f_i);
        %end
        %filename = strcat(dir_path, '/', potter_name, '_', str_num, '.txt');
        filename = strcat(dir_path, '/', potter_filenames{p_i}{f_i}, '.txt');
        fileID = fopen(filename,'w');
        
        pts = all_profs{p_i}{f_i};
        for pt_idx=1:length(pts)
           str_pt = strcat(num2str(pts(pt_idx,1)), ',', num2str(pts(pt_idx,2)), '\n');
           fprintf(fileID, str_pt);
        end
        nb = num2str(trm_noicp_NB_pts{p_i}{f_i}(2));
        rn = num2str(trm_noicp_RN_pts{p_i}{f_i}(2));

        fprintf(fileID, strcat(nb, ',', rn)); 
        fclose(fileID);
    end
end
    
