function [mean_distance] = find_sampling_distance(x, y, z, f)
    ctr=0;
    for ii=100:100:length(f)
        ctr=ctr+1;
        p1 = f(ii,1); p2 = f(ii,2); p3 = f(ii,3);
        D(ctr,1)=sqrt((x(p1)-x(p2))^2+(y(p1)-y(p2))^2+(z(p1)-z(p2))^2);
        D(ctr,2)=sqrt((x(p3)-x(p2))^2+(y(p3)-y(p2))^2+(z(p3)-z(p2))^2);
        D(ctr,3)=sqrt((x(p1)-x(p3))^2+(y(p1)-y(p3))^2+(z(p1)-z(p3))^2);
    end
    mean_distance=mean(mean(D));
    clear('ctr', 'D', 'p1', 'p2', 'p3', 'ii');
end