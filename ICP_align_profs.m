function [all_Dicp, all_Ricp, all_Ticp, all_ERs] = ICP_align_profs(all_profs, potters_obj_names, potters_groups, is_ploting, dir_path, ref_profiles)
    all_Dicp = cell(size(all_profs)); % aligned profiles
    all_Ricp = cell(size(all_profs)); % rotate matrix
    all_Ticp = cell(size(all_profs)); % translate matrix
    all_ERs  = cell(size(all_profs)); % Error of alignment, Er(0) = initialize error
    for j=1:length(all_profs)
        if ~exist('ref_profiles', 'var')
            ref_prof = all_profs{j}{1};
        else
            ref_prof = ref_profiles{j};
        end
        n_pts_ref = size(ref_prof,1);
        M = zeros(3, n_pts_ref);
        M(1,:) = ref_prof(:,1);
        M(2,:) = ref_prof(:,2);

        n_objects = length(all_profs{j});
        all_Dicp{j} = cell(1, n_objects);
        all_Ricp{j} = cell(1, n_objects);
        all_Ticp{j} = cell(1, n_objects);
        all_ERs{j} = cell(1, n_objects);
        
        %all_Dicp{j}{1} = all_profs{j}{1};
        for i=1:n_objects
            prof_i = [all_profs{j}{i}(:,1), all_profs{j}{i}(:,2)];
            D = zeros(3, length(prof_i));
            D(1,:) = prof_i(:,1);
            D(2,:) = prof_i(:,2);

            [Ricp Ticp ER t] = icp_no_rotation(M, D, 15); % icp
            % Transform data-matrix using ICP result
            n = size(D,2);
            Dicp = Ricp * (D + repmat(Ticp, 1, n));
            all_Dicp{j}{i}(:,1) = Dicp(1,:)';
            all_Dicp{j}{i}(:,2) = Dicp(2,:)';
            all_Ricp{j}{i} = Ricp;
            all_Ticp{j}{i} = Ticp;
            all_ERs{j}{i} = ER;
            if is_ploting
                % Plot model points blue and transformed points red
                prof_fig = figure;
                name = strcat('icp-',potters_obj_names{j}{i});
                
                %subplot(2,2,1);
                subplot(1,2,1);
                plot(M(1,:),M(2,:),'b.',D(1,:),D(2,:),'r.');
                axis equal;
                xlabel('R:orig-1,  B:orig-2'); % ylabel('y');
                title(name);
                
                % 5. Mark the "curvature place in this plot, after alignment!
                %subplot(2,2,2);
                subplot(1,2,2);
                plot(M(1,:),M(2,:),'b.',Dicp(1,:),Dicp(2,:),'r.');
                axis equal;
                xlabel('R:Orig-1,  B:ICP-2'); %ylabel('y');

                % 6. save all plots: different dirs for different potters groups(j)
                fig_path = strcat( dir_path, potters_groups{j} ,'\_TRIMMED\RNB_aligned_',potters_obj_names{j}{i},'.jpg');
                saveas(prof_fig,fig_path);
                close(prof_fig)
            end
        end
    end

end

