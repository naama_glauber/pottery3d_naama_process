function [x_no_bottom, y_no_bottom] = cut_bottom_from_min(all_x_Prof, all_y_Prof, global_min, numOfGirls, numOfCuts, is_3DModel)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
x_no_bottom = cell(1,numOfGirls);
y_no_bottom = cell(1,numOfGirls);
for j =1:numOfGirls
    N = length(all_x_Prof{j});
    x_no_bottom{j} = cell(1,N);
    y_no_bottom{j} = cell(1,N);
    for i = 1:N
        if is_3DModel
            x_no_bottom{j}{i} = cell(1,numOfCuts);
            y_no_bottom{j}{i} = cell(1,numOfCuts);
            for k = 1:numOfCuts
                x_s = all_x_Prof{j}{i}{k};
                y_s = all_y_Prof{j}{i}{k};
                for idx = 1:length(y_s)
                    y = y_s(idx);
                    if y > global_min
                        x_no_bottom{j}{i}{k} = [x_no_bottom{j}{i}{k}, x_s(idx)];
                        y_no_bottom{j}{i}{k} = [y_no_bottom{j}{i}{k}, y_s(idx)];
                    end
                end
            end
        else
            x_s = all_x_Prof{j}{i};
            y_s = all_y_Prof{j}{i};
            for idx = 1:length(y_s)
                y = y_s(idx);
                if y > global_min
                    x_no_bottom{j}{i} = [x_no_bottom{j}{i}, x_s(idx)];
                    y_no_bottom{j}{i} = [y_no_bottom{j}{i}, y_s(idx)];
                end
            end
        end
    end
end
end

