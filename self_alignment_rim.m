function [new_v,new_n]=self_alignment_rim(v,f,n,skn,mean_distance)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should find the best alignment of a shard using self
% improve method, in which the computer knows what should be the next
% iteration. It is based on Uzy's formulas that compute the distortion of
% every horizontal section and find the angles of the transformation.
% This version should include improvement that based only on the rim!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the neighbors of each point, and the triangles that meat at each
% point
if ~isempty(n)
    if isempty(skn)
        skn(length(v)+1).skn=0;    
        for i=1:length(f)
            skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
            skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
            skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);  
        end
        skn(end)=[];
        for i=1:length(skn)
            skn(i).skn=unique(skn(i).skn);
        end
    end
else
    n=zeros(size(v));
    w=zeros(length(v),1);
    [cog,normals]=calc_normals(v,f);
    skn(length(v)+1).skn=0;
    for i=1:length(f)
        skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
        skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
        skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);
        n(f(i,1),:)=(w(f(i,1))*n(f(i,1),:)+normals(i,:))/(w(f(i,1))+1);
        n(f(i,2),:)=(w(f(i,2))*n(f(i,2),:)+normals(i,:))/(w(f(i,2))+1);
        n(f(i,3),:)=(w(f(i,3))*n(f(i,3),:)+normals(i,:))/(w(f(i,3))+1);
        w(f(i,1))=w(f(i,1))+1;
        w(f(i,2))=w(f(i,2))+1;
        w(f(i,3))=w(f(i,3))+1;
    end
    skn(end)=[];
    for i=1:length(skn)
        skn(i).skn=unique(skn(i).skn);
        n(i,:)=n(i,:)/norm(n(i,:));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the mean distance between points.
if isempty(mean_distance)
    z=v(:,3);y=v(:,2);x=v(:,1);
    ctr=0;
    for i=100:100:length(f)
        ctr=ctr+1;
        p1=f(i,1);p2=f(i,2);p3=f(i,3);
        D(ctr,1)=sqrt((x(p1)-x(p2))^2+(y(p1)-y(p2))^2+(z(p1)-z(p2))^2);
        D(ctr,2)=sqrt((x(p3)-x(p2))^2+(y(p3)-y(p2))^2+(z(p3)-z(p2))^2);
        D(ctr,3)=sqrt((x(p1)-x(p3))^2+(y(p1)-y(p3))^2+(z(p1)-z(p3))^2);
    end
    mean_distance=mean(mean(D));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% subplot(1,2,1);t=draw_profiles(v,f,n);title('Original position');
% disp('-----------------------------------------')
% disp('Preparation:');
% [Pa Pt PA]=profiles_area_intersection(v,f,n);
% disp(['Mean of profiles intersection in original position: ' num2str(Pa)]);PPa(1)=Pa;
toc
% disp('-----------------------------------------')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[XP,YP,QP]=arrange_mean_profile13(v,n,0.15,1);
CTR=0;last_Q=mean(QP);
if isnan(last_Q)
    last_Q=1000;
end
FLEG=0;
v_record=v;n_record=n;
while FLEG<2
    clear('FS1','FS2','DFS1','DFS2','sections');
    sections=[];piece_size=1;
    CTR=CTR+1;
    z=v(:,3);y=v(:,2);x=v(:,1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if CTR==0
        tetasec=atan2(v(:,2),v(:,1));
        if range(tetasec)>pi
            sections=compute_sections_3(v,f,n,skn,mean_distance);
        else
            sections=compute_sections_1(v,f,n,skn,mean_distance);
        end
        teta=[sections.range_teta];
        I=find(teta<prctile(teta,20));
        sections(I)=[];
        x0=mean([sections.x0]);
        y0=mean([sections.y0]);
        r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
        I=find(r0>mean(r0));
        sections(I)=[];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Computing the tranformation parameters
        if length(sections)>0
            p=polyfit([sections.x0],[sections.y0],1);
            phi=acot(-p(1));
            if sin(phi)*cos(phi)>0
                phi=phi+pi;
            end
            bx=-sin(phi)*p(2);
            if sum(gradient([sections.x0]))>0
                phi=phi+pi;
            end
            side1=find([sections.type]==1);
            side2=find([sections.type]==2);
            F1=[sections(side1).f];
            F2=[sections(side2).f];
        else
            side1=[];
            side2=[];
        end
        %%%%%%%%%%%%%%%%%%
        % smooth F
        if length(side1)>5 & length(side2)>5
            piece_size=1;
            FS1(1)=mean(F1(1:3));
            FS1(2)=mean(F1(1:4));
            FS1(length(F1))=mean(F1(end-2:end));
            FS1(end-1)=mean(F1(end-3:end));
            for i=3:length(F1)-2
                FS1(i)=mean(F1(i-2:i+2));
            end
            FS2(1)=mean(F2(1:3));FS2(2)=mean(F2(1:4));
            FS2(length(F2))=mean(F2(end-2:end));
            FS2(end-1)=mean(F2(end-3:end));
            for i=3:length(F2)-2
                FS2(i)=mean(F2(i-2:i+2));
            end
            F1=FS1;F2=FS2;
            %%%%%%%%%%%%%%%%%
            Z1=[sections(side1).z0];
            Z2=[sections(side2).z0];
            DF1=gradient(F1)./gradient(Z1);
            DF2=gradient(F2)./gradient(Z2);
            %%%%%%%%%%%%%%%%%%
            % smooth DF
            DFS1(1)=mean(DF1(1:3));DFS1(2)=mean(DF1(1:4));
            DFS1(length(DF1))=mean(DF1(end-2:end));
            DFS1(end-1)=mean(DF1(end-3:end));
            for i=3:length(DF1)-2
                DFS1(i)=mean(DF1(i-2:i+2));
            end
            DFS2(1)=mean(DF2(1:3));DFS2(2)=mean(DF2(1:4));
            DFS2(length(DF2))=mean(DF2(end-2:end));
            DFS2(end-1)=mean(DF2(end-3:end));
            for i=3:length(DF2)-2
                DFS2(i)=mean(DF2(i-2:i+2));
            end
            %%%%%%%%%%%%%%%%%
            A1=[ones(length(side1),1) -[Z1+F1.*DFS1]'];
            A2=[ones(length(side2),1) -[Z2+F2.*DFS2]'];
            B1=[sections(side1).x0]*sin(phi)-[sections(side1).y0]*cos(phi);
            B2=[sections(side2).x0]*sin(phi)-[sections(side2).y0]*cos(phi);
            X1=-real(A1\B1');
            if sign(X1(1))==sign(mean([sections(side1).x0]))
                X1(1)=-X1(1);
            end
            X2=-real(A2\B2');
            if sign(X2(1))==sign(mean([sections(side2).x0]))
                X2(1)=-X2(1);
            end
            %disp([X1 X2]);
            X=(X1+X2)/2;
%             disp(['Iteration no. ' num2str(CTR) ':']);
%             disp(['calculated phi = ' num2str(phi/pi) ' pi']);
%             disp(['calculated teta = ' num2str(X(2)/pi) ' pi']);
%             disp(['by = ' num2str(bx) '  --  bx = ' num2str(X(1))]);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Rotate 
            MM=v;
            MM(:,2)=MM(:,2)-bx;
            MM(:,1)=MM(:,1)+X(1);
            M3=[cos(phi) sin(phi) 0;-sin(phi) cos(phi) 0;0 0 1];
            M2=[1 0 0;0 cos(X(2)) sin(X(2));0 -sin(X(2)) cos(X(2))];
            M1=[cos(phi) -sin(phi) 0;sin(phi) cos(phi) 0;0 0 1];
            MM=M1*M2*M3*MM';
            NN=M1*M2*M3*n';
            v=MM';
            n=NN';
            [XP,YP,QP]=arrange_mean_profile13(v,n,0.15,1);
            improvement_ratio=1-mean(QP)/last_Q;
%             disp(['current Q is: ' num2str(mean(QP)) '   improvement ratio is: ' num2str(improvement_ratio)]);
%             disp('-----------------------------------------')
            last_Q=mean(QP);
            if improvement_ratio>0
                v_record=v;n_record=n;
            end
        else
            piece_size=0;
            FLEG=1;
        end
    else
        CTR=CTR-1;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Computations of phi and teta based on the rim
    if piece_size==1
        par=pi/100;
        CTR=CTR+1;
%         disp(['rim itteration no. ' num2str(CTR) ':']);
        %%%%%%%%%%%%%%%%%%
        v2=v;
        I=find(v2(:,3)>prctile(v2(:,3),90));
        alpha=atan2(v2(I,2),v2(I,1));
        p=-pi:par:pi-par;
        II=hist(alpha,p);
        p(find(II<5))=[];
        t=length(p);ctr2=0;clear('rim');
        for i=1:t
            N=cross([0,0,1],[cos(p(i)),sin(p(i)),0]);
            a=N(1);b=N(2);c=N(3);
            d=abs((a*v2(:,1)+b*v2(:,2)+c*v2(:,3)));
            I=find(d<mean_distance/4);
            if length(I)>10
                [z1 z2]=max(v2(I,3));
                ctr2=ctr2+1;
                rim(ctr2,:)=v2(I(z2),:);
            end
        end
        %%%%%%%%%%%%%%%%%%
%         v2=v;
%         alpha=atan2(v2(:,2),v2(:,1));
%         p=min(alpha):pi/100:max(alpha);
%         t=length(p);ctr2=0;clear('rim');
%         for i=1:t
%             N=cross([0,0,1],[cos(p(i)),sin(p(i)),0]);
%             a=N(1);b=N(2);c=N(3);
%             d=abs((a*v2(:,1)+b*v2(:,2)+c*v2(:,3)));
%             I=find(d<mean_distance/4);
%             if length(I)>10
%                 [z1 z2]=max(v2(I,3));
%                 ctr2=ctr2+1;
%                 rim(ctr2,:)=v2(I(z2),:);
%             end
%         end
%         I=find(rim(:,3)>prctile(rim(:,3),10));
        I=find(rim(:,3)>prctile(rim(:,3),10) & rim(:,3)<prctile(rim(:,3),100));
        rim2=rim(I,:);
        AA(1,1)=sum((rim2(:,1)-mean(rim2(:,1))).^2);
        AA(1,2)=-sum((rim2(:,1)-mean(rim2(:,1))).*(rim2(:,2)-mean(rim2(:,2))));
        AA(2,1)=AA(1,2);
        AA(2,2)=sum((rim2(:,2)-mean(rim2(:,2))).^2);
        BB(1,1)=sum((rim2(:,1)-mean(rim2(:,1))).*(rim2(:,3)-mean(rim2(:,3))));
        BB(2,1)=-sum((rim2(:,2)-mean(rim2(:,2))).*(rim2(:,3)-mean(rim2(:,3))));
        SvdA=svd(AA);
        if ~isempty(find(abs(SvdA)<0.0001))
            XX=[1,1];
        else
            XX=AA\BB;
        end
        if ~isnan(XX(1))
            phi_rim=atan(XX(1)/XX(2));
            teta_rim=XX(1)/sin(phi_rim);
            if teta_rim<0
                teta_rim=-teta_rim;
                phi_rim=phi_rim+pi;
            end
%             disp(['phi_rim = ' num2str(phi_rim/pi) ' pi.    :     teta_rim = ' num2str(-teta_rim/pi) ' pi.']);
        else
            teta_rim=0;
            phi_rim=0;
%             disp('phi_rim and teta_rim could not be computed')
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Rotate 
        phi=phi_rim;teta=-teta_rim;
        MM=v2;
        M3=[cos(phi) sin(phi) 0;-sin(phi) cos(phi) 0;0 0 1];
        M2=[1 0 0;0 cos(teta) sin(teta);0 -sin(teta) cos(teta)];
        M1=[cos(phi) -sin(phi) 0;sin(phi) cos(phi) 0;0 0 1];
        MM=M1*M2*M3*MM';
        NN=M1*M2*M3*n';
        v=MM';
        n=NN';
        z=v(:,3);x=v(:,1);y=v(:,2);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        tetasec=atan2(v(:,2),v(:,1));
        if range(tetasec)>pi
            sections=compute_sections_3(v,f,n,skn,mean_distance);
        else
            sections=compute_sections_1(v,f,n,skn,mean_distance);
        end
        teta2=[sections.range_teta];
        I=find(teta2<prctile(teta2,20));
        sections(I)=[];
        x0=mean([sections.x0]);
        y0=mean([sections.y0]);
        r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
        I=find(r0>mean(r0));
        sections(I)=[];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if length(sections)>10
            x_move=mean([sections.x0]);
            y_move=mean([sections.y0]);
            v(:,1)=v(:,1)-x_move;
            v(:,2)=v(:,2)-y_move;
            for i=1:length(sections)
                [sections(i).x]=[sections(i).x]-x_move;
                [sections(i).x0]=[sections(i).x0]-x_move;
                [sections(i).y]=[sections(i).y]-y_move;
                [sections(i).y0]=[sections(i).y0]-y_move;
            end
%             disp(['x movement = ' num2str(x_move) '   :    y movement = ' num2str(y_move)]);
        end
        [XP,YP,QP]=arrange_mean_profile13(v,n,0.15,1);
        improvement_ratio=1-mean(QP)/last_Q;
%         disp(['current Q is: ' num2str(mean(QP)) '   improvement ratio is: ' num2str(improvement_ratio)]);
%         disp('-----------------------------------------')
        
        if improvement_ratio<0.01
            FLEG=FLEG+1;
%             disp(['End of program, number of iteration was ' num2str(CTR)]);
%             disp(['Q convegenes'])
        end
        if improvement_ratio>0
            v_record=v;n_record=n;
            last_Q=mean(QP);
        end
%         disp('-----------------------------------------')
        if abs(teta/pi)<0.00005
            FLEG=2;
%             disp(['End of program, number of iteration was ' num2str(CTR) '. teta<0.00005 pi']);
        end
        if CTR==4
            FLEG=2;
%             disp(['End of program, number of iteration is ' num2str(CTR) '. teta = ' num2str(teta/pi) ' pi']);
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if piece_size==1
    new_v=v_record;
    new_n=n_record;
%     disp('-----------------------------------------------------------------------------------------');
%     disp('Total ');toc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Rotating the fragment to be on the positive x-axis
    M=mean(new_v);
    teta=atan2(M(2),M(1));
    v1=cos(teta)*new_v(:,1)+sin(teta)*new_v(:,2);
    v2=-sin(teta)*new_v(:,1)+cos(teta)*new_v(:,2);
    v3=new_v(:,3)-M(3);
    new_v=[v1 v2 v3];
    v1=cos(teta)*new_n(:,1)+sin(teta)*new_n(:,2);
    v2=-sin(teta)*new_n(:,1)+cos(teta)*new_n(:,2);
    new_n=[v1 v2 new_n(:,3)];
    clear('v1','M','v2','teta')
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % An option to plot the horizontal sections and their centers
%     show_vrml(new_v,f);
%     for i=1:length(sections)
%         plot3(sections(i).x,sections(i).y,ones(1,length(sections(i).x))*sections(i).z0,'.');
%     end
%     plot3([sections.x0],[sections.y0],[sections.z0],'ro');
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     figure
%     profiles=new_draw_profiles2(new_v,f,new_n);
%     title('Final alignment by the rim')
else
    new_v=[];
    new_n=[];
    sections=[];
    profiles=[];
end