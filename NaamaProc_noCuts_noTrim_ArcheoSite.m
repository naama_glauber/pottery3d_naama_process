% Naama_procedure_Manager Script (main procedure)

% local .mat files with the 3D scans of the potshreds
% files_path = 'I:\labs\ng6767535\MatLab-M-Filles\matPottersFiles\';
% girlsNames = {'Adi', 'Casey', 'Eliya', 'Grace', 'Tama'};

% Archeological sites
files_path = 'I:\labs\ng6767535\MatLab-M-Filles\ArcheologicalSites\mat files\';
girlsNames = {'Rehov', 'RZ'};

% extract data from .mat file's
% TEMP: t_lst - FOR NOW IS EMPTY!
[V_lst, f_lst, skn_lst, n_lst, ~, all_file_names] = readFilesToDataStructures(files_path, girlsNames);

% according to the experiment, here we have 5 different potters.
numOfGirls = length(girlsNames);
% init
all_xy_prof_no_trim = cell(1, numOfGirls);
mean_xy_prof_no_trim = cell(1, numOfGirls);

for j=1:numOfGirls       
   %numberof vessels for each girl might be different
   N = length(V_lst{j});
   all_xy_prof_no_trim{j} = cell(1,N);
   mean_xy_prof_no_trim{j} = cell(1,N);
   for i=1:N
       %%% TO CHECK: if this is a correct way or we need another procedure. %%%
       %%% 1. aligning the model according to axis of symetry  %%%
       z=V_lst{j}{i}(:,3);y=V_lst{j}{i}(:,2);x=V_lst{j}{i}(:,1);
       smpl_dist = find_sampling_distance(x,y,z,f_lst{j}{i});
       [V_fixed,n_fixed] = self_alignment_sections(V_lst{j}{i},f_lst{j}{i},n_lst{j}{i}, skn_lst{j}{i}, smpl_dist);
       
       %%% 2. find the mean profile after alignment            %%%
       [X, Y,~] = arrange_mean_profile13(V_fixed, n_fixed, 0.15, 1.0);
       mean_xy_prof_no_trim{j}{i} = zeros(length(X),2);
       mean_xy_prof_no_trim{j}{i}(:,1) = X;
       mean_xy_prof_no_trim{j}{i}(:,2) = Y;
       
       figure; plot(X,Y, 'k', X, Y, 'b.');
       str = all_file_names{j}(i);
       str_new = strrep(str,'_','-');
       title(strcat('prof name: ',str_new));
       %saveas(gcf,['Prof-', str_new, '.png']);
       
       V_lst{j}{i} = V_fixed;
       n_lst{j}{i} = n_fixed;
   end
end
clear ('skn', 'f', 'V', 'X','Y', 'x', 'y', 'z', 'VC', 'HL', 'V_fixed', 'n_fixed', 'V_lst_cut', 'n_lst_cut', 'Material', 'DefiniteDiameter', 'datasource', 'ext_prof', 'XaxisSet', 'selectquery', 'newVidx_cells', 'newV_cells', 'files_path');

for j=1:numOfGirls       
   %numberof vessels for each girl might be different
   N = length(V_lst{j});
   for i=1:N
       X = mean_xy_prof_no_trim{j}{i}(:,1);
       Y = mean_xy_prof_no_trim{j}{i}(:,2);
       prof_fig = figure; plot(X,Y, 'k', X, Y, 'b.');
       str = all_file_names{j}(i);
       str_new = strrep(str,'_','-');
       title(strcat('prof name: ',str_new));
       saveas(gcf,strcat('Prof-', char(str_new), '.png'));
       close(prof_fig)
   end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 3. resampling the curve for unify vectors' length                          %%%
%%% 4. plot the mean variance over all the cuts, comparing to the mean profile %%%

prof_write_path = 'I:\labs\ng6767535\MatLab-M-Filles\Naama_Procedure\results\write_profs_4rhino\write_prof_archeo_sites\';
write_profiles_to_file(prof_write_path, girlsNames, mean_xy_prof_ref_align , 'mean_trim_refalign');


