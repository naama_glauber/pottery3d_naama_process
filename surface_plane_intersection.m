function [x,y,z,n]=surface_plane_intersection(v,f,z0,delta,skn,Normals)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should calculate the exact points of the intersection
% between a surface in the space and a given horizontal plane.
% The height of the plane is given by z0.
% In order to save time we first look for the nearest points to the plane
% and then look what triangles does this plane cut which goes out of these
% points.
% delta is the minimum value that we start with.
% skn is a structure with the field 'skn' in which the neighbors to every point
% are listed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dist_from_plane=z0-v(:,3);
near_below=find(dist_from_plane<delta & dist_from_plane>0);
ctr=1;
while isempty(near_below)
    ctr=ctr+1;
    delta=ctr*delta;
    dist_from_plane=z0-v(:,3);
    near_below=find(dist_from_plane<delta & dist_from_plane>0);
end
ctr=0;
for i=1:length(near_below)
    local_neighbors=skn(near_below(i)).skn;
    II=local_neighbors(find(dist_from_plane(local_neighbors)<0));
    if ~isempty(II)
        for j=1:length(II)
            p=dist_from_plane(near_below(i))*v(II(j),:)-dist_from_plane(II(j))*v(near_below(i),:);
            p=p/(dist_from_plane(near_below(i))-dist_from_plane(II(j)));
            nn=dist_from_plane(near_below(i))*Normals(II(j),:)-dist_from_plane(II(j))*Normals(near_below(i),:);
            nn=nn/(dist_from_plane(near_below(i))-dist_from_plane(II(j)));
            ctr=ctr+1;
            x(ctr)=p(1);
            y(ctr)=p(2);
            z(ctr)=p(3);
            n(ctr,1)=nn(1);
            n(ctr,2)=nn(2);
            n(ctr,3)=nn(3);
        end
    end
end
% show_vrml(v,f);
% plot3(x,y,z,'r.')
% figure(2);
% plot(x,y,'.',v(near_below,1),v(near_below,2),'r.');axis equal