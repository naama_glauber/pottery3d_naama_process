function write_prof_2file_inc_cuts( file_write_prof, girlsNames, prof_xs, prof_ys, numOfCuts )
% writing mean profiles (complete object, no cuts..)
% for each girl- different file. in each file, while each line contain: x , y
% and '***\n' saperate one object fron the next one.
numOfGirls = length(girlsNames);
for j = 1:numOfGirls
    fileID = fopen(strcat(file_write_prof, girlsNames{j}, '\' ,'prof_with_cuts.txt'),'w');
    N = length(prof_xs{j});
    for i = 1:N
        for cut_idx = 1:numOfCuts
            xs = prof_xs{j}{i}{cut_idx};
            ys = prof_ys{j}{i}{cut_idx};
            for idx = 1:length(xs)
                fprintf(fileID, strcat(num2str(xs(idx)), ',', num2str(ys(idx)), '\n'));
            end
        fprintf(fileID,'###\n');
        end
        fprintf(fileID,'***\n');
    end
    fclose(fileID);
end
end