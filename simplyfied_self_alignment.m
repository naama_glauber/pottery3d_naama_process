function [V_fixed, n_fixed, X_meanProf, Y_meanProf, smpl_dist_align] = simplyfied_self_alignment(v, f, n, skn) % mean_distance, j, i)
% v, f, n, are lists of one object (of part of it), not cell of objs!!
%   Detailed explanation goes here

%%%%%%%%% SHOULD I USE IT?? %%%%%%%%%%
% v=v';f=f';n=n';
 
%   if max(max(f))<length(v(:,1))
%       I=max(max(f))+1:length(v(:,1));
%       v(I,:)=[];
%       n(I,:)=[];
%       TT(I,:)=[];
%   end

    z=v(:,3);y=v(:,2);x=v(:,1);
    smpl_dist_align = find_sampling_distance(x, y, z, f)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    try
        %%% align the object in 4 different method (section and rim,   %%%
        %%% and also after Uzy transformation for section and for rim) %%%
        try
            [new_v,new_n]=self_alignment_sections(v,f,n,skn,smpl_dist_align);
        catch
            new_v=v;
            new_v=n;
        end
        try
            [v_rim,n_rim]=self_alignment_rim(v,f,n,skn,smpl_dist_align);
        catch
            v_rim=v;
            n_rim=n;
        end
        v_test=Uzy_transformation(v,0,pi);
        v_test(:,3)=v_test(:,3)-max(v_test(:,3));
        n_test=Uzy_transformation(n,0,pi);
        try
            [new_v_test,new_n_test]=self_alignment_sections(v_test, f, n_test, skn, smpl_dist_align);
        catch
            new_v_test=v;
            new_n_test=n;
        end
        try
            [v_rim_test,n_rim_test]=self_alignment_rim(v_test, f, n_test, skn, smpl_dist_align);
        catch
            v_rim_test=v;
            n_rim_test=n;
        end
        
        %%% from each type of alignment: Calculate X-Y of mean profile and check quality %%%
        if ~isempty(new_v)
            [X1,Y1,Q1]=arrange_mean_profile13(new_v,new_n,0.15,1);
        else
            Q1=100;
        end
        if ~isempty(v_rim)
            [X2,Y2,Q2]=arrange_mean_profile13(v_rim,n_rim,0.15,1);
        else
            Q2=100;
        end
        if ~isempty(new_v_test)
            [X3,Y3,Q3]=arrange_mean_profile13(new_v_test,new_n_test,0.15,1);
        else
            Q3=100;
        end
        if ~isempty(v_rim_test)
            [X4,Y4,Q4]=arrange_mean_profile13(v_rim_test,n_rim_test,0.15,1);
        else
            Q4=100;
        end
        h=gcf;
        if h==1
            close(1)
        end
        
        %%% Q measurment is an unbounded number (depends on the object alone)
        [z1,z2]=min([mean(Q1) mean(Q2) mean(Q3) mean(Q4)]);
        switch z2
            case 1
                X_meanProf=X1;Y_meanProf=Y1;
                V_fixed=new_v;
                n_fixed=new_n;
            case 2
                X_meanProf=X2;Y_meanProf=Y2;
                V_fixed=v_rim;
                n_fixed=n_rim;
            case 3
                X_meanProf=X3;Y_meanProf=Y3;
                V_fixed=new_v_test;
                n_fixed=new_n_test;
            case 4
                X_meanProf=X4;Y_meanProf=Y4;
                V_fixed=v_rim_test;
                n_fixed=n_rim_test;
        end
    catch
        V_fixed=v;
        n_fixed=n;
        [X_meanProf, Y_meanProf, Q]=arrange_mean_profile13(V_fixed,n_fixed,0.15,1);
    end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %figure; plot(X,Y,'k.',X,Y,'k');
    clear('X1', 'Y1', 'Q1', 'X2', 'Y2', 'Q2', 'X3', 'Y3', 'Q3', 'X4', 'Y4', 'Q4', 'z2', 'D1', 'D2', 'H1', 'H2');
end
                    

%     [new_v,new_n]=self_alignment_sections(v,f,n,skn, mean_distance);
%     if ~isempty(new_v)
%         H1 = new_v(:,3) > prctile(new_v(:,3),99);
%         H2 = new_v(:,3) < prctile(new_v(:,3),1);                        
%         if std(new_n(H1,3)) > std(new_n(H2,3))
%             new_v = Uzy_transformation(new_v,0,pi);
%             new_v(:,3)= new_v(:,3) - max(new_v(:,3));
%             new_n = Uzy_transformation(new_n,0,pi);
%         end
%         [v_rim,n_rim] = self_alignment_rim(new_v,f,new_n, skn, mean_distance);
%         [~,D1] = global_direction(new_n,new_v,1:length(new_v));
%         [~,D2] = global_direction(n_rim,v_rim,1:length(v_rim));
%         test_result1 = abs(dot(D1(:,3),D2(:,3)));
%         
%         if test_result1 < 0.96
%             disp('***********************');
%             disp(['j: ' num2str(j), 'i: ' num2str(i)]);
%             disp(['test result (<0.96): ' num2str(test_result1)]);
%             disp('***********************');
%             
%             new_v_test = Uzy_transformation(new_v,0,pi);
%             new_v_test(:,3) = new_v_test(:,3)-max(new_v_test(:,3)); %% naama:??
%             new_n_test = Uzy_transformation(new_n,0,pi);
%             [v_test,n_test] = self_alignment_rim(new_v_test,f,new_n_test, skn, mean_distance);
%             [~,D1] = global_direction(new_n_test,new_v_test,1:length(new_v));
%             [~,D2] = global_direction(n_test,v_test,1:length(v_test));
%             test_result2 = abs(dot(D1(:,3),D2(:,3)));
%             if test_result2 > test_result1
%                 v_rim = v_test;
%                 n_rim = n_test;
%                 new_v = new_v_test;
%                 new_n = new_n_test;
%             end
%         end
%         
%         if ~isempty(v_rim)
%             [X1, Y1, Q1]=arrange_mean_profile11(new_v, new_n, 0.1, 0.6);
%             [X2, Y2, Q2]=arrange_mean_profile11(v_rim, n_rim, 0.1, 0.6);
%             if test_result1 < 0.96
%                 [X3, Y3, Q3]=arrange_mean_profile11(new_v_test, new_n_test, 0.1, 0.6);
%                 [X4, Y4, Q4]=arrange_mean_profile11(v_test, n_test, 0.1, 0.6);
%             else
%                 Q3=100;
%                 Q4=100;
%             end
% 
%             % saving the data
%             [~, z2] = min([mean(Q1) mean(Q2) mean(Q3) mean(Q4)]);
%             %disp(['************   ' num2str(z2) '   ************']);
%             switch z2
%                 case 1
%                     X_meanProf = X1;
%                     Y_meanProf = Y1;
%                     V_fixed = new_v;
%                     n_fixed = new_n;
%                 case 2
%                     X_meanProf = X2;
%                     Y_meanProf = Y2;
%                     V_fixed = v_rim;
%                     n_fixed = n_rim;
%                 case 3
%                     X_meanProf = X3;
%                     Y_meanProf = Y3;
%                     V_fixed=new_v_test;
%                     n_fixed=new_n_test;
%                 case 4
%                     X_meanProf = X4;
%                     Y_meanProf = Y4;
%                     V_fixed=v_test;
%                     n_fixed=n_test;
%             end
%         end
%         if std(n_fixed(H1,3)) > std(n_fixed(H2,3)) %% new_n
% %             disp('*************************\n');
% %             disp (['j: ' num2str(j) ', i: ' num2str(1i) '\n' 'std(new_n(H1,3)) > std(new_n(H2,3))']); % >>>> TEMPORARY flip Y to be -Y']);
% %             disp('*************************\n');
%             Y_meanProf = -Y_meanProf;
%             Y_meanProf = Y_meanProf - max(Y_meanProf)/2;
%         end
%     end
%     clear('X1', 'Y1', 'Q1', 'X2', 'Y2', 'Q2', 'X3', 'Y3', 'Q3', 'X4', 'Y4', 'Q4', 'z2', 'D1', 'D2', 'H1', 'H2');

