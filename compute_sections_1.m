function sections=compute_sections_1(v,f,n,skn,mean_distance)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should compute horizontal sections of a fragment and the
% centers of the two arcs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the neighbors of each point, and the triangles that meat at each
% point

if length(n)>0
    if isempty(skn)
        skn(length(v)+1).skn=0;
        for i=1:length(f)
            skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
            skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
            skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);
        end
        skn(end)=[];
        for i=1:length(skn)
            skn(i).skn=unique(skn(i).skn);
        end
    end
else
    n=zeros(size(v));
    w=zeros(length(v),1);
    [cog,normals]=calc_normals(v,f);
    skn(length(v)+1).skn=0;
    for i=1:length(f)
        skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
        skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
        skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);
        n(f(i,1),:)=(w(f(i,1))*n(f(i,1),:)+normals(i,:))/(w(f(i,1))+1);
        n(f(i,2),:)=(w(f(i,2))*n(f(i,2),:)+normals(i,:))/(w(f(i,2))+1);
        n(f(i,3),:)=(w(f(i,3))*n(f(i,3),:)+normals(i,:))/(w(f(i,3))+1);
        w(f(i,1))=w(f(i,1))+1;
        w(f(i,2))=w(f(i,2))+1;
        w(f(i,3))=w(f(i,3))+1;
    end
    skn(end)=[];
    for i=1:length(skn)
        skn(i).skn=unique(skn(i).skn);
        n(i,:)=n(i,:)/norm(n(i,:));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the mean distance between points.
if isempty(mean_distance)
    z=v(:,3);y=v(:,2);x=v(:,1);
    ctr=0;
    for i=100:100:length(f)
        ctr=ctr+1;
        p1=f(i,1);p2=f(i,2);p3=f(i,3);
        D(ctr,1)=sqrt((x(p1)-x(p2))^2+(y(p1)-y(p2))^2+(z(p1)-z(p2))^2);
        D(ctr,2)=sqrt((x(p3)-x(p2))^2+(y(p3)-y(p2))^2+(z(p3)-z(p2))^2);
        D(ctr,3)=sqrt((x(p1)-x(p3))^2+(y(p1)-y(p3))^2+(z(p1)-z(p3))^2);
    end
    mean_distance=mean(mean(D));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%disp('-----------------------------------------')
%disp('Preparation:');
%disp('-----------------------------------------')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CTR=0;
clear('FS1','FS2','DFS1','DFS2','sections');
CTR=CTR+1;
z=v(:,3);y=v(:,2);x=v(:,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute horizontal sections.
% t=[min(z)+mean_distance/2:mean_distance:max(z)-mean_distance/2];
t=[min(z)+mean_distance/2:mean_distance:max(z)-mean_distance/2];
% t=[min(z)+mean_distance/2:mean_distance:max(z)-15];
ctr=0;
for i=1:length(t)
    try
        h=t(i);
        [x_section,y_section,z_section,n_section]=surface_plane_intersection(v,f,h,3*mean_distance,skn,n);
        v_section=[x_section' y_section' z_section'];
        [h1 D1]=global_direction(n_section,v_section,1:length(x_section));
        r_section=sqrt(x_section.^2+y_section.^2);
        
        %%%%%%%%%%%%%%
        D=diag(v_section*n_section')';
        %%%%%%%%%%%%%%
        arc1=find(D<0 & abs(h1(:,3)')>0.85); % arc1=find(D<0);
        arc2=find(D>0 & abs(h1(:,3)')>0.85); % arc2=find(D>0);
        teta1=atan2(y_section(arc1),x_section(arc1));
        teta2=atan2(y_section(arc2),x_section(arc2));
        [Xcenter1,Ycenter1,Radius1,Index1,W1]=FIND_CIRCLE_1(x_section(arc1),y_section(arc1));
        Index1=arc1(Index1);% plot3(x_section(Index1),y_section(Index1),z_section(Index1),'r*')
        [Xcenter2,Ycenter2,Radius2,Index2,W2]=FIND_CIRCLE_1(x_section(arc2),y_section(arc2));
        Index2=arc2(Index2);% plot3(x_section(Index2),y_section(Index2),z_section(Index2),'b*')
        if length(Index1)*length(Index2)>0
            f1=mean(sqrt(x_section(Index1).^2+y_section(Index1).^2));
            f2=mean(sqrt(x_section(Index2).^2+y_section(Index2).^2));
            if f1>f2
                ff=0;
            else
                ff=1;
            end
            if length(Index1)>4
                ctr=ctr+1;
                if ff==1
                    sections(ctr).type=1;
                else
                    sections(ctr).type=2;
                end
                sections(ctr).x=x_section(Index1);
                sections(ctr).y=y_section(Index1);
                sections(ctr).f=f1;
                sections(ctr).r=Radius1;
                sections(ctr).x0=Xcenter1;
                sections(ctr).y0=Ycenter1;
                sections(ctr).z0=h;
                sections(ctr).quality=W1;
                if range(teta1)>6 & prctile(x_section,70)<0
                    teta1(find(teta1<0))=teta1(find(teta1<0))+2*pi;
                end
                sections(ctr).range_teta=range(teta1);
            end
            if length(Index2)>4
                ctr=ctr+1;
                if ff==1
                    sections(ctr).type=2;
                else
                    sections(ctr).type=1;
                end
                sections(ctr).x=x_section(Index2);
                sections(ctr).y=y_section(Index2);
                sections(ctr).f=f2;
                sections(ctr).r=Radius2;
                sections(ctr).x0=Xcenter2;
                sections(ctr).y0=Ycenter2;
                sections(ctr).z0=h;
                sections(ctr).quality=W2;
                if range(teta2)>6 & prctile(x_section,70)<0
                    teta2(find(teta2<0))=teta2(find(teta2<0))+2*pi;
                end
                sections(ctr).range_teta=range(teta2);
            end
        end
    end
end
