function [cls_x, cls_y, variance]  = closest_xy(x, y, cut_x, cut_y)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    min_dist1 = 1000;
    idx1 = 0;
    for idx = 1:length(cut_x)
        dist = sqrt((cut_x(idx) - x).^2 + (cut_y(idx) - y).^2);
        if (dist < min_dist1)
            min_dist1 = dist;
            idx1 = idx;
        end
    end
    if idx1 == 1 || idx1 == length(cut_x)
        cls_x = cut_x(idx1);
        cls_y = cut_y(idx1);
    else
        sec_cut_x = [cut_x(1:idx1 - 1); cut_x(idx1 + 1:end)];
        sec_cut_y = [cut_y(1:idx1 - 1); cut_y(idx1 + 1:end)];
        
        min_dist2 = 1000;
        idx2 = 0;
        for idx = 1:length(sec_cut_x)
            dist = sqrt((sec_cut_x(idx) - x).^2 + (sec_cut_y(idx) - y).^2);
            if (dist < min_dist2)
                min_dist2 = dist;
                idx2 = idx;
            end
        end

        perc1 = 1 - (min_dist1 /(min_dist1 + min_dist2));
        perc2 = 1 - (min_dist2 /(min_dist1 + min_dist2));
        cls_x = cut_x(idx1)*perc1 + sec_cut_x(idx2)*perc2;
        cls_y = cut_y(idx1)*perc1 + sec_cut_y(idx2)*perc2;
    end
    variance = sqrt( (cls_x - x)^2 + (cls_y - y)^2 );
%     if variance > 1
%         variance;
%     end
    clear ('mean_dist1', 'mean_dist2', 'sec_cut_y', 'sec_cut_x', 'dist', 'idx1', 'idx2', 'perc1', 'perc2');
end

