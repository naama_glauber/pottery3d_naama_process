function [output_files_data, file_names] = readAllFileWithExt( dir_path, ext )
    % read all files in directory with given extantion 
    % dir_path = string, path of directory including all files.
    % ext = string, extantion defining the file type (including the dot)

    file_full_name = strcat(dir_path, ext);
    files = dir(char(file_full_name));
    N = length(files);

    file_names = cell(1,N);
    output_files_data = cell(1,N);
    for i=1:N
        file_fullname = strcat(dir_path ,files(i).name);
        [~,name,~] = fileparts(file_fullname);
        file_names{i} = name;
        if ext == '*.mat'
            output_files_data{i} = load(file_fullname);
        else
            if ext == '*.wrl'
               output_files_data{i} = readvrml(file_fullname);
            end
        end
    end
end

