function write_profiles_to_file( file_write_prof, girlsNames, all_xy, type )
% writing mean profiles (complete object, no cuts..)
% for each girl- different file. in each file, while each line contain: x , y
% and '***\n' saperate one object fron the next one.
numOfGirls = length(girlsNames);
for j = 1:numOfGirls
    fileID = fopen(strcat(file_write_prof, girlsNames{j}, '_' ,type, '_prof.txt'),'w');
    N = length(all_xy{j});
    for i = 1:N
        xs = all_xy{j}{i}(:,1);
        ys = all_xy{j}{i}(:,2);
        for idx = 1:length(xs)
            fprintf(fileID, strcat(num2str(xs(idx)), ',', num2str(ys(idx)), '\n'));
        end 
        fprintf(fileID,'***\n');
    end
    fclose(fileID);
end

end