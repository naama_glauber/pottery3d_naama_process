%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%         Read images               %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Adi_det = imread('Adi_details.png');
Adi_smooth = imread('Adi_smooth.png');

Casey_det = imread('Casey_details.png');
Casey_smooth = imread('Casey_smooth.png');

Eliya_det = imread('Eliya_details.png');
Eliya_smooth = imread('Eliya_smooth.png');

Grace_det = imread('Grace_details.png');
Grace_smooth = imread('Grace_smooth.png');

Tama_det = imread('Tama_details.png');
Tama_smooth = imread('Tama_smooth.png');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RZ_det = imread('RZ_details.png');
RZ_smooth = imread('RZ_smooth.png');

Rehov_det = imread('Rehov_details.png');
Rehov_smooth = imread('Rehov_smooth.png');

%%%%%%%%%%%%%%%%%%% alignment: area vs icp %%%%%%%%%%%%%%%%%
RZ = imread('I:\labs\ng6767535\Archeology\RESULTS\renders_days\Rehov_icp_vs_area_align.png');
Rehov = imread('I:\labs\ng6767535\Archeology\RESULTS\renders_days\RZ_icp_vs_area_align.png');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%         Convert to double         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Adi_det_2 = double(Adi_det)./255;
Adi_smooth_2 = double(Adi_smooth)./255;

Casey_det_2 = double(Casey_det)./255;
Casey_smooth_2 = double(Casey_smooth)./255;

Eliya_det_2 = double(Eliya_det)./255;
Eliya_smooth_2 = double(Eliya_smooth)./255;

Grace_det_2 = double(Grace_det)./255;
Grace_smooth_2 = double(Grace_smooth)./255;

Tama_det_2 = double(Tama_det)./255;
Tama_smooth_2 = double(Tama_smooth)./255;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RZ_det_2 = double(RZ_det)./255;
RZ_smooth_2 = double(RZ_smooth)./255;

Rehov_det_2 = double(Rehov_det)./255;
Rehov_smooth_2 = double(Rehov_smooth)./255;
%%%%%%%%%%%%%%%%%%% alignment: area vs icp %%%%%%%%%%%%%%%%%
RZ_2 = double(RZ)./255;
Rehov_2 = double(Rehov)./255;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%         Gamma Correction      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Adi_det_2 = Adi_det_2.^0.85;
Adi_smooth_2 = Adi_smooth_2.^0.85;

Casey_det_2 = Casey_det_2.^0.85;
Casey_smooth_2 = Casey_smooth_2.^0.85;

Eliya_det_2 = Eliya_det_2.^0.85;
Eliya_smooth_2 = Eliya_smooth_2.^0.85;

Grace_det_2 = Grace_det_2.^0.85;
Grace_smooth_2 = Grace_smooth_2.^0.85;

Tama_det_2 = Tama_det_2.^0.85;
Tama_smooth_2 = Tama_smooth_2.^0.85;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RZ_det_2 = RZ_det_2.^0.85;
RZ_smooth_2 = RZ_smooth_2.^0.85;

Rehov_det_2 = Rehov_det_2.^0.85;
Rehov_smooth_2 = Rehov_smooth_2.^0.85;
%%%%%%%%%%%%%%%%%%% alignment: area vs icp %%%%%%%%%%%%%%%%%
RZ_2 = RZ_2.^0.85;
Rehov_2 = Rehov_2.^0.85;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%           Write images            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imwrite(Adi_det_2,'Adi_det_2.png')
imwrite(Adi_smooth_2,'Adi_smooth_2.png')

imwrite(Casey_det_2,'Casey_det_2.png')
imwrite(Casey_smooth_2,'Casey_smooth_2.png')

imwrite(Eliya_det_2,'Eliya_det_2.png')
imwrite(Eliya_smooth_2,'Eliya_smooth_2.png')

imwrite(Grace_det_2,'Grace_det_2.png')
imwrite(Grace_smooth_2,'Grace_smooth_2.png')

imwrite(Tama_det_2,'Tama_det_2.png')
imwrite(Tama_smooth_2,'Tama_smooth_2.png')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imwrite(RZ_det_2,'RZ_det_2.png')
imwrite(RZ_smooth_2,'RZ_smooth_2.png')

imwrite(Rehov_det_2,'Rehov_det_2.png')
imwrite(Rehov_smooth_2,'Rehov_smooth_2.png')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
imwrite(RZ_2,'RZ_black_background.png')
imwrite(Rehov_2,'Rehov_black_background.png')







