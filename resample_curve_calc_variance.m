function [ output_xy_all, output_xy_mean, var ] = resample_curve_calc_variance( mean_xy, cuts_xy, max_mean_s_idx, max_all_s_idx, num_of_samples )
% [smpl_all_xy{j}{i}, smpl_xy_mean_prof{j}{i}, mean_vars{j}{i}]
%   @mean_prof_x, @mean_prof_y = are lists of floats
%   @cuts_x, @cuts_y = are cells of size(numOfCuts), each contain a list of the cut's profile.
%   @min__y, @max_y = are the min and max of the profile
%   resampling the mean curve @num_of_samples times. for such point (sample)
%   we find in each cut the closest points along the curve, with jump
%   according to @num_of_samples
%   @output_x, @output_y incldes these closest points
%   @var contains for each cut and each sample_idx, how far is it from this
%   point in the mean profile.
step = length(mean_xy) / num_of_samples;
num_of_cuts = length(cuts_xy);

output_xy_all = cell(1, num_of_cuts);
output_xy_mean = zeros(num_of_samples,2);
all_vars = zeros(num_of_samples, num_of_cuts);
var = [];

for k = 1:num_of_cuts
    output_xy_all{k} = zeros(num_of_samples, 2);
    for smpl_idx = 1:num_of_samples
        acc_smpl_float = max(smpl_idx*step, 1);
        acc_smpl_idx = fix(acc_smpl_float);
        next_idx_ratio = acc_smpl_float - fix(acc_smpl_float); % from 0 to 1
        prev_idx_ratio = 1 - next_idx_ratio;
        x_s = mean_xy(:,1);
        y_s = mean_xy(:,2);
        x = x_s(min(acc_smpl_idx + 1, length(x_s)))*next_idx_ratio +  x_s(acc_smpl_idx)*prev_idx_ratio;
        y = y_s(min(acc_smpl_idx + 1, length(y_s)))*next_idx_ratio +  y_s(acc_smpl_idx)*prev_idx_ratio;

        %%%% Why do I need that??
        if k==1
            output_xy_mean(smpl_idx,1) = x;
            output_xy_mean(smpl_idx,2) = y;
        end
        
        cut_x = cuts_xy{k}(:,1);
        cut_y = cuts_xy{k}(:,2);
        % check if we are in inner\ outer profile:
        % for each such case (^) we will respectively check in the
        % inner\ outer profile of the cut..
        if acc_smpl_idx < max_mean_s_idx
            indexs = 1:max_all_s_idx{k};
        else
            length_lst = length(cut_x);
            indexs = max_all_s_idx{k}:length_lst;
        end
        %figure;
        %plot(mean_prof_x, mean_prof_y, 'b');
        %hold on;
        %plot(cuts_x{k}, cuts_y{k}, 'k');
        %scatter(x,y, 'r*')
        [cls_x, cls_y, variance]  = closest_xy(x, y, cut_x(indexs), cut_y(indexs));
        %scatter(cls_x,cls_y, 'g*')
        %hold off;
        output_xy_all{k}(smpl_idx,1) = cls_x;
        output_xy_all{k}(smpl_idx,2) = cls_y;
        all_vars(smpl_idx,k) = variance;
        %end
    end
end
for smpl_idx = 1:num_of_samples
    var = [var, mean(all_vars(smpl_idx,:))];
end
end

