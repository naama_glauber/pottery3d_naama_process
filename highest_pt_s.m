function [highX, highY, idxRef] = highest_pt_s( allX, allY )
% allX, allY = are lists of values
% we want to find the point(highX,highY), when Y in maximal.
% idxRef = is the index of that point
    highY = 0.0;
    indexY = -1;
    for i=1:length(allY)
        if ( allY(i) > highY )
            highY = allY(i);
            idxRef = i;
        end
    end
   highX = allX(idxRef);
end
