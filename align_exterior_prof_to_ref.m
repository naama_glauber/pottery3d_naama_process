% align_out_prof_to_ref

% using: mean_xy_prof_ref
% using: mean_xy_prof_no_trim, cell 1x5

% find exterior of ref profile
Y_mean_ref_prof = mean_xy_prof_ref(:,2);
X_mean_ref_prof = mean_xy_prof_ref(:,1);
[~, max_idx] = max(Y_mean_ref_prof);

X_mean_ref_prof = X_mean_ref_prof(1:max_idx);
Y_mean_ref_prof = Y_mean_ref_prof(1:max_idx);
mean_exterior_prof_ref = [X_mean_ref_prof, Y_mean_ref_prof];

% trim ref profile
above_y_threshold = Y_mean_ref_prof > -15;
X_mean_ref_prof = X_mean_ref_prof(above_y_threshold);
Y_mean_ref_prof = Y_mean_ref_prof(above_y_threshold);
figure;
plot(X_mean_ref_prof, Y_mean_ref_prof);

% take only the exterior of all other profiles
exterior_all_mean_profiles = mean_xy_prof_no_trim; %cell(1,5)
for j=1:length(mean_xy_prof_no_trim)
    N = length(mean_xy_prof_no_trim{j})
    for i = 1:N
       
        
    end    
end
