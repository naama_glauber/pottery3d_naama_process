function [RN_pts,NB_pts, TOP_pts, RIGHT_pts, NECK_pts] = calc_plot_curv_norm(orig_mean_profs, sampled_mean_prof, potters_names, cutrim_y_ranges, is_ploting)
    n_potters = length(sampled_mean_prof);
    RN_pts = cell(1,n_potters);
    NB_pts = cell(1,n_potters);
    TOP_pts = cell(1,n_potters);
    NECK_pts = cell(1,n_potters);
    RIGHT_pts = cell(1,n_potters);
    
    %potters_obj_names = cell(2,1);
    %potters_obj_names{1} = cell(9,1);
    %potters_obj_names{1} = {'Lashman', 'Jera-Ram', 'Mangilal', 'Pemaram', 'Devilal', 'Govindram', 'Hajiram', 'Udaram', 'Zorilal'};
    %potters_obj_names{2} = cell(14,1);
    %potters_obj_names{2} = {'Anwar-Khan', 'Ramzan', 'Sakur', 'Sokat', 'Azruddin', 'Ramdayal', 'Aksar', 'Razak', 'Samir', 'Youssouf', 'Fazrudin', 'Mazid', 'Amin', 'Gani'};
    
    for j=1:n_potters
        n_objects = length(sampled_mean_prof{j});
        RN_pts{j} = cell(1,n_objects);
        NB_pts{j} = cell(1,n_objects);
        TOP_pts{j} = cell(1,n_objects);
        NECK_pts{j} = cell(1,n_objects);
        RIGHT_pts{j} = cell(1,n_objects);
        cutrim_y = cutrim_y_ranges(j);
        for i=1:n_objects
            v = sampled_mean_prof{j}{i};
            v_orig = orig_mean_profs{j}{i};
            % the border from top is given by the user 
            range_RN_idxs = find(v(:,2) <= cutrim_y);
            v_cutrim = v(range_RN_idxs, :); % = v(range_idxs, :)
            [~, deep_idx] = min(v_cutrim(:,1));
            % the border from bottom, this is the smallest radius
            [~, cutrim_cutneck_idx] = min(v_cutrim(:,1));
            
            cutrimneck_y = v_cutrim(cutrim_cutneck_idx,2);
            range_NB_idxs = find(v_cutrim(:,2) > cutrimneck_y);
            range_B_idxs = find(v(:,2) < cutrimneck_y) + 1;
            
            % rim orig part
            range_origRN_idxs = find(v_orig(:,2) >= cutrim_y);
            origv_cutrim = v_orig(range_origRN_idxs, :);
            [rightRx, rightR_arg] = max(origv_cutrim(:,1));
            rightRy = origv_cutrim(rightR_arg,2);
            RIGHT_pts{j}{i} = [rightRx, rightRy];
            
            % NORMAL
            all_norms = LineNormals2D(v);
            norms_RN = all_norms(range_NB_idxs, :);
            norms_NB = all_norms(range_B_idxs, :);
            
            %norm_in_min_curve_pt = norms(argmin, :);
            %[~, norm_argmax] = max(norms(:,1)); %curvs_range*x_in_range
            [RNnorm_minval, RNnorm_argmin] = min(norms_RN(:,1)); %curvs_range*x_in_range
            RNnorm_argmin = RNnorm_argmin + range_NB_idxs(1)-1;
            point_RN = [v(RNnorm_argmin, 1), v(RNnorm_argmin, 2)];
            
            %[NBnorm_maxval, NBnorm_argmax] = max(norms_NB(:,1)); %curvs_range*x_in_range
            end_idx = size(norms_NB,1);
            diff_norms_NB = norms_NB(2:end_idx,1) - norms_NB(1:end_idx-1,1);
            [NBnorm_maxval, NBnorm_argmax] = max(diff_norms_NB);
            point_NB = [v(NBnorm_argmax, 1), v(NBnorm_argmax, 2)];
            
            % find closest point in the original curve (not sampled)
            [~, RNidx] = min(sum(((v_orig - point_RN).^2),2));
            RN_pts{j}{i} = v_orig(RNidx,:);
            [~, NBidx] = min(sum(((v_orig - point_NB).^2),2));
            NB_pts{j}{i} = v_orig(NBidx,:);

            [topRy, topR_arg] = max(v_orig(:,2));
            topRx = v_orig(topR_arg,1);
            [deepNx, deepN_arg] = min(v_orig(:,1));
            deepNy = v_orig(deepN_arg,2);
            TOP_pts{j}{i} = [topRx,topRy];
            NECK_pts{j}{i} = [deepNx, deepNy];
           
            %%%%%%%%%%%%%%%%%%%%%%%%%  CURVATURE  %%%%%%%%%%%%%%%%%%%%%%%%%
%             prev_idx = range_idxs(1)-1;
%             curvs = LineCurvature2D(v);
%             [maxval, argmax] = max(curvs);
%             [minval, argmin] = min(curvs);
%             curvs_amp = max(curvs, -curvs);
%             curvs_range = LineCurvature2D(v_in_range);
%             [~, argmax] = max(curvs_range); 
%             [~, argmin] = min(curvs_range); %curvs_range*x_in_range
            
            %%%%%%%%%%%%%%%%%%%%%%%  FIGURE PLOT  %%%%%%%%%%%%%%%%%%%%%%%%%
            if nargin > 3 && is_ploting
                hfig = figure;
                subplot(1,2,1);
                title(strcat('x norm: ', potters_names{j}, '-', num2str(i))) %, ', argmin: ', num2str(argmin), ', argmax: ', num2str(argmax)));
                hold on;
                % BASE
                bar(all_norms(:,1),'FaceColor', [0.5,0.5,0.5]);
                % GREEN
                NBmin_norm_bar = zeros(size(all_norms));
                NBmin_norm_bar(NBnorm_argmax) = all_norms(NBnorm_argmax + 1, 1); %NBnorm_maxval;
                bar(NBmin_norm_bar, 'FaceColor', [0.0,1.0,0.0]);
                
                green_bar = zeros(size(all_norms));
                for k=1:deep_idx
                    green_bar(k) = all_norms(k,1);
                end
                bar(green_bar,'FaceColor', [0.7,1.0,0.7]);
                
                % RED
                red_bar = zeros(size(all_norms));
                for k=deep_idx+1:range_RN_idxs(end)
                    red_bar(k) = all_norms(k,1);
                end
                bar(red_bar,'FaceColor', [1.0,0.7,0.7]);
                 
                RNmin_norm_bar = zeros(size(all_norms));
                RNmin_norm_bar(RNnorm_argmin) = RNnorm_minval;
                bar(RNmin_norm_bar, 'FaceColor', [1.0,0.0,0.0]);
                
                %endtop_bar = zeros(size(all_norms));
                %endtop_bar(range_RN_idxs(end)) = all_norms(range_RN_idxs(end));
                %bar(endtop_bar, 'FaceColor', [0.0,0.0,1.0]);
                
                endtop_bar = zeros(size(all_norms));
                endtop_bar(deep_idx) = all_norms(deep_idx);
                bar(endtop_bar, 'FaceColor', [0.0,0.0,0.0]);
                
                xlabel('profile points')
                ylabel('X normal value')
                title('Divide RNB by X-norm')
                hold off;
                %legend('All X-nornal Vals','NB search area', 'NB Point', 'RN search area', 'RN Point', 'Smallest Radius')

                %%%%%%%%%%%%%%%%%%%%%%%% CURVATURE BARS %%%%%%%%%%%%%%%%%%%%%%%%
                %bar(curvs_range, 'k');
                %max_bar = zeros(size(curvs_range));
                %min_bar = zeros(size(curvs_range));
                %max_bar(argmax) = maxval;
                %min_bar(argmin) = curvs_range(argmin);
                %bar(max_bar, 'r');
                %bar(min_bar, 'g');
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                subplot(1,2,2);
                title(strcat('Prof: ',potters_names{j}, '-', num2str(i), ', min:G, max:R'));
                hold on;
                plot(v_orig(:,1),v_orig(:,2), 'k');
                plot(v(1:deep_idx,1),v(1:deep_idx,2), 'color', [0.5,1.0,0.5]);
                plot(v(deep_idx:range_RN_idxs(end),1),v(deep_idx:range_RN_idxs(end),2), 'color', [1.0,0.5,0.5]);
                
                plot(point_NB(1),point_NB(2), 'go');
                %plot(point_NB(1),point_NB(2), 'gx');
                plot(point_RN(1),point_RN(2), 'ro');
                %plot(point_RN(1),point_RN(2), 'rx');
                plot(v(1,1),v(1,2), 'kx');
                plot(v(deep_idx,1),v(deep_idx,2), 'kx') %plot(deepNx,deepNy, 'kx');
                plot(v(range_RN_idxs(end),1),v(range_RN_idxs(end),2), 'kx');
                hold off;
                %legend('profile curve','NB smoothed search area','RN smoothed search area', 'NB Point', 'RN Point','borders')
                
                %plot([60, 115], [cutrimneck_y, cutrimneck_y], 'b')
                %plot([60, 115], [cutrim_y, cutrim_y], 'b')
                %plot(topRx, topRy, 'b*');
                %plot(rightRx, rightRy, 'b*');
                xlim([60 115])
                ylim([-65 5])

                fig_path = strcat('.\Mean profile\RNB\norm_', potters_names{j}, '_', num2str(i),'.png');
                saveas(hfig, fig_path)
                close(hfig)
            end
        end
    end
    clear('n_potters','n_objects','cutrim_y','v','v_orig','max_x','min_x','range_RN_idxs');
    clear('v_cutrim','cutrim_cutneck_idx','cutrimneck_y','range_NB_idxs','range_B_idxs','all_norms','norms_RN','norms_NB');
    clear('RNnorm_minval','RNnorm_argmin', 'point_RN','end_idx', 'diff_norms_NB','NBnorm_maxval','NBnorm_argmax', 'orig_pts');
    clear('point_NB', 'point_RN','RNidx', 'NBidx' ,'hfig','fig_path','RNmin_norm_bar','NBmin_norm_bar', 'max_bar', 'min_bar');
end

%figure;
%set(gcf, 'PaperUnits', 'inches');
%set(gcf, 'PaperSize', [80, 40]);
%plot(v(:,1), v(:,2));
