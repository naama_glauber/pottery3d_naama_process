function [ inner_hist, outer_hist, pt_in, pt_out ] = calc_var_on_ys(x_out1,y_out1, x_in1,y_in1, x_avg1,y_avg1, min_s1,max_s1, sample_num_y )
%   find statistics about the histogram of variance as a function of the height
%   sample_num_y: divide model horizontally to this number of samples
%   x_in, y_in, x_out, y_out: each is a cell(1, num_of_cuts). in each cell
%     there is a list of x or y values (of the profile of the current cut)
%   x_avg, y_avg: are lists of the avarage profile.
%   min_s, max_s: are lists values of the min\max height I can use for each
%   object (it is the common area between all the cut-profiles of the same object)
%   NOTE: All params are of the same object!!

    num_of_cuts = length(x_out1);
    inner_hist = zeros(sample_num_y, 2);
    outer_hist = zeros(sample_num_y, 2);
    pt_in = cell(1,num_of_cuts); % zeros(sample_num_y, 2);
    pt_out = cell(1,num_of_cuts); % zeros(sample_num_y, 2);
    
    for cut_idx = 1:num_of_cuts
        pt_in{cut_idx} = zeros(sample_num_y, 2);
        pt_out{cut_idx} = zeros(sample_num_y, 2);
    end
    
    for h_idx = 1:sample_num_y
        var_x_in = 0;
        var_x_out = 0;
        dist = max_s1 - min_s1;
        for cut_idx = 1:num_of_cuts
            height_y = (min_s1) + dist*(h_idx - 1)/sample_num_y;
            [approx_x_in, ~, ~]  = linear_interpolatation_x_ginen_y(y_in1{cut_idx}, x_in1{cut_idx}, height_y);
            [approx_x_out, ~, ~] = linear_interpolatation_x_ginen_y(y_out1{cut_idx}, x_out1{cut_idx}, height_y);
            [approx_x_avg, ~, ~] = linear_interpolatation_x_ginen_y(y_avg1, x_avg1, height_y);
            
            pt_in{cut_idx}(h_idx,:) = [approx_x_in, height_y];
            pt_out{cut_idx}(h_idx,:) = [approx_x_out, height_y];
            
            var_x_in  = var_x_in  + (approx_x_in  - approx_x_avg)^2;
            var_x_out = var_x_out + (approx_x_out - approx_x_avg)^2;   
        end
        var_x_in = var_x_in/num_of_cuts;
        var_x_out = var_x_out/num_of_cuts;
        
        inner_hist(h_idx,:) = [var_x_in, height_y];
        outer_hist(h_idx,:) = [var_x_out, height_y];
    end
end