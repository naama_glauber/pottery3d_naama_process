function [new_v,new_n]=self_alignment_sections(v,f,n,skn,mean_distance)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should find the best alignment of a shard using self
% improve method, in which the computer knows what should be the next
% iteration. It is based on Uzy's formulas that compute the distortion of
% every horizontal section and find the angles of the transformation.


% The difference between this function and self_alignment3 is that here we
% give weights to the equations relatively to the derivative value of the
% radius as a function of the height.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the neighbors of each point, and the triangles that meat at each
% point
tic
if ~isempty(n)
    if isempty(skn)
        skn(length(v)+1).skn=0;    
        for i=1:length(f)
            skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
            skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
            skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);  
        end
        skn(end)=[];
        for i=1:length(skn)
            skn(i).skn=unique(skn(i).skn);
        end
    end
else
    n=zeros(size(v));
    w=zeros(length(v),1);
    [cog,normals]=calc_normals(v,f);
    skn(length(v)+1).skn=0;
    for i=1:length(f)
        skn(f(i,1)).skn(end+1:end+2)=f(i,2:3);
        skn(f(i,2)).skn(end+1:end+2)=f(i,[1,3]);
        skn(f(i,3)).skn(end+1:end+2)=f(i,[1,2]);
        n(f(i,1),:)=(w(f(i,1))*n(f(i,1),:)+normals(i,:))/(w(f(i,1))+1);
        n(f(i,2),:)=(w(f(i,2))*n(f(i,2),:)+normals(i,:))/(w(f(i,2))+1);
        n(f(i,3),:)=(w(f(i,3))*n(f(i,3),:)+normals(i,:))/(w(f(i,3))+1);
        w(f(i,1))=w(f(i,1))+1;
        w(f(i,2))=w(f(i,2))+1;
        w(f(i,3))=w(f(i,3))+1;
    end
    skn(end)=[];
    for i=1:length(skn)
        skn(i).skn=unique(skn(i).skn);
        n(i,:)=n(i,:)/norm(n(i,:));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the mean distance between points.
if isempty(mean_distance)
    z=v(:,3);y=v(:,2);x=v(:,1);
    ctr=0;
    for i=100:100:length(f)
        ctr=ctr+1;
        p1=f(i,1);p2=f(i,2);p3=f(i,3);
        D(ctr,1)=sqrt((x(p1)-x(p2))^2+(y(p1)-y(p2))^2+(z(p1)-z(p2))^2);
        D(ctr,2)=sqrt((x(p3)-x(p2))^2+(y(p3)-y(p2))^2+(z(p3)-z(p2))^2);
        D(ctr,3)=sqrt((x(p1)-x(p3))^2+(y(p1)-y(p3))^2+(z(p1)-z(p3))^2);
    end
    mean_distance=mean(mean(D));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% subplot(1,2,1);t=draw_profiles(v,f,n);title('Original position');
%disp('-----------------------------------------')
%disp('Preparation:');
% [Pa Pt PA]=profiles_area_intersection(v,f,n);
% disp(['Mean of profiles intersection in original position: ' num2str(Pa)]);PPa(1)=Pa;
toc
%disp('-----------------------------------------')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sections=compute_sections_2(v,f,n,skn,mean_distance);
% teta=[sections.range_teta];
% I=find(teta<prctile(teta,20));
% sections(I)=[];
% x0=mean([sections.x0]);
% y0=mean([sections.y0]);
% r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
% I=find(r0>mean(r0));
% sections(I)=[];
% x0=mean([sections.x0]);
% y0=mean([sections.y0]);
% r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
% I=find(r0>mean(r0));
% sections(I)=[];
% x0=mean([sections.x0]);
% y0=mean([sections.y0]);
% v(:,1)=v(:,1)-x0;
% v(:,2)=v(:,2)-y0;
CTR=0;
FLEG=0;
v_record=v;n_record=n;
while FLEG<2
    clear('FS1','FS2','DFS1','DFS2','sections');
    CTR=CTR+1;
    z=v(:,3);y=v(:,2);x=v(:,1);
    ctr=0; % v_record=v;n_record=n;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     sections=compute_sections(v,f,n);
    if CTR==1
        sections=compute_sections_3(v,f,n,skn,mean_distance);
        teta=[sections.range_teta];
        I=find(teta<prctile(teta,20));
        sections(I)=[];
        x0=mean([sections.x0]);
        y0=mean([sections.y0]);
        r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
        I=find(r0>mean(r0));
        sections(I)=[];
        x0=mean([sections.x0]);
        y0=mean([sections.y0]);
        r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
        I=find(r0>mean(r0));
        sections(I)=[];
        x0=mean([sections.x0]);
        y0=mean([sections.y0]);
        v(:,1)=v(:,1)-x0;
        v(:,2)=v(:,2)-y0;
        tetasec=atan2(v(:,2),v(:,1));
        if range(tetasec)>pi
            sections=compute_sections_3(v,f,n,skn,mean_distance);
        else
            sections=compute_sections_1(v,f,n,skn,mean_distance);
        end
        [XP,YP,QP]=arrange_mean_profile13(v,n,0.15,0.6);
        last_Q=mean(QP);
    else
        tetasec=atan2(v(:,2),v(:,1));
        if range(tetasec)>pi
            sections=compute_sections_3(v,f,n,skn,mean_distance);
        else
            sections=compute_sections_1(v,f,n,skn,mean_distance);
        end
    end
    teta=[sections.range_teta];
    I=find(teta<prctile(teta,20));
    sections(I)=[];
    x0=mean([sections.x0]);
    y0=mean([sections.y0]);
    r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
    I=find(r0>mean(r0));
    sections(I)=[];
%     x0=mean([sections.x0]);
%     y0=mean([sections.y0]);
%     r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
%     I=find(r0>mean(r0));
%     sections(I)=[];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Computing the tranformation parameters
    side1=find([sections.type]==1);
    side2=find([sections.type]==2);
    F1=[sections(side1).f];
    F2=[sections(side2).f];
    method='both';
%     method='in  ';
%     method='out ';
    switch method
        case 'both'
            I=1:length(sections);
        case 'in  '
            I=find([sections.type]==1);
        case 'out '
            I=find([sections.type]==2);
    end
    p=polyfit([sections(I).x0],[sections(I).y0],1);
    phi=acot(-p(1));
    if sin(phi)*cos(phi)>0
        phi=phi+pi;
    end
    bx=-sin(phi)*p(2);
    if sum(gradient([sections(I).x0]))>0
        phi=phi+pi;
    end
    %%%%%%%%%%%%%%%%%%
    % smooth F
    if length(side1)>5 && length(side2)>5
        piece_size=1;
        FS1(1)=mean(F1(1:3));
        FS1(2)=mean(F1(1:4));
        FS1(length(F1))=mean(F1(end-2:end));
        FS1(end-1)=mean(F1(end-3:end));
        for i=3:length(F1)-2
            FS1(i)=mean(F1(i-2:i+2));
        end
        FS2(1)=mean(F2(1:3));
        FS2(2)=mean(F2(1:4));
        FS2(length(F2))=mean(F2(end-2:end));
        FS2(end-1)=mean(F2(end-3:end));
        for i=3:length(F2)-2
            FS2(i)=mean(F2(i-2:i+2));
        end
        F1=FS1;F2=FS2;
        %%%%%%%%%%%%%%%%%
        Z1=[sections(side1).z0];
        Z2=[sections(side2).z0];
        DF1=gradient(F1)./gradient(Z1);
        DF2=gradient(F2)./gradient(Z2);
        %%%%%%%%%%%%%%%%%%
        % smooth DF
        DFS1(1)=mean(DF1(1:3));DFS1(2)=mean(DF1(1:4));
        DFS1(length(DF1))=mean(DF1(end-2:end));
        DFS1(end-1)=mean(DF1(end-3:end));
        for i=3:length(DF1)-2
            DFS1(i)=mean(DF1(i-2:i+2));
        end
        DFS2(1)=mean(DF2(1:3));DFS2(2)=mean(DF2(1:4));
        DFS2(length(DF2))=mean(DF2(end-2:end));
        DFS2(end-1)=mean(DF2(end-3:end));
        for i=3:length(DF2)-2
            DFS2(i)=mean(DF2(i-2:i+2));
        end
        %%%%%%%%%%%%%%%%%
        A1=[ones(length(side1),1) -[Z1+F1.*DFS1]'];
        A2=[ones(length(side2),1) -[Z2+F2.*DFS2]'];
        B1=[sections(side1).x0]*sin(phi)-[sections(side1).y0]*cos(phi);
        B2=[sections(side2).x0]*sin(phi)-[sections(side2).y0]*cos(phi);
        W1=1./(1+abs(DFS1));
        W2=1./(1+abs(DFS2));
        A1(:,1)=A1(:,1).*W1';A1(:,2)=A1(:,2).*W1';B1=B1.*W1;
        A2(:,1)=A2(:,1).*W2';A2(:,2)=A2(:,2).*W2';B2=B2.*W2;
        X1=-real(A1\B1');
        if ~isnan(X1(1))
            if sign(X1(1))==sign(mean([sections(side1).x0]))
                X1(1)=-X1(1);
            end
        else
            X1=0;
        end
        X2=-real(A2\B2');
        if ~isnan(X2(1))
            if sign(X2(1))==sign(mean([sections(side2).x0]))
                X2(1)=-X2(1);
            end
        else
            X2=0;
        end
        %disp([X1 X2]);
        switch method
            case 'both'
                X=(X1+X2)/2;
            case 'in  '
                X=X1;
            case 'out '
                X=X2;
        end
%         disp(['Iteration no. ' num2str(CTR) ':']);
%         disp(['calculated phi = ' num2str(phi/pi) ' pi']);
%         disp(['calculated teta = ' num2str(X(2)/pi) ' pi']);
%         disp(['by = ' num2str(bx) '  --  bx = ' num2str(X(1))]);
        %%%%%%%%%%%%%%%%%
        % Rotate 
        MM=v;
        MM(:,2)=MM(:,2)-bx;
        MM(:,1)=MM(:,1)+X(1);
        M3=[cos(phi) sin(phi) 0;-sin(phi) cos(phi) 0;0 0 1];
        M2=[1 0 0;0 cos(X(2)) sin(X(2));0 -sin(X(2)) cos(X(2))];
        M1=[cos(phi) -sin(phi) 0;sin(phi) cos(phi) 0;0 0 1];
        MM=M1*M2*M3*MM';
        NN=M1*M2*M3*n';
        v=MM';
        n=NN';
        try
            [XP,YP,QP]=arrange_mean_profile13(v,n,0.15,0.6);
            improvement_ratio=1-mean(QP)/last_Q;
        catch
            improvement_ratio=-1;
        end
%         disp(['current Q is: ' num2str(mean(QP)) '   improvement ratio is: ' num2str(improvement_ratio)]);
%         disp('-----------------------------------------')
        if improvement_ratio<0.01
            FLEG=FLEG+1;
%             disp(['End of program, number of iteration was ' num2str(CTR)]);
%             disp(['Q convegenes'])
        end
        if improvement_ratio>0
            v_record=v;n_record=n;
            last_Q=mean(QP);
        end
        if abs(X(2)/pi)<0.00005
            FLEG=2;
%             disp(['End of program, number of iteration was ' num2str(CTR) '. teta<0.00005 pi']);
        end
        if CTR==6
            FLEG=2;
%             disp(['End of program, number of iteration is ' num2str(CTR) '. teta = ' num2str(X(2)/pi) ' pi']);
%             disp(['maximum iterations number'])
        end
    else
        piece_size=0;
        FLEG=2;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if piece_size==1
    v=v_record;n=n_record;
%     disp(['Final Q value is:   ' num2str(last_Q)]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tetasec=atan2(v(:,2),v(:,1));
    if range(tetasec)>pi
        sections=compute_sections_3(v,f,n,skn,mean_distance);
    else
        sections=compute_sections_1(v,f,n,skn,mean_distance);
    end
    teta2=[sections.range_teta];
    I=find(teta2<prctile(teta2,20));
    sections(I)=[];
    x0=mean([sections.x0]);
    y0=mean([sections.y0]);
    r0=sqrt(([sections.x0]-x0).^2+([sections.y0]-y0).^2);
    I=find(r0>mean(r0));
    sections(I)=[];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if length(sections)>10
        x_move=mean([sections.x0]);
        y_move=mean([sections.y0]);
        v(:,1)=v(:,1)-x_move;
        v(:,2)=v(:,2)-y_move;
        [X1,Y1,Q1]=arrange_mean_profile13(v,n,0.15,0.6);
        [X2,Y2,Q2]=arrange_mean_profile13(v_record,n_record,0.15,0.6);
        if mean(Q1)<mean(Q2)
            new_v=v;new_n=n;
%             disp([' Final x movement = ' num2str(x_move) '   :    Final y movement = ' num2str(y_move)]);    
%             disp([' Final Q value is:   ' num2str(mean(Q1))]);
        else
            new_v=v_record;
            new_n=n_record;
        end
    else
        new_v=v_record;
        new_n=n_record;
    end
%     disp('-----------------------------------------------------------------------------------------');
%     disp('Total ');toc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Rotating the fragment to be on the positive x-axis
    M=mean(new_v);
    teta=atan2(M(2),M(1));
    v1=cos(teta)*new_v(:,1)+sin(teta)*new_v(:,2);
    v2=-sin(teta)*new_v(:,1)+cos(teta)*new_v(:,2);
    v3=new_v(:,3)-M(3);
    new_v=[v1 v2 v3];
    v1=cos(teta)*new_n(:,1)+sin(teta)*new_n(:,2);
    v2=-sin(teta)*new_n(:,1)+cos(teta)*new_n(:,2);
    new_n=[v1 v2 new_n(:,3)];
    clear('v1','M','v2','teta')
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % An option to plot the horizontal sections and their centers
%     show_vrml(new_v,f);
%     for i=1:length(sections)
%         plot3(sections(i).x,sections(i).y,ones(1,length(sections(i).x))*sections(i).z0,'.');
%     end
%     plot3([sections.x0],[sections.y0],[sections.z0],'ro');
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     figure
%     profiles=new_draw_profiles2(new_v,f,new_n);
%     title('Final alignment by the sections')
else
    new_v=v_record;
    new_n=n_record;
end
