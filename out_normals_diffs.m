function [diffs, xy_in, xy_out] = out_normals_diffs(all_profs, potters_names )
% INPUT:
%  all_profs are trm_icp_smpl_prof
% OUTPUT: This function do all of this:
%  calculate the different between @in_x and @out_x, both sharing the same y (@diffs)

find_dist = @( x, y, ref_x, ref_y ) ((x - ref_x)^2 + (y - ref_y)^2)^0.5;
%find_ortho_ab = @( x0, y0, x1, y1) [(-(x1-x0)/(y1-y0)), y0+((x1-x0)/(y1-y0))*x0];
find_ab = @( x0, y0, x1, y1) [(y1-y0)/(x1-x0), y0-((y1-y0)/(x1-x0))*x0];
% this number '10' is just to find a point that is far (outside the profile).
find_ortho_xy = @( x0, y0, x1, y1, sign ) [ 20*sign + x0, (-(x1-x0)*20*sign)/(y1-y0) + y0 ];

num_of_samples = 80;
numOfPottres = length(potters_names);

% initialize output data cells
val_xy   = cell(1,numOfPottres);
avg_dist = cell(1,numOfPottres);

%mat_profs = cell(1,numOfPottres);
sign = 1;
for j=1:numOfPottres
   N = length(all_profs{j});
   avg_dist{j} = zeros(1,N);
   val_xy{j} = cell(1,N);
   
   X1 = all_profs{j}{1}(:,1);
   Y1 = all_profs{j}{1}(:,2);
   v = [X1, Y1];
   all_norms = LineNormals2D(v);
   
   % https://www.mathworks.com/matlabcentral/answers/371799-intersection-of-line-and-curve-from-thier-points
   % it doesnt work well: first should switch between the X1 and X0 (blue and black)
   % second- in the start and end the polynom is too complex!! (to check if can control the plynom dgree or do not use it!!)
   line_p1 = v - all_norms*30;
   line_p2 = v + all_norms*30;
   x_lines = [line_p1(:,1), line_p2(:,1)];
   y_lines = [line_p1(:,2), line_p2(:,2)];
   
   hfig = figure; hold on;
   for smpl_idx = 1:num_of_samples
       plot(Xi, Yi, 'k')
       hold on; plot(X1, Y1, 'b')
       %for i=2:N
       i=2
           Xi = all_profs{j}{i}(:,1);
           Yi = all_profs{j}{i}(:,2);
           
           b_line = polyfit(x_lines(smpl_idx, :), y_lines(smpl_idx, :), 1);
           y_line2 = polyval(b_line, Xi(smpl_idx, :));
           
           x_intersect = interp1((y_line2-Yi), Xi, 0);
           y_intersect = polyval(b_line,x_intersect); 
           plot(x_intersect, y_intersect, 'r.')
       %end
   end
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % MANUALLY DO IT 
   
   % x0, y0 = cur  point on first curve, for each potter
   % x1, y1 = next point on mean curve
   for smpl_idx = 1:num_of_samples
       avg_dist_j_smpl = 0;
       
       x1_0 = X1(smpl_idx);
       y1_0 = Y1(smpl_idx);
       if smpl_idx == num_of_samples % in case we are in the last point..
           x1_1 = x1_0;
           y1_1 = y1_0 -1;
       else
           x1_1 = X1(smpl_idx+1);
           y1_1 = Y1(smpl_idx+1);
       end
       xy_ortho = find_ortho_xy( x1_0, y1_0, x1_1, y1_1, sign);
       xy_ortho = xy_ortho/norm(xy_ortho);
       ab_ortho = find_ab(x1_0, y1_0, xy_ortho(1), xy_ortho(2));

       for i=2:N
           X_all = all_profs{j}{i}(:,1);
           Y_all = all_profs{j}{i}(:,2);
           xi_0 = X_all(smpl_idx);
           yi_0 = Y_all(smpl_idx);
           
           if smpl_idx == num_of_samples % in case we are in the last pt
               xi_1 = xi_0;
               yi_1 = yi_0 -1; % to calc the normal only (will be in the zanith)
           else
               xi_1 = X_all(smpl_idx+1);
               yi_1 = Y_all(smpl_idx+1);
           end
           ab_i = find_ab(xi_0, yi_0, xi_1, yi_1);

           x_intersect = (ab_ortho(2)-ab_i(2))/(ab_ortho(1)-ab_i(1));
           y_intersect = (ab_ortho(1)*x_intersect) + ab_ortho(2);
           intersection_pt = [-x_intersect, -y_intersect];

           end
           dist_i = find_dist(xi_0, yi_0, intersection_pt(1), intersection_pt(2));
           avg_dist_j_smpl = avg_dist_j_smpl + dist_i;
           
           %plot(all_profs{j}{i}(:,1), all_profs{j}{i}(:,2), 'color', [0.8, 0.8,0.8]);
           %plot(intersection_pt(1), intersection_pt(2), 'color', [0.0, 1.0, 0.0]);
            
       end
end
   
%     A = cell2mat(all_profs{j});
%     Ax = A(:,1:2:end);
%     Ay = A(:,2:2:end);
%     max_y = min(max(Ay));
%     min_y = max(min(Ay));
%     %organized in matrices instead of cells
%     B = zeros(size(Ax,2), 2, size(Ax,1));
%     for m=1:size(Ax, 1)
%         pts = [];
%         for n=1:size(Ax,2)
%             x= Ax(m,n);
%             y= Ay(m,n);
%             pts = [pts; [x,y]];
%         end
%         B(:,:,m) = pts;
%    end
%    mat_profs{j} = B;
%    clear('Ax', 'Ay', 'A', 'i', 'j', 'pts', 'B')
   
end

