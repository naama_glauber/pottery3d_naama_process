function [mean_xy_prof, all_xy_prof, diffrences, min_diff] = cut_prof_buttom_by_min_height(mean_xy_prof_no_trim, all_xy_prof_no_trim, numOfGirls, numOfCuts)
min_diff = zeros(1,numOfGirls);
diffrences = cell(1,numOfGirls);
mean_xy_prof = cell(1, numOfGirls);
all_xy_prof = cell(1, numOfGirls);

eps = 5;
for j=1:numOfGirls
    N = length(mean_xy_prof_no_trim{j});
    mean_xy_prof{j} = cell(1,N);
    all_xy_prof{j} = cell(1,N);
%     longest_obj_val = 0;
%     longest_obj_val_who = '';
     shortest_obj_val = 1000;
%      shortest_obj_val_who = '';
%     avg_val = 0;
    for i = 1:N
        x = mean_xy_prof_no_trim{j}{i}(:,1);
        y = mean_xy_prof_no_trim{j}{i}(:,2);
        max_y = max(y);
        min_y = min(y);
        diff_height = max_y - min_y;
        diffrences{j} = [diffrences{j}, diff_height];
        if diff_height < shortest_obj_val
            shortest_obj_val = diff_height;
%             shortest_obj_val_who = strcat(girlsNames{j}, num2str(i));
        end
%         if diff_height > longest_obj_val
%             longest_obj_val = diff_height;
%             longest_obj_val_who = strcat(girlsNames{j}, num2str(i));
%         end
%         avg_val = avg_val + diff_height;
    end
    min_diff(j) = shortest_obj_val - eps;
    for i = 1:N
        x = mean_xy_prof_no_trim{j}{i}(:,1);
        y = mean_xy_prof_no_trim{j}{i}(:,2);
        cur_diff = max(y);
        threshold = cur_diff - min_diff(j);
        over_threshold_idxs = find(y >= threshold);
        mean_xy_prof{j}{i} = zeros(length(over_threshold_idxs),2);
        mean_xy_prof{j}{i}(:,2) = y(over_threshold_idxs);
        mean_xy_prof{j}{i}(:,1) = x(over_threshold_idxs);
        
        all_xy_prof{j}{i} = cell(1,numOfCuts);
        for k =1:numOfCuts
            x_s = all_xy_prof_no_trim{j}{i}{k}(:,1);
            y_s = all_xy_prof_no_trim{j}{i}{k}(:,2);
            cut_over_thrshld_idxs = find(y_s >= threshold);
            all_xy_prof{j}{i}{k} = zeros(length(cut_over_thrshld_idxs),2);
            all_xy_prof{j}{i}{k}(:,1) = x_s(cut_over_thrshld_idxs);
            all_xy_prof{j}{i}{k}(:,2) = y_s(cut_over_thrshld_idxs);
        end
    end
%     avg_val = avg_val/N
%     shortest_obj_val
%     shortest_obj_val_who
%     longest_obj_val
%     longest_obj_val_who
end

% for j = 1:5
%     figure; plot(1:1:length(diffrences{j}), diffrences{j}, 'k', 1:1:length(diffrences{j}), diffrences{j},'b*');
%     ylim([35 70])
% end
% figure; plot(X_meanProf{3}{6}, Y_meanProf{3}{6}, 'k', X_meanProf{3}{6}, Y_meanProf{3}{6}, 'b*');