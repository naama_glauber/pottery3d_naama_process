function [ newX, newY ] = ref_point_trans(allX, allY, highX_from, highY_from, highX_to, highY_to)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    transX = highX_to - highX_from;
    transY = highY_to - highY_from;
    
    newX = allX; % + transX;
    newY = allY + transY;
    
end

