function [V_lst, f_lst, skn_lst, n_lst, t_lst, all_file_names] = readFilesToDataStructures(files_path, girlsNames) %files_path)   %'/Users/ng6767535/Documents/MATLAB/mat files/Adi/'
% read files to data structures
% files_path = the path of all .mat files of the girls
% girlsNames = is a list of the names 
% reading the wrl is not working for now!

numOfGirls = length(girlsNames);
all_file_names  = cell(1,numOfGirls);
allFilesContent = cell(1,numOfGirls);
%allFilesContent_wrl = cell(1,numOfGirls);

for j=1:numOfGirls
   dir_path = strcat(files_path, char(girlsNames(j)),'\');
   %dir_path_wrl = strcat(files_path_wrl, char(girlsNames(j)));

   [allFilesContent{j}, all_file_names{j}] = readAllFileWithExt(dir_path, '*.mat' );
   %allFilesContent_wrl{j} = readAllFileWithExt(dir_path_wrl, '*.wrl' );
end

clear ('file_loaded', 'files');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% initialize all prameter are needed to extraxt the profiles
%%% (for now only 'V_lst', and 'n_lst' are in a
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   allX_val = cell(1,numOfGirls);
   allY_val = cell(1,numOfGirls);
   f_lst = cell(1, numOfGirls);
   skn_lst = cell(1, numOfGirls);
   V_lst = cell(1, numOfGirls);
   n_lst = cell(1, numOfGirls);
   t_lst = []; % ?????????????
   for j=1:numOfGirls
       %numberof vessels for each girl might be different
       N = length(allFilesContent{j});
       %init cell sizes
       allX_val{j} = cell(1,N);
       allY_val{j} = cell(1,N);
       f_lst{j} = cell(1,N);
       skn_lst{j} = cell(1,N);
       V_lst{j} = cell(1,N);
       n_lst{j} = cell(1,N);
      
       for i=1:N 
           %init content for each girl, each vessel
           allX_val{j}{i} = [allFilesContent{j}{i}.X];
           allY_val{j}{i} = [allFilesContent{j}{i}.Y];
           V_lst{j}{i} = [allFilesContent{j}{i}.V];
           f_lst{j}{i} = [allFilesContent{j}{i}.f];
           skn_lst{j}{i} = [allFilesContent{j}{i}.skn];
           n_lst{j}{i} = [allFilesContent{j}{i}.N];
       end
   end
end