% Naama_procedure_Manager Script (main procedure)

% local .mat files with the 3D scans of the potshreds
files_path = 'C:\Users\HP\Documents\Naama\MatLab-M-Filles\matfiles_Bezalel\'; %'I:\labs\ng6767535\MatLab-M-Filles\matPottersFiles\';
girlsNames = {'Adi', 'Casey', 'Eliya', 'Grace', 'Tama'};

% Archeological sites
% files_path = 'I:\labs\ng6767535\MatLab-M-Filles\ArcheologicalSites\';
% girlsNames = {'Rehov', 'RZ'};

% extract data from .mat file's
% TEMP: t_lst - FOR NOW IS EMPTY!
[V_lst, f_lst, skn_lst, n_lst, ~] = readFilesToDataStructures(files_path,girlsNames);

% numOfCuts could be changed according to user wish!
numOfCuts = 20;
% according to the experiment, here we have 5 different potters.
numOfGirls = length(girlsNames);
% init
all_xy_prof_no_trim = cell(1, numOfGirls);
mean_xy_prof_no_trim = cell(1, numOfGirls);

parfor j=1:numOfGirls       
   %numberof vessels for each girl might be different
   N = length(V_lst{j});
   all_xy_prof_no_trim{j} = cell(1,N);
   mean_xy_prof_no_trim{j} = cell(1,N);
   for i=1:N
       i
       %%% TO CHECK: if this is a correct way or we need another procedure. %%%
       %%% 1. aligning the model according to axis of symetry  %%%
       z=V_lst{j}{i}(:,3);y=V_lst{j}{i}(:,2);x=V_lst{j}{i}(:,1);
       smpl_dist = find_sampling_distance(x,y,z,f_lst{j}{i});
       [V_fixed,n_fixed] = self_alignment_sections(V_lst{j}{i},f_lst{j}{i},n_lst{j}{i}, skn_lst{j}{i}, smpl_dist);
       %%% try instead of "self_alignment_sections"
       %[V_fixed, n_fixed, X_meanProf{j}{i}, Y_meanProf{j}{i}] = simplyfied_self_alignment(V_lst{j}{i},f_lst{j}{i},n_lst{j}{i}, skn_lst{j}{i});
       
       %%% 2. find the mean profile after alignment            %%%
       [X, Y,~] = arrange_mean_profile13(V_fixed, n_fixed, 0.15, 1.0);
       mean_xy_prof_no_trim{j}{i} = zeros(length(X),2);
       mean_xy_prof_no_trim{j}{i}(:,1) = X;
       mean_xy_prof_no_trim{j}{i}(:,2) = Y;
       % plot(X,Y, 'r', X, Y, 'k.')
       
       %%% 3. Cut the object like a circle cake relating to the rotation axis
       % (which is the z-axis after the alignment)
       [newV_cells, newVidx_cells] = cakeCutModelAftAlign(V_fixed, numOfCuts);
       
       %%% 4. find the mean profile of each cut
       all_xy_prof_no_trim{j}{i} = cell(1,numOfCuts);
       for cutIdx = 1:numOfCuts         
           V_lst_cut    = V_fixed(newVidx_cells{cutIdx},:);
           n_lst_cut    = n_fixed(newVidx_cells{cutIdx},:);
           [Xs, Ys, ~] = arrange_mean_profile13(V_lst_cut, n_lst_cut, 0.15, 1.0);
           all_xy_prof_no_trim{j}{i}{cutIdx} = zeros(length(Xs),2);
           all_xy_prof_no_trim{j}{i}{cutIdx}(:,1) = Xs;
           all_xy_prof_no_trim{j}{i}{cutIdx}(:,2) = Ys;
       end
       V_lst{j}{i} = V_fixed;
       n_lst{j}{i} = n_fixed;
   end
end
clear ('skn', 'f', 'V', 'X', 'Y', 'x', 'y', 'z', 'Xs', 'Ys', 'VC', 'HL', 'V_fixed', 'n_fixed', 'V_lst_cut', 'n_lst_cut', 'Material', 'DefiniteDiameter', 'datasource', 'ext_prof', 'XaxisSet', 'selectquery', 'newVidx_cells', 'newV_cells', 'files_path');
%save('20_cuts.mat')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 1. on y = -10, reduce from the  mean profile (for all cuts) the buttom   %%%
%%% 2. split the mode to inner and outer sides according to max point, s =0  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[mean_xy_prof, all_xy_prof, ~, ~] = cut_prof_buttom_by_min_height(mean_xy_prof_no_trim, all_xy_prof_no_trim, numOfGirls, numOfCuts);
% for i = 1:N
%     figure;
%     for k = 1:numOfCuts
%         plot(all_xy_prof{j}{i}{k}(:,1), all_xy_prof{j}{i}{k}(:,2), 'color', [0.5,0.5,0.5]); hold on;
%     end
%         plot(mean_xy_prof{j}{i}(:,1), mean_xy_prof{j}{i}(:,2), 'b', mean_xy_prof{j}{i}(:,1), mean_xy_prof{j}{i}(:,2), 'b*'); hold off;
% end

sample_num_y = 100;
[xy_out, xy_in, all_max_s, min_s, max_s, min_avg_s, max_avg_s, max_mean_s_idx, max_all_s_idx, diff_in_out] = in_out_prof( all_xy_prof, mean_xy_prof, sample_num_y );
%min_global = -10;
%[X_meanProf_no_bottom, Y_meanProf_no_bottom] = cut_bottom_from_min(X_meanProf, Y_meanProf, min_global, numOfGirls, numOfCuts, false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 3. resampling the curve for unify vectors' length                          %%%
%%% 4. plot the mean variance over all the cuts, comparing to the mean profile %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num_of_samples = 100;
mean_vars = cell(1,numOfGirls);
smpl_all_xy = cell(1, numOfGirls);
smpl_xy_mean_prof = cell(1,numOfGirls);
num_of_vectors = 0;
for j = 1:numOfGirls
   N = length(mean_xy_prof{j});
   num_of_vectors = num_of_vectors+N;
   mean_vars{j} = cell(1,N);
   smpl_all_xy{j} = cell(1, N);
   smpl_xy_mean_prof{j} = cell(1,N);
   for i=1:N
    [smpl_all_xy{j}{i}, smpl_xy_mean_prof{j}{i}, mean_vars{j}{i}] = resample_curve_calc_variance( mean_xy_prof{j}{i}, all_xy_prof{j}{i},  max_mean_s_idx{j}{i}, max_all_s_idx{j}{i}, num_of_samples );
    
    %figure;
    %for k = 1:numOfCuts
    %    plot(smpl_x{j}{i}{k}, smpl_y{j}{i}{k}, 'color', [0.5,0.5,0.5]); hold on;
    %end
    %plot(X_meanProf_no_bottom{j}{i}, Y_meanProf_no_bottom{j}{i}, 'b', X_meanProf_no_bottom{j}{i}, Y_meanProf_no_bottom{j}{i}, 'b*');
    %hold off;
    %plot_var_profile(smpl_x{j}{i}, smpl_y{j}{i}, mean_vars{j}{i}, girlsNames, j, i)
   end
end

[all_in_vals_xy, all_out_vals_xy, ~] = in_out_pts_cuts_prof2( smpl_all_xy, smpl_xy_mean_prof , girlsNames ,numOfCuts, num_of_samples);

prof_write_path = 'C:\Users\HP\Documents\Naama\MatLab-M-Filles\matfiles_Bezalel\results\write_profiles_correct\' % 'I:\labs\ng6767535\MatLab-M-Filles\Naama_Procedure\results\write_profiles_correct\';
write_profiles_to_file(prof_write_path, girlsNames, all_in_vals_xy, 'in');
write_profiles_to_file(prof_write_path, girlsNames, all_out_vals_xy, 'out');
write_profiles_to_file(prof_write_path, girlsNames, smpl_xy_mean_prof, 'mean');
write_profiles_to_file(prof_write_path, girlsNames, mean_xy_prof_no_trim, 'mean_no_trim');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 5. Plots profiles histograms of variations (histograms don't work) %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[all_inner_hist, all_outer_hist, pt_inner, pt_outer] = calc_var_on_ys_all_obj(x_out,y_out, x_in,y_in, X_meanProf_no_bottom,Y_meanProf_no_bottom, min_s, max_s, sample_num_y);
%plot_profiles_histograms(all_x_Prof_no_bottom, all_y_Prof_no_bottom, X_meanProf_no_bottom, Y_meanProf_no_bottom, pt_inner, pt_outer, all_inner_hist, all_outer_hist,min_s, max_s,sample_num_y, girlsNames);
plot_only_profiles(all_x_Prof_no_bottom, all_y_Prof_no_bottom, X_meanProf_no_bottom, Y_meanProf_no_bottom, min_avg_s, max_avg_s, all_max_s, girlsNames, diff_in_out);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 6. Apply PCA! data is orgenized in a matrix [num_of_vectors][num_of_samples]  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
pca_mat = zeros(num_of_vectors, num_of_samples);
row_idx = 1;
for j = 1:numOfGirls
   N = length(all_x_Prof_no_bottom{j});
   for i=1:N
       pca_mat(row_idx, :) =  mean_vars{j}{i};
       row_idx = row_idx+1;
   end
end

% APPLY PCA
% @latent = principal component variances, i.e., contain the eigenvalues of the covariance matrix of pca_mat
% @score  = principal component score, which is the representation of pca_mat in the principal component space.
% @coeff = principal component coefficients for the 'num_of_vectors' by 'num_of_samples' data matrix pca_mat.
%
% Rows of score correspond to observations, columns to components. The centered data can be reconstructed by score*coeff'
% Rows (num_of_vectors) correspond to observations, and columns (num_of_samples) to variables or 'features'
% Each column of coeff contains coefficients for one principal component.
% The columns are in descending order in terms of component variance (latent)
[coeff,score,latent] = pca(pca_mat);
dim = 2;
coeff_reduce_dim = coeff(:, 1:dim);
mat_reduce_dim = pca_mat*coeff_reduce_dim;


colors = {'r','b','c','g','k'};
row_idx = 1;
figure;
for j = 1:numOfGirls
   N = length(all_x_Prof_no_bottom{j});
   for i=1:N
       scatter(mat_reduce_dim(row_idx, 1), mat_reduce_dim(row_idx, 2), strcat(colors{j},'*'));
       hold on;
       row_idx = row_idx+1;
   end
end

