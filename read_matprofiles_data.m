function [all_profs, potters_names, potter_filenames] = read_matprofiles_data(is_indians)
    % default is indians
    if ~exist('is_indians', 'var')
         is_indians = true;
    end
    
    if is_indians
        dir_path = '.\indian_potters_files\mat\';
%         files = dir(dir_path);
%         % Get a logical vector that tells which is a directory.
%         dirFlags = [files.isdir];
%         % Extract only those that are directories.
%         subFolders = files(dirFlags);
        
        potters_names = {'AKSAR', 'AMIN', 'ANWAR KHAN', 'AZRUDDIN', 'DEVILAL', 'FAZRUDIN', 'GANI', 'GOVINDRAM', 'HAJIRAM', 'JERA RAM', 'LASHMAN', 'MANGILAL', 'MAZID', 'PEMARAM', 'RAMDAYAL', 'RAMZAN', 'RAZAK', 'SAKUR', 'SAMIR', 'SOKAT', 'UDARAM', 'YOUSSOUF', 'ZORILAL'};
        n_potters = length(potters_names);
        allFilesContent = cell(1,n_potters);
        all_profs       = cell(1,n_potters);
        if ~exist('all_indian_profs', 'var')
            indian_path = '.\mat_files\all_indian_profs.mat';
            if exist(indian_path, 'file')
                all_profs = load(indian_path);
            else
                for j=1:n_potters
                    potter_path = strcat(dir_path, potters_names{j},'\');
                    [allFilesContent{j},~] = readAllFileWithExt(potter_path, '*.mat');
                   
                    figure; hold on;
                    title(potters_names{j});
                    n_objects = length(allFilesContent{j});
                    for i=1:n_objects
                        % allFilesContent{j}{n} contains these fields:
                        % V (vertices), f (faces), N (normals- to faces), skn (?), VC (??in same size of V)
                        % X, Y (profile- full!)
                        % HL (? 12x4), DefiniteDiameter (1), XaxisSet (0), Material (ceramics)
                        % PotteryObject (struct with many values)
                        % ext_prof (struct, contains X, Y - THE EXTERNAL ONLY! ~ 1K points)
                        X = allFilesContent{j}{i}.ext_prof.X';
                        Y = allFilesContent{j}{i}.ext_prof.Y';

                        all_profs{j}{i} = [X, Y];
                        plot(X, Y);
                        potter_filenames{j}{i} = allFilesContent{j}{i}.PotteryObject.Name;
                    end
                end
                save(indian_path, 'all_profs')
            end
        else
            all_profs = all_bez_profs;
            return
        end
        
    else % BEZ GIRLS
        potters_names = {'Adi', 'Casey', 'Eliya', 'Grace', 'Tama'};
        n_potters = length(potters_names);
        if ~exist('all_bez_profs', 'var')
            bez_path = '.\mat_files\all_bez_profs.mat';
            if exist(bez_path, 'file')
                all_profs = load(bez_path);
                all_profs = all_profs.all_bez_profs;
            else
                all_profs = cell(1,n_potters);
                dir_path = '..\MatLab-M-Filles\matfiles_Bezalel\'; %'I:\labs\ng6767535\MatLab-M-Filles\matPottersFiles\';
                for j=1:n_potters
                    potter_path = strcat(dir_path, potters_names{j},'\');
                    allFilesContent = cell(1,n_potters);
                    [allFilesContent{j},~] = readAllFileWithExt(potter_path, '*.mat');
                
                    figure; hold on;
                    n_objects = length(allFilesContent{j});
                    for i=1:n_objects
                        V   = [allFilesContent{j}{i}.V];
                        f   = [allFilesContent{j}{i}.f];
                        skn = [allFilesContent{j}{i}.skn];
                        n   = [allFilesContent{j}{i}.N];

                        % 1. aligning the model according to axis of symetry  %%%
                        z=V(:,3);y=V(:,2);x=V(:,1);
                        smpl_dist = find_sampling_distance(x,y,z,f);
                        [V_fixed,n_fixed] = self_alignment_sections(V,f,n, skn, smpl_dist);
                        % 2. find the mean profile after alignment            %%%
                        [X, Y,~] = arrange_mean_profile13(V_fixed, n_fixed, 0.15, 1.0);
                    end
                    all_profs{j}{i} = [X; Y];
                    plot(X,Y)
                end
                save(bez_path, 'all_profs')
                title(potters_names{j});
                %fig_path = strcat('.\indian_potters_files\results\all_profs_', potters_names{j},'.jpg');
                %saveas(all_profs_fig, fig_path);
            end
        else
            all_profs = all_bez_profs;
            return
        end
    end
    clear ('skn', 'f', 'V', 'X','Y', 'x', 'y', 'z', 'VC', 'HL','indian_path','bez_path', 'smpl_dist', 'fig_path', 'potter_path', 'dir_path', 'allFilesContent', 'V_fixed', 'n_objects', 'n_fixed', 'V_lst_cut', 'allFilesContent');
end