function [approx_x, approx_y, dist_x, idx_x] = closest_x_to_y( y_s, x_s, given_y )
%   Assumption: y curve is not extreme, and increasing (almost monotonicaly)
%   given a list of x-s and list of y-s and a given y not necessarily in the list of y_s.
%   we need to find an x value to his to find it's
    n = length(y_s);
    for i=1:n
        if (y_s(i) > given_y)
            break;
        end
    end
    idx_x = max(i-1, 1);
    dist_x = y_s(idx_x)-given_y;
    approx_x = x_s(idx_x);
    approx_y = y_s(idx_x);
end
