function plot_var_profile(x, y, var, girlsNames, j ,i)
% plot a profile with dots of red-white scale on it.
% the purpose is to present on this profile the variance of the cuts at each point.
% the higher the 

    % linear scale to 0-1:
    min_val = min(var);
    norm_var = (var - min_val);
    max_val = max(norm_var);
    if max_val == 0
        norm_var = norm_var.*0;
    else
        norm_var = norm_var./max_val;
    end
    
    % to see better the d?fferences!
    gamma_correction = 4;
    norm_var = norm_var.^gamma_correction;
    
    numOfCuts = length(x);
    for cut_idx = 1:numOfCuts
        var_fig = figure; hold on;
        str = strcat('var-', girlsNames{j}, '-', num2str(i), '-cut-', num2str(cut_idx));
        title(str)
        plot(x{cut_idx} ,y{cut_idx}, 'color', [.6 .6 .6])
        for pt_idx =1:length(var)
            tmp = max(min(var(pt_idx),1),0);
            % as long as the point is more stable (low variance), it is more red.
            plot(x{cut_idx}(pt_idx) ,y{cut_idx}(pt_idx), '*', 'color', [1 tmp tmp])
        end
        hold off;        
        saveas(gcf,[str '.png']);
        close(var_fig)
    end
    clear('norm_var', 'max_val', 'min_val', 'gamma_correction', 'var_fig', 'str', 'tmp');
end

