function new_mean_xy_prof = trim_bottom_for_specific_obj( mean_xy_prof , j, i, y_height)
    subplot(1,2,1);
    plot(mean_xy_prof{j}{i}(:,1), mean_xy_prof{j}{i}(:,2), 'b', mean_xy_prof{j}{i}(:,1), mean_xy_prof{j}{i}(:,2), 'r.');
    axis equal;
    
    x_s = mean_xy_prof{j}{i}(:,1);
    y_s = mean_xy_prof{j}{i}(:,2);
    n = sum(y_s > y_height);
    new_mean_xy_prof = zeros(n,2);
    counter = 0;
    for idx = 1:length(y_s)
        y = y_s(idx);
        x = x_s(idx);
        if y > y_height && ~(x == 0 && y ==0)
            counter = counter +1;
            new_mean_xy_prof(counter, 1) = x_s(idx);
            new_mean_xy_prof(counter, 2) = y_s(idx);
            %y_no_bottom{j}{i}{k} = [y_no_bottom{j}{i}{k}, y_s(idx)];
        end
    end
    subplot(1,2,2);
    plot(new_mean_xy_prof(:,1), new_mean_xy_prof(:,2), 'b', new_mean_xy_prof(:,1), new_mean_xy_prof(:,2), 'r.');
    axis equal;
end

% j = 1;
% i = 1;
% y_height = 7;
% mean_xy_prof{j}{i} = trim_bottom_for_specific_obj( mean_xy_prof , j, i, min_yheigt);