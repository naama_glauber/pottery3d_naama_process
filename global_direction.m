function [hh,D,V]=global_direction(nn,vv,I)
% function [hh D]=global_direction(nn,vv,f,I)

% this function should calculate a global direction of a 3D vector that the
% sum of the dot function of it with all of the normals in n, which are the
% normals to the surface of a shard.
% this function takes into consideration only the points that are included
% in I.
n=nn(I,:);
% v=vv(I,:);
N=length(I);
A(1,1)=sum(n(:,1).^2)/N;
A(2,2)=sum(n(:,2).^2)/N;
A(3,3)=sum(n(:,3).^2)/N;
A(1,2)=sum(n(:,1).*n(:,2))/N;
A(1,3)=sum(n(:,1).*n(:,3))/N;
A(2,3)=sum(n(:,2).*n(:,3))/N;
A(2,1)=A(1,2);
A(3,1)=A(1,3);
A(3,2)=A(2,3);
[v1,d1]=eig(A);
d1=diag(d1);
hh=n*v1;
D=v1;V=d1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% r=mean([range(v(:,1)),range(v(:,2)),range(v(:,3))])*3;
% c='brm';
% show_vrml(vv,f);hold on;axis equal
% H=mean(v);
% for i=1:3
%     h=v1(:,i)'*r;
%     plot3([H(1)-h(1) H(1)+h(1)],[H(2)-h(2) H(2)+h(2)],[H(3)-h(3) H(3)+h(3)],c(i),'LineWidth',2)
% end
% for i=1:3
%     II=find(abs(hh(:,i))>0.9);
%     plot3(v(II,1),v(II,2),v(II,3),[c(i),'o'])
% end
