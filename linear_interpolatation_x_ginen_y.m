function [approx_x, idx_x1, idx_x2] = linear_interpolatation_x_ginen_y( y_s, x_s, given_y )
%   Assumption, curve is not very extreme(?)
%   y_s, x_s should be lists, make sure these are list of only ONE SIDE of
%   the profile, i.e inner OR outter!
%   given y not necessarily in the list of y_s, we need to find an x value, using linear interpolation 
    n = length(y_s);
    for i=1:n
        if (y_s(i) > given_y)
            break;
        end
    end
    if i == 1 || i >= n % first or end point
        idx_x1 = i;
        idx_x2 = idx_x1;
        approx_x = x_s(idx_x1);
    else
        idx_x1 = i-1;
        idx_x2 = i;
        dist_x1 = abs(y_s(idx_x1)-given_y);
        dist_x2 = abs(y_s(idx_x2)-given_y);
        perc_x1 = dist_x1 /(dist_x1 + dist_x2);
        perc_x2 = dist_x2 /(dist_x1 + dist_x2);
        approx_x = x_s(idx_x1)*(1-perc_x1) + x_s(idx_x2)*(1-perc_x2); 
    end
end

