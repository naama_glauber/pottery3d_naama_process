function plot_only_profiles(all_x_Prof, all_y_Prof, X_meanProf, Y_meanProf, min_avg_s, max_avg_s , all_max_s, girlsNames, diff_in_out )
% INPUT:
% @all_x_Prof, @all_y_Prof = contain all cuts of profile, size (num_of_girls x num_of_object x num_of_cuts)
% @X_meanProf, @Y_meanProf = contain mean (full) profile, size (num_of_girls x num_of_object)
% @min_avg_s, @max_avg_s = are the max and min vals of height of the mean profile. and @all_max_s is the list of each cut .
% @girlsNames = list of strings, with the names of the girls.
% @diff_in_out = contains for each girl, ??? each object a list in size of 'sample_num_y'
% of all differnces between inner and outer 'x' on the mean profile.

numOfGirls = length(all_x_Prof);
numOfCuts = length(all_x_Prof{1}{1});
sample_num_y = length(diff_in_out{1}{1});
for j=1:numOfGirls
    N = length(all_x_Prof{j});
    for i = 8:N      
        %%%%%%%%%%%%%%%%%%%%%%%%%%% PROFILE PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%% different plot for each object %%%%%%%%%%%%%%%%%%
        prof_fig = figure;
        hold on;
        
         str = strcat('prof-', girlsNames{j}, '-', num2str(i));
%         for cutIdx = 1:numOfCuts         
%            plot (all_x_Prof{j}{i}{cutIdx}, all_y_Prof{j}{i}{cutIdx},'b');
%            hold on;
%         end
        plot(X_meanProf{j}{i}, Y_meanProf{j}{i}, 'r', X_meanProf{j}{i}, Y_meanProf{j}{i}, 'k.');
        title(str);
        
        saveas(gcf,[str '.png']);
        hold off;
        close(prof_fig)
        
        %%%%%%%%%%%%%%%%%%%%%%%% DIFF IN-OUT PLOT %%%%%%%%%%%%%%%%%%%%%%%%%
%         prof_fig = figure;
%         str = strcat('diff-', girlsNames{j}, '-', num2str(i));
%         all_space = abs(max_avg_s{j}{i} - min_avg_s{j}{i})
%         plot (diff_in_out{j}{i}, [min_avg_s{j}{i}:(all_space)/(sample_num_y - 1):max_avg_s{j}{i}] , 'b');
%         title(str);
%         
%         saveas(gcf,[str '.png']);
%         close(prof_fig)
    end
    
    %%%%%%%%%%%%%%%%%%%%%% DIFF MAX_S FROM MEAN %%%%%%%%%%%%%%%%%%%%%%%
%     prof_fig = figure; hold on;
%     str = strcat('diff_max-s-', girlsNames{j});
%     title(str);
%     N = length(all_x_Prof{j});
%     for i = 1:N
%         plot(repelem(i,numOfCuts), repelem(max_avg_s{j}{i},numOfCuts) - all_max_s{j}{i}(:)', 'b')
%         plot(repelem(i,numOfCuts), repelem(max_avg_s{j}{i},numOfCuts) - all_max_s{j}{i}(:)', 'r.')
% %         for k = 1:numOfCuts
% %             plot( i, max_avg_s{j}{i} - all_max_s{j}{i}(:), 'r*');
% %         end
%     end
%     saveas(gcf,[str '.png']);
%     close(prof_fig)
end
end