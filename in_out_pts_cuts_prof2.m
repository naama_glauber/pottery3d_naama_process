function [all_in_vals_xy, all_out_vals_xy, mean_val_xy] = in_out_pts_cuts_prof2( all_xy_Prof, mean_xy_prof, girlsNames ,numOfCuts, num_of_samples)
%UNTITLED Summary of this function goes here

find_dist = @( x, y, ref_x, ref_y ) ((x - ref_x)^2 + (y - ref_y)^2)^0.5;
%find_ortho_ab = @( x0, y0, x1, y1) [(-(x1-x0)/(y1-y0)), y0+((x1-x0)/(y1-y0))*x0];
find_ab = @( x0, y0, x1, y1) [(y1-y0)/(x1-x0), y0-((y1-y0)/(x1-x0))*x0];
% this number '10' is just to find a point that is far (outside the profile).
find_ortho_xy = @( x0, y0, x1, y1, sign ) [ 10*sign + x0, (-(x1-x0)*10*sign)/(y1-y0) + y0 ];

threshold_distance = 4;
numOfGirls = length(girlsNames);

cut_dist = cell(1,numOfGirls);
mean_val_xy = cell(1,numOfGirls);
all_in_vals_xy = cell(1,numOfGirls);
all_out_vals_xy = cell(1,numOfGirls);

for j=1:numOfGirls
   N = length(all_xy_Prof{j});
   cut_dist{j} = cell(1,N);
   mean_val_xy{j} = cell(1,N);
   all_in_vals_xy{j} = cell(1,N);
   all_out_vals_xy{j} = cell(1,N);
   for i=1:N
       cut_dist{j}{i} = cell(1,num_of_samples);
       mean_val_xy{j}{i} = zeros(num_of_samples, 2); % for x,y
       all_in_vals_xy{j}{i} = zeros(num_of_samples,2);
       all_out_vals_xy{j}{i} = zeros(num_of_samples,2);
       Y_meanProf = mean_xy_prof{j}{i}(:,2);
       X_meanProf = mean_xy_prof{j}{i}(:,1);
       [~, max_idx] = max(Y_meanProf);
       
       in_out_fig = figure; hold on;
       str = strcat('in-out-', girlsNames{j}, '-', num2str(i));
       for smpl_idx = 1:num_of_samples
           cut_dist{j}{i}{smpl_idx} = zeros(numOfCuts,3);
           cuts_to_count = 0;
           % x0, y0 = cur  point on mean curve
           % x1, y1 = next point on mean curve
           % The direction is depends if we are before max point, or after
           x0 = X_meanProf(smpl_idx);
           y0 = Y_meanProf(smpl_idx);
           if smpl_idx == num_of_samples % in case we are in the last point..
               x1 = x0;
               y1 = y0 -1;
           else
               x1 = X_meanProf(smpl_idx+1);
               y1 = Y_meanProf(smpl_idx+1);
           end
           sign = 1;
           if smpl_idx >= max_idx
               sign = -1;
           end
           xy_ortho = find_ortho_xy( x0, y0, x1, y1, sign);
           ab = find_ab(x0, y0, x1, y1);
           %ab_ortho = find_ab(x0, y0, xy_ortho(1), xy_ortho(2));
           
           %plot([x0, xy_ortho(1)],[y0, xy_ortho(2)], 'r.');
           %plot([x0, xy_ortho(1)],[y0, xy_ortho(2)], 'r');
           
           %syms x y;
           %eqn1 = -ab_ortho(1)*x + y == ab_ortho(2);
           for cut_idx = 1:numOfCuts
               % x0, y0 = cur  point on cut's curve
               % x1, y1 = next point on cut's curve
               x_cut = all_xy_Prof{j}{i}{cut_idx}(:,1);
               y_cut = all_xy_Prof{j}{i}{cut_idx}(:,2);
               
               cut_x0 = x_cut(smpl_idx);
               cut_y0 = y_cut(smpl_idx);
%                if smpl_idx == num_of_samples
%                    cut_x1 = x0;
%                    cut_y1 = y0 -1;
%                else
%                    cut_x1 = all_x_Prof{j}{i}{cut_idx}(smpl_idx+1);
%                    cut_y1 = all_y_Prof{j}{i}{cut_idx}(smpl_idx+1);
%                end           
%                ab_cut = find_ab(cut_x0, cut_y0, cut_x1, cut_y1);
%                eqn2 = (-ab_cut(1)*x + y == ab_cut(2));
%                [A,B] = equationsToMatrix([eqn1, eqn2], [x, y]);
%                TODO: check what I get when there is no solution!!!!
%                try 
%                    intersection_pt = linsolve(A,B);
%                catch
%                    intersection_pt = [cut_x0, cut_y0];
%                end

               %MAYBE TEMP??
               intersection_pt = [cut_x0, cut_y0];
               
               plot(x_cut, y_cut, 'color', [0.8,0.8,0.8]);
               plot(cut_x0,cut_y0, '.', 'color', [0.5,0.5,0.5]);
               
               % to check if we can count this cut, since some of the cuts
               % are not valid (start from the middle etc..)
               dist_to_count = find_dist(double(intersection_pt(1)), double(intersection_pt(2)), x0, y0);
               if dist_to_count > threshold_distance
                   cut_dist{j}{i}{smpl_idx}(cut_idx,:) = NaN;
                   continue;
               end
               cut_dist{j}{i}{smpl_idx}(cut_idx,1) = find_dist(double(intersection_pt(1)), double(intersection_pt(2)), xy_ortho(1), xy_ortho(2));
               cut_dist{j}{i}{smpl_idx}(cut_idx, 2:3 ) = [cut_x0, cut_y0]; % save the values of the intersection between the cut and orthogonal vector
               new_x = double(intersection_pt(1));
               new_y = double(intersection_pt(2));
               mean_val_xy{j}{i}(smpl_idx,1) = mean_val_xy{j}{i}(smpl_idx,1) + new_x;
               mean_val_xy{j}{i}(smpl_idx,2) = mean_val_xy{j}{i}(smpl_idx,2) + new_y;
               cuts_to_count = cuts_to_count + 1;
           end
           if cuts_to_count == 0
                mean_val_xy{j}{i}(smpl_idx,1) = x0;
                mean_val_xy{j}{i}(smpl_idx,2) = y0;
           else
                mean_val_xy{j}{i}(smpl_idx,:) = mean_val_xy{j}{i}(smpl_idx,:)./cuts_to_count;
           end
           
           %if (ab(1) >= 0 && smpl_idx <= max_idx) || (ab(1) <= 0 && smpl_idx >= max_idx)
           [~, in_cut_idx] = min(cut_dist{j}{i}{smpl_idx}(:,1));
           [~, out_cut_idx] = max(cut_dist{j}{i}{smpl_idx}(:,1));
           %else
           %    [~, in_cut_idx] = max(cut_dist{j}{i}{smpl_idx}(:,1));
           %    [~, out_cut_idx] = min(cut_dist{j}{i}{smpl_idx}(:,1));
           %end
           
           out_xy = cut_dist{j}{i}{smpl_idx}(in_cut_idx, 2:3);
           in_xy = cut_dist{j}{i}{smpl_idx}(out_cut_idx, 2:3);
           all_in_vals_xy{j}{i}(smpl_idx, :) = in_xy;
           all_out_vals_xy{j}{i}(smpl_idx, :) = out_xy; 
           
           plot(in_xy(1), in_xy(2), 'b*');
           plot(out_xy(1), out_xy(2),'r*');
           %plot(mean_val_xy{j}{i}(smpl_idx,1), mean_val_xy{j}{i}(smpl_idx,2), 'g.'); 
       end
       plot(X_meanProf, Y_meanProf, 'g');
       plot(X_meanProf, Y_meanProf, 'g.');
       title(str);
       saveas(gcf,[str '.png']);
       close(in_out_fig);
       %hold off;
   end
end
end

   
