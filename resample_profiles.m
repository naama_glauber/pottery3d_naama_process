function sampled_prof = resample_profiles(all_profs, potters_names, p, fs, is_ploting)
    sampled_prof = cell(size(all_profs));
    p = p + 2*fs;
    %beta = 1.5; % beta=free parameter for Kaisar filter (>0), higher value for more centric
    for j=1:length(all_profs)
        n_objects = length(all_profs{j});
        for i=1:n_objects
            % y =  resample(x,p,q,fs,beta) resamples the input sequence, x, at p/q times the original sample rate.
            u_x = all_profs{j}{i}(:,1);
            u_y = all_profs{j}{i}(:,2);
            % append last point and first point for smoothing convolution
            xpad = [repmat(u_x(1), fs*2, 1); u_x; repmat(u_x(end), fs*2, 1)];
            ypad = [repmat(u_y(1), fs*2, 1); u_y; repmat(u_y(end), fs*2, 1)];
            q = length(xpad);
            % SMOOTH BY CONVOLUTION
            w = gausswin(fs);
            w = w/sum(w);
            xpad_smooth = filter(w,1,xpad);
            ypad_smooth = filter(w,1,ypad);
            % RESAMPLING
            smooth_x = resample(xpad_smooth,p,q,fs);
            sampled_x = smooth_x(fs*2:p-fs-1);
            start_x = all_profs{j}{i}(1:floor(q/p):floor(1.5*fs-1), 1);
            top_size = floor((floor(length(all_profs{j}{i}(:,1))*p/q)-length(sampled_x)-length(start_x)-1)*(q/p));
            orig_size = length(all_profs{j}{i}(:,1));
            end_x = all_profs{j}{i}(orig_size-top_size:floor(q/p):end, 1);
            
            smooth_y = resample(ypad_smooth,p,q,fs);
            sampled_y = smooth_y(fs*2:p-fs-1);
            start_y =  all_profs{j}{i}(1:floor(q/p):floor(1.5*fs-1), 2);
            end_y = all_profs{j}{i}(orig_size-top_size:floor(q/p):end, 2);
            
            new_x = [sampled_x; end_x]; % [start_x; sampled_x; end_x]
            new_y = [sampled_y; end_y]; % [start_y; sampled_x; end_x]
            sampled_prof{j}{i} = [new_x, new_y];

            % check resampling on profile- 
            if nargin > 3 && is_ploting
                hfig = figure; hold on;
                
                plot(sampled_x, sampled_y, 'r')
                plot(all_profs{j}{i}(:,1), all_profs{j}{i}(:,2), 'k.')
                plot(sampled_x, sampled_y, 'r.')
                plot(start_x, start_y, 'g.')
                plot(end_x, end_y, 'g.')
                xlim([65 115])
                ylim([-65 5])
                title(strcat('profile: ', num2str(j), '-', num2str(i), ', orig: k, sampled: r'));
                fig_path = strcat('.\indian_potters_files\results\resampling\fs', num2str(fs) ,'rsmpl_prof_' , potters_names{j}, '-', num2str(i) ,'.jpg');
                saveas(hfig, fig_path);
                close(hfig);
            end
        end
    end
    clear('p', 'n_objects', 'u_x', 'u_y', 'xpad', 'ypad', 'q', 'w', 'xpad_smooth', 'ypad_smooth', 'sampled_x', 'sampled_y', 'hfig', 'fig_path');
end

