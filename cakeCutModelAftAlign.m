function [ newV_cells, newVidx_cells] = cakeCutModelAftAlign( V_fixed, numOfCuts )
%cakeCutModelAftAlign:
%V_fixed: a 3D-model (mesh) of the (X,Y,Z) point representing the pottery
%object (after alignment on the rotation axis). i.e. model is centered
%around z axis..
%numOfCuts:

angStep = 360/numOfCuts;
newV_cells = cell(numOfCuts,1);
newVidx_cells = cell(numOfCuts,1);
for v_idx = 1:size(V_fixed, 1)
    x = V_fixed(v_idx,1);
    y = V_fixed(v_idx,2);
    v_ang = radtodeg(atan(y/x));
    if x<0
        v_ang = 180 + v_ang;
    else
        v_ang = mod(360 + v_ang, 360);
    end
    
    % depends on the angle, we put this point in a different cell.
    cell_of_v = fix(v_ang/angStep)+1;
    v_curr = cell(1,3);
    v_curr = {V_fixed(v_idx,:)};
    
    v_curr_cell = newV_cells{cell_of_v};
    v_idx_curr_cell = newVidx_cells {cell_of_v};
    
    newV_cells{cell_of_v} = [v_curr_cell, v_curr];
    newVidx_cells{cell_of_v} = [v_idx_curr_cell, v_idx];
end

