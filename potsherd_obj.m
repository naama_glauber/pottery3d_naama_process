classdef potsherd_obj
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        V_lst
        V_align
        n_lst % normals lst
        n_align
        f_lst
        skn_lst % neighbors
        
        mean_profile_xy
        
        min_height_cuts
        max_height_cuts
        min_height_mean_profile
        max_height_mean_profile
        
        all_cuts_vals = cell(1,number_of_cuts) % list of cut_vals_obj
        num_of_cuts
    end
    
    methods
        function [ newV_cells, newVidx_cells] = cakeCutModelAftAlign( V_aft_align, numOfCuts )
            %cakeCutModelAftAlign:
            %V_fixed: a 3D-model (mesh) of the (X,Y,Z) point representing the pottery
            %object (after alignment on the rotation axis). i.e. model is centered
            %around z axis..
            %numOfCuts:
            angStep = 360/numOfCuts;
            newV_cells = cell(numOfCuts,1);
            newVidx_cells = cell(numOfCuts,1);
            for v_idx = 1:size(V_aft_align, 1)
                x = V_aft_align(v_idx,1);
                y = V_aft_align(v_idx,2);
                v_ang = radtodeg(atan(y/x));
                if x<0
                    v_ang = 180 + v_ang;
                else
                    v_ang = mod(360 + v_ang, 360);
                end

                % depends on the angle, we put this point in a different cell.
                cell_of_v = fix(v_ang/angStep)+1;
                v_curr = cell(1,3);
                v_curr = {V_aft_align(v_idx,:)};

                v_curr_cell = newV_cells{cell_of_v};
                v_idx_curr_cell = newVidx_cells {cell_of_v};

                newV_cells{cell_of_v} = [v_curr_cell, v_curr];
                newVidx_cells{cell_of_v} = [v_idx_curr_cell, v_idx];
            end
        end
    end
end

