function [x0,y0,r,Index,W]=FIND_CIRCLE_1(x,y)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should fit a circle to a noisy set of points using the
% parametic form of the circle and not the squared distances as we did in
% the past
% Then, one itteration is done in order the improve the results, where we
% look in the direction of the gradient for better guess.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if length(x)>5
% Switching to culumns input
if length(x(1,:))>length(x(:,1))
    x=x';
    y=y';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creating the matrices
A1(:,1)=x.^2;  A2(:,1)=x.*y;   A3(:,1)=x;    
A1(:,2)=x.*y;  A2(:,2)=y.^2;   A3(:,2)=y;  
A1(:,3)=x;     A2(:,3)=y;      A3(:,3)=ones(length(x),1);   
B1=-x.*(x.^2+y.^2);         B2=-y.*(x.^2+y.^2);       B3=-(x.^2+y.^2);    
A=[A1;A2;A3];
B=[B1;B2;B3];
X=A\B;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computing the circle parameters
x0=-0.5*X(1);
y0=-0.5*X(2);
r=sqrt(0.25*(X(1)^2+X(2)^2)-X(3));
Hi=sum((sqrt((x-x0).^2+(y-y0).^2)-r).^2);
newHi=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ignoring outliers
Index=(1:length(x));
R=sqrt((x-x0).^2+(y-y0).^2);
W=(R-r)/std(R);
if max(abs(W))>2
    A1=[];A2=[];A3=[];
    I_delete=find(abs(W)>prctile(abs(W),90));
    Index(I_delete)=[];
    A1(:,1)=x(Index).^2;            A2(:,1)=x(Index).*y(Index);   A3(:,1)=x(Index);    
    A1(:,2)=x(Index).*y(Index);     A2(:,2)=y(Index).^2;          A3(:,2)=y(Index);  
    A1(:,3)=x(Index);               A2(:,3)=y(Index);             A3(:,3)=ones(length(x(Index)),1);   
    B1=-x(Index).*(x(Index).^2+y(Index).^2);         
    B2=-y(Index).*(x(Index).^2+y(Index).^2);       
    B3=-(x(Index).^2+y(Index).^2);        
    A=[A1;A2;A3];
    B=[B1;B2;B3];
    X=A\B;
    x0=-0.5*X(1);
    y0=-0.5*X(2);
    r=sqrt(0.25*(X(1)^2+X(2)^2)-X(3));
    Hi=sum((sqrt((x(Index)-x0).^2+(y(Index)-y0).^2)-r).^2);
    R=sqrt((x(Index)-x0).^2+(y(Index)-y0).^2);
    W=(R-r)/std(R);
end
while max(abs(W))>2.5
    A1=[];A2=[];A3=[];
    I_delete= abs(W)>2.5;
    Index(I_delete)=[];
    A1(:,1)=x(Index).^2;            A2(:,1)=x(Index).*y(Index);   A3(:,1)=x(Index);    
    A1(:,2)=x(Index).*y(Index);     A2(:,2)=y(Index).^2;          A3(:,2)=y(Index);  
    A1(:,3)=x(Index);               A2(:,3)=y(Index);             A3(:,3)=ones(length(x(Index)),1);   
    B1=-x(Index).*(x(Index).^2+y(Index).^2);         
    B2=-y(Index).*(x(Index).^2+y(Index).^2);       
    B3=-(x(Index).^2+y(Index).^2);        
    A=[A1;A2;A3];
    B=[B1;B2;B3];
    X=A\B;
    x0=-0.5*X(1);
    y0=-0.5*X(2);
    r=sqrt(0.25*(X(1)^2+X(2)^2)-X(3));
    Hi=sum((sqrt((x(Index)-x0).^2+(y(Index)-y0).^2)-r).^2);
    R=sqrt((x(Index)-x0).^2+(y(Index)-y0).^2);
    W=(R-r)/std(R);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ploting the results
% t=-pi:pi/200:pi;
% X=r*cos(t)+x0;
% Y=r*sin(t)+y0;
% plot(x0,y0 ,'b+',x,y,'b.',X,Y,'r');axis equal;hold on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xx=x(Index);yy=y(Index);
r=mean(sqrt((xx-x0).^2+(yy-y0).^2));
delta=[r;r];
% Gradient calculations
while norm(delta)>(r/5000)
    R=sqrt((xx-x0).^2+(yy-y0).^2);
    RR=(R*(1./R'))';
    N=length(xx);
    Xi=(xx-x0)*ones(1,N);
    Xij=(xx-x0)*(xx-x0)';
    Yi=(yy-y0)*ones(1,N);
    Yij=(yy-y0)*(yy-y0)';
    F=(2/N)*sum(sum(Xi.*RR))-2*sum(xx-x0);
    G=(2/N)*sum(sum(Yi.*RR))-2*sum(yy-y0);
    dFdX=(2/N)*sum(sum(((Xi.^2).*RR-(R*R')-RR'.*Xij)./((R.^2)*ones(1,N))))+2*N;
    dFdY=(2/N)*sum(sum(((Xi.*Yi).*RR-RR'.*(Xi.*Yi'))./((R.^2)*ones(1,N))));
    dGdX=dFdY;
    dGdY=(2/N)*sum(sum(((Yi.^2).*RR-(R*R')-RR'.*Yij)./((R.^2)*ones(1,N))))+2*N;
    delta=inv([dFdX dFdY;dGdX dGdY])*[-F;-G];
%     disp([delta' norm(delta)])
%     plot(x0,y0,'r+')
    x0=x0+delta(1);
    y0=y0+delta(2);
    r=mean(sqrt((xx-x0).^2+(yy-y0).^2));
    R=sqrt((xx-x0).^2+(yy-y0).^2);
    W=std(R)/mean(R);
    newHi=sum((sqrt((xx-x0).^2+(yy-y0).^2)-r).^2);
    if newHi<Hi
        Hi=newHi;
    else
        x0=x0-delta(1);
        y0=y0-delta(2);
        r=mean(sqrt((xx-x0).^2+(yy-y0).^2));
        delta=[0;0];
        R=sqrt((xx-x0).^2+(yy-y0).^2);
        W=std(R)/mean(R);
    end
end
else
    x0=[];y0=[];r=[];Index=[];W=[];
end
% plot(x,y,'.',xx,yy,'r.');axis equal
% X=r*cos(t)+x0;
% Y=r*sin(t)+y0;
% plot(X,Y,'k')
% plot(x0,y0,'k+')
% grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%