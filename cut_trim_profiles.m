
function trim_profs = cut_trim_profiles(all_profs, potters_names, y_threshold)
    trim_profs = cell(size(all_profs));
    for j=1:length(all_profs)
        fig=figure;hold on;
        title("Trimmed,  ALL  " + potters_names{j})        
        n_objects = length(all_profs{j});
        for i=1:n_objects
            X = all_profs{j}{i}(:,1); %.ext_prof.X;
            Y = all_profs{j}{i}(:,2); %.ext_prof.Y;
            [~, idx_top] = max(Y);
            % 2.1 TRIM take only indexs before this point (outside part of the pot)
            X = X(1:idx_top);
            Y = Y(1:idx_top);
            %plot(X, Y, 'b')
            
            % 2.2 TRIM in y~-60, because we want the alignment to be according to
            % the rim and neck and not the whole pot!
            [~, idx_Xmax] = max(X);
            X = X(idx_Xmax:end);
            Y = Y(idx_Xmax:end);
            
            trim_profs{j}{i} = [X, Y];
            idxes_totake = Y > y_threshold;
            trim_profs{j}{i} = trim_profs{j}{i}(idxes_totake,:);
            plot(trim_profs{j}{i}(:,1), trim_profs{j}{i}(:,2)); %, 'r'
        end
        dir_path = ".\Mean profile\"; % '.\indian_potters_files\results\all_profs_plot\'
        fig_path = dir_path + potters_names{j} + "_trimed.jpg";
        if ~exist(dir_path, 'dir')
            mkdir(dir_path)
        end
        saveas(fig,char(fig_path));
    end
end

