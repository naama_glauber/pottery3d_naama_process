
% HINDU:  'Lashman', 'Jera-Ram', 'Mangilal', 'Pemaram', 'Devilal', 'Govindram', 'Hajiram', 'Udaram', 'Zorilal'
% MUSLIM: 'Anwar-Khan', 'Ramzan', 'Sakur', 'Sokat', 'Azruddin', 'Ramdayal', 'Aksar', 'Razak', 'Samir', 'Youssouf', 'Fazrudin', 'Mazid', 'Amin', 'Gani'
dir_path = ".\Mean profile\";
fig_files = dir ('**/*.fig');
mean_profs_hindu = cell(9,1);
mean_profs_muslim = cell(14,1);
mean_profs_names = cell(length(fig_files),1);

hindu_i = 1;
muslim_i = 1;
for i=1:length(fig_files)
     %Check for correct directory.
     %if(~isempty(regexp(fig_files(i).folder)))
     path = strcat(fig_files(i).folder,'\',fig_files(i).name);
     fig = openfig( path );
     axObjs = fig.Children;
     dataObjs = axObjs.Children;

     x = dataObjs(1).XData;
     y = dataObjs(1).YData;
     nn = fig_files(i).name;
     
     formatSpec = "%f,%f\n";
     txtfile_path = strcat(fig_files(i).folder,'\',nn(1:end-4), '.txt');
     fileID = fopen(txtfile_path, 'w');
     str = compose(formatSpec,x',y');
     
     Xs = zeros(length(str), 1);
     Ys = zeros(length(str), 1);
     for j=1:length(str)
        fprintf(fileID , str(j));
        C = strsplit(str(1),",");
        Xs(j) = str2double(C{1});
        Ys(j) = str2double(C{2});
     end
     
     if i < 10
        mean_profs_hindu{hindu_i} = [x', y'];
        hindu_i = hindu_i + 1;
     else
        mean_profs_muslim{muslim_i} = [x', y'];
        muslim_i = muslim_i + 1;
     end
     
     mean_profs_names{i} = nn(1:end-4);
     fclose(fileID);
     close(fig)
end

figure;
title("All Hindu")
for p_i=1:length(mean_profs_hindu)
    x = mean_profs_hindu{p_i}(:,1);
    y = mean_profs_hindu{p_i}(:,2);
    hold on; plot(x,y);
end

figure;
title("All Muslim")
for p_i=1:length(mean_profs_muslim)
    x = mean_profs_muslim{p_i}(:,1);
    y = mean_profs_muslim{p_i}(:,2);
    hold on; plot(x,y);
end

% 2.1 CUT THE LEFT SIDE TO HAVE ONLY THE OUSIDE PROFILE
% 2.2 CUT THE BOTTOM OF THE PROFILE (just a bit lower from the neck)
% to check where to cut, first we plot all profiles of the same potters
group_names = {'Hindu', 'Muslim'};
y_threshold = -60;
all_profs = cell(2,1);
all_profs{1} = mean_profs_hindu;
all_profs{2} = mean_profs_muslim;
trim_profs = cut_trim_profiles(all_profs, group_names, y_threshold);

% ICP PROFILES:
is_indians = true;
is_ploting = true;
potters_obj_names = cell(2,1);
potters_obj_names{1} = cell(9,1);
potters_obj_names{1} = {'Lashman', 'Jera-Ram', 'Mangilal', 'Pemaram', 'Devilal', 'Govindram', 'Hajiram', 'Udaram', 'Zorilal'};
potters_obj_names{2} = cell(14,1);
potters_obj_names{2} = {'Anwar-Khan', 'Ramzan', 'Sakur', 'Sokat', 'Azruddin', 'Ramdayal', 'Aksar', 'Razak', 'Samir', 'Youssouf', 'Fazrudin', 'Mazid', 'Amin', 'Gani'};

[all_Dicp, all_Ricp, all_Ticp, all_ERs] = ICP_align_profs(all_profs, potters_obj_names, group_names, is_ploting, char(dir_path));
[trimed_Dicp, trimed_Ricp, trimed_Ticp, trimed_ERs] = ICP_align_profs(trim_profs, potters_obj_names, group_names, is_ploting, char(dir_path));

% TODO 
% cutrim_y_ranges: 'AMIN' -25,  'GANI' -30, others -20
y_cut_bottom = [-20,-20];
filter_size = 9;   % Gaussian window
desired_size = 99;

trimed_sampled_Dicp = resample_profiles(trimed_Dicp, group_names, desired_size, filter_size, is_ploting);

[trm_icp_RN_pts,trm_icp_NB_pts, trm_icp_Rtop_pts, trm_icp_Rright_pts, trm_icp_Nleft_pts] = calc_plot_curv_norm(trimed_Dicp, trimed_sampled_Dicp, group_names, y_cut_bottom , is_ploting);
[R_ylen, N_ylen_1, N_ylen_2, N_xdepth, N_xtotdepth, RN_loc, NB_loc, Rright_loc] = length_location_statistics(potters_names, trm_icp_RN_pts,trm_icp_NB_pts, trm_icp_Rtop_pts, trm_icp_Rright_pts, trm_icp_Nleft_pts);

%all_profs = trm_icp_smp17_prof; %trimed_Dicp
%save_rhino_profs_pts

clear('i','j','x','y','hindu_i','muslim_i','formatSpec','str','nn','path','txtfile_path','axObjs','dataObjs','fig')


