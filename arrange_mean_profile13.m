function [x,y,qual]=arrange_mean_profile13(v,n,min_step,max_step)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should be a combination of the two series of functions:
% 1)draw_mean_profile 
% 2)arrange_profile

% Within this set of functions from this version on, there is a try to have
% a quality factor of the shard alignment. This quality is measured in
% terms of the distribution of the points within each rectangle, which
% means the spread of points along the normal of each point.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isvector(min_step)
    min_step=min_step;
else
    min_step=0.5;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rotate the fragment to be sure its on the positive X-axis.
Alpha=atan2(v(:,2),v(:,1));
if max(Alpha)-min(Alpha)<4
    M=mean(v);
    teta=atan2(M(2),M(1));
    v1=cos(teta)*v(:,1)+sin(teta)*v(:,2);
    v2=-sin(teta)*v(:,1)+cos(teta)*v(:,2);
    v3=v(:,3);
    v=[v1 v2 v3];
    v1=cos(teta)*n(:,1)+sin(teta)*n(:,2);
    v2=-sin(teta)*n(:,1)+cos(teta)*n(:,2);
    n=[v1 v2 n(:,3)];
    AlphaFlag=0;
else
    AlphaFlag=1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ignoring irrelevant points for which the normals are far from the z-axis
L=length(v(:,1));
ax=[zeros(L,2) ones(L,1)];
q1=cross(n,ax);
q2=sum(-v'.*q1');
D=abs(q2)./norm(q1);
% if prctile(D,70)<0.05
%     D_par=0.05;
% else
%     D_par=prctile(D,40);
% end
% D_par=prctile(D,60);
D_par=0.07;
D_I=find(D<D_par);
original_index=1:length(v);
original_index=original_index(D_I);
D=D(D_I);
%%%%%%%%%%%%%%%%%%%%%%
X=sqrt(v(D_I,1).^2+v(D_I,2).^2);
Y=v(D_I,3);
% X2=sqrt(v(D_I2,1).^2+v(D_I2,2).^2);
% Y2=v(D_I2,3);
NY=n(D_I,3);
NX=sqrt(n(D_I,1).^2+n(D_I,2).^2);
if AlphaFlag
    XI=[];
    for i=1:length(NX)
        if norm(v(D_I(i),1:2)+n(D_I(i),1:2))<norm(v(D_I(i),1:2))
            NX(i)=-NX(i);
            XI(end+1)=i;
        end
    end
else
    NX=NX.*sign(n(D_I,1));
end
% plot(X,Y,'k.');axis equal;hold on
line=zeros(2,length(X));
%%%%%%%%%%%%%%%%%%%%%%
% defind the first (highest) point
x_profile=X;
y_profile=Y;
L=length(x_profile);
[ysort,I]=sort(y_profile);
% D=sqrt((x_profile(I(1:end-1))-x_profile(I(end))).^2+(y_profile(I(1:end-1))-y_profile(I(end))).^2);
NNx=0;
NNy=1;
Sx=x_profile(I(end));
Sy=y_profile(I(end));
len=2;wid=2*min_step;
malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
Idel=find(INOUT);
last_norm_right=[mean(NX(Idel));mean(NY(Idel))];
last_norm_right=last_norm_right/norm(last_norm_right);
NNx=last_norm_right(1);
NNy=last_norm_right(2);
Sx=mean(x_profile(Idel));
Sy=mean(y_profile(Idel));
len=1.5;wid=2*min_step;
last_wid_right=wid;
last_wid_left=wid;
malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
Idel=find(INOUT);
mid_index=fix(L/2);
line(1,mid_index)=mean(x_profile(Idel));
line(2,mid_index)=mean(y_profile(Idel));
DD(mid_index)=mean(D(Idel));
NORM(:,mid_index)=last_norm_right;
% plot(malben_x,malben_y,'k');
% alpha(0.5);
% plot([Sx Sx+3*NNx],[Sy Sy+3*NNy]);
%%%%%%%%%%%%%%%%%%
% The quality of the first rectangle
qual(1,mid_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*last_norm_right);
% qual(2,mid_index)=2*wid;
%%%%%%%%%%%%%%%%%%
% keep the original index of the points for each new point
OI(mid_index).I=original_index(Idel);
%%%%%%%%%%%%%%%%%%
x_profile(Idel)=[];
y_profile(Idel)=[];
NX(Idel)=[];
NY(Idel)=[];
original_index(Idel)=[];
D(Idel)=[];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% searching for the following points
right_index=mid_index;
left_index=mid_index;
last_norm_right=[NNx;NNy];
last_norm_left=[NNx;NNy];
right_end=0;left_end=0;
while L>0
    if right_end==0
        right_direction=[last_norm_right(2);-last_norm_right(1)];
        NNx=last_norm_right(1);
        NNy=last_norm_right(2);
        Sx=line(1,right_index)+last_wid_right*right_direction(1);
        Sy=line(2,right_index)+last_wid_right*right_direction(2);
        DO=[];N=[];SX=[];SY=[];IDEL=[];in_right=[];
        wid=max_step;
        while min_step<wid
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                new_norm_right=[mean(NX(Idel));mean(NY(Idel))];
                new_norm_right=new_norm_right/norm(new_norm_right);
                Q=[];
                for i=1:length(Idel)
                    Q(i)=dot(new_norm_right,[NX(Idel(i));NY(Idel(i))])/norm([NX(Idel(i));NY(Idel(i))]);
                end
                IIdel=find(Q>0.9);
                IDEL.Idel=Idel';
                if length(IIdel)>0
                    IIdel=IIdel;
                else
                    IIdel=find(Q>prctile(Q,60));
                end
                new_norm_right=[mean(NX(Idel(IIdel)));mean(NY(Idel(IIdel)))];
                new_norm_right=new_norm_right/norm(new_norm_right);
                DO=dot(last_norm_right,new_norm_right);
                N=new_norm_right;
                SX=mean(x_profile(Idel(IIdel)));
                SY=mean(y_profile(Idel(IIdel)));
                if DO>0.99
                    in_right=wid;
                    break
                else
                    in_right=wid;
                    wid=wid-0.1;
                end
            else
                wid=wid-0.1;
                if length(in_right)>0
                    DO=0;
                else
                    DO=-1;
                end
            end
        end
        if DO>-1
            if wid<min_step
                wid=min_step;
            end
            last_wid_right=wid;
            Sx=SX;
            Sy=SY;
            NNx=N(1);
            NNy=N(2);
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                Idel=Idel;
            else
                Idel=union(Idel,IDEL.Idel);
            end
%             plot(malben_x,malben_y,'r');
%             alpha(0.5);
%             plot([Sx Sx+3*NNx],[Sy Sy+3*NNy])
            right_index=right_index-1;
            line(1,right_index)=Sx;
            line(2,right_index)=Sy;
            DD(right_index)=mean(D(Idel));
            NORM(:,right_index)=N;
            OI(right_index).I=original_index(Idel);
            %%%%%%%%%%%%%%%%%%
            % The quality of the current rectangle
            qual(1,right_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*new_norm_right);
            perp=[-new_norm_right(2);new_norm_right(1)];
            %%%%%%%%%%%%%%%%%%
            x_profile(Idel)=[];
            y_profile(Idel)=[];
            NX(Idel)=[];
            NY(Idel)=[];
            original_index(Idel)=[];
            D(Idel)=[];
            last_norm_right=new_norm_right;
        else
            right_end=1;
        end
    end

    %%%%%%%%%%%%%%%%%
    if left_end==0
        left_direction=[-last_norm_left(2);last_norm_left(1)];
        NNx=last_norm_left(1);
        NNy=last_norm_left(2);
        Sx=line(1,left_index)+last_wid_left*left_direction(1);
        Sy=line(2,left_index)+last_wid_left*left_direction(2);
        DO=[];N=[];SX=[];SY=[];IDEL=[];in_left=[];
        wid=max_step;
        while min_step<wid
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if ~isempty(Idel)
                new_norm_left=[mean(NX(Idel));mean(NY(Idel))];
                new_norm_left=new_norm_left/norm(new_norm_left);
                Q=[];
                for i=1:length(Idel)
                    Q(i)=dot(new_norm_left,[NX(Idel(i));NY(Idel(i))])/norm([NX(Idel(i));NY(Idel(i))]);
                end
                IIdel=find(Q>0.9);
                IDEL.Idel=Idel';
                if isempty(IIdel)
                    IIdel=find(Q>prctile(Q,60));
                end
                new_norm_left=[mean(NX(Idel(IIdel)));mean(NY(Idel(IIdel)))];
                new_norm_left=new_norm_left/norm(new_norm_left);
                DO=dot(last_norm_left,new_norm_left);
                N=new_norm_left;
                SX=mean(x_profile(Idel(IIdel)));
                SY=mean(y_profile(Idel(IIdel)));
                if DO>0.99
                    in_left=wid;
                    break
                else
                    in_left=wid;
                    wid=wid-0.1;
                end
            else
                wid=wid-0.1;
                if ~isempty(in_left)
                    DO=0;
                else
                    DO=-1;
                end
            end
        end
        if DO>-1
            if wid<min_step
                wid=min_step;
            end
            last_wid_left=wid;
            Sx=SX;
            Sy=SY;
            NNx=N(1);
            NNy=N(2);
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if isempty(Idel)
                Idel=union(Idel,IDEL.Idel);
            end
%             plot(malben_x,malben_y,'k');
%             alpha(0.5);
%             plot([Sx Sx+3*NNx],[Sy Sy+3*NNy])
            left_index=left_index+1;
            line(1,left_index)=Sx;
            line(2,left_index)=Sy;
            DD(left_index)=mean(D(Idel));
            NORM(:,left_index)=N;
            OI(left_index).I=original_index(Idel);
            %%%%%%%%%%%%%%%%%%
            % The quality of the current rectangle
            qual(1,left_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*new_norm_left);
            perp=[-new_norm_left(2);new_norm_left(1)];
            %%%%%%%%%%%%%%%%%%
            x_profile(Idel)=[];
            y_profile(Idel)=[];
            NX(Idel)=[];
            NY(Idel)=[];
            original_index(Idel)=[];
            D(Idel)=[];
            last_norm_left=new_norm_left;
        else
            left_end=1;
        end
    end
    y1=line(2,left_index);
    y2=line(2,right_index);
    Idel=find(y_profile>max(y1,y2)+5);
    x_profile(Idel)=[];
    y_profile(Idel)=[];
    NX(Idel)=[];
    NY(Idel)=[];
    original_index(Idel)=[];
    D(Idel)=[];
    if right_end==1 && left_end==1
        L=0;
    else
        L=length(x_profile);
    end
end
I=find(line(1,:)>0);
x=line(1,I);
y=line(2,I);
qual=qual(:,I);
OI=OI(I);
DD=DD(I);
NORM=NORM(:,I);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Searching for the cutting points of the break
% clear('C1','C2');
% A=NORM'*NORM;
% par=7;
% B=[A(:,end-par+1:end) A A(:,1:par)];
% t=length(A(:,1));
% for i=1:t
%     I1=par+1+i:par+7+i;
%     I2=par-7+i:par-1+i;
%     C1(i)=mean(B(i,I1));
%     C2(i)=mean(B(i,I2));
% end
% C=C1-C2;
% s=New_arc_length(x,y);
% [z1,z2]=max(y);
% s=s-s(z2);
% TH=thickness2(x,y);
% I1=find([TH.thickness]<prctile([TH.thickness],75) & [TH.thickness]>prctile([TH.thickness],15));
% I2=find([TH.distance]<prctile([TH.distance],75));
% I=intersect(I1,I2);
% TH=mean([TH.thickness(I)]);
qual=10*qual;

% if abs(s(end))<abs(s(1))+1.5*TH
%     while abs(s(end))<abs(s(1))+1.5*TH
%         x=[x(2:end) x(1)];
%         y=[y(2:end) y(1)];
%         s=New_arc_length(x,y);
%         [z1,z2]=max(y);
%         s=s-s(z2);
%         qual=[qual(:,2:end) qual(:,1)];
%         NORM=[NORM(:,2:end) NORM(:,1)];
%         C1=[C1(2:end) C1(1)];
%         C2=[C2(2:end) C2(1)];
%         C=C1-C2;
%     end
% end
% if abs(s(1))<s(end)+1.5*TH
%     while abs(s(1))<s(end)+1.5*TH
%         x=[x(end) x(1:end-1)];
%         y=[y(end) y(1:end-1)];
%         s=New_arc_length(x,y);
%         [z1,z2]=max(y);
%         s=s-s(z2);
%         qual=[qual(:,end) qual(:,1:end-1)];
%         NORM=[NORM(:,end) NORM(:,1:end-1)];
%         C1=[C1(end) C1(1:end-1)];
%         C2=[C2(end) C2(1:end-1)];
%         C=C1-C2;
%     end
% end
% 
% CC=C;
% I=find(abs(CC)<0.2);
% CC(I)=zeros(1,length(I));
% I=find(abs(s)<0.65*s(end));
% CC(I)=zeros(1,length(I));
% t=length(x);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MI=find(imregionalmax(CC) & CC>0 & C>0.2);
% if ismember(1,MI) && CC(1)<CC(end)
%     MI(1)=[];
% end
% if ismember(t,MI) && CC(1)>CC(end)
%     MI(end)=[];
% end
% worning=0;
% M=CC(MI);
% if ~isempty(M)
%     [z1,z2]=sort(M);
%     MI=MI(z2);
%     start=MI(end);
%     if MI(end)<t/2
%         f_mi=find(MI(end)+2*round(TH/max_step)>MI & MI<t/2 & MI(end)<MI);
%         if ~isempty(f_mi)
%             [z3,z4]=sort(MI(f_mi));
%             start=max(MI(f_mi));
%         end
%     else
%         f_mi=find(MI<2*round(TH/max_step) | MI>MI(end));
%         if ~isempty(f_mi)
%             I=find(MI(f_mi)<t/2);
%             if ~isempty(I)
%                 start=max(MI(f_mi(I)));
%             else
%                 start=max(MI(f_mi));
%             end
%         end
%     end
% else
%     start=1;sof=length(x);worning=1;
% end
% if worning==0
%     MI=find(imregionalmin(CC) & CC<0 & C<-0.2);
%     if ismember(1,MI) && CC(1)>CC(end)
%         MI(1)=[];
%     end
%     if ismember(t,MI) && CC(1)<CC(end)
%         MI(end)=[];
%     end
%     M=CC(MI);
%     if ~isempty(M)        
%         [z1,z2]=sort(M);
%         MI=MI(z2);
%         sof=MI(1);
%         if MI(1)>t/2
%             f_mi=find(MI+2*round(TH/max_step)>MI(1) & MI>t/2 & MI<MI(1));
%             if ~isempty(f_mi)
%                 sof=min(MI(f_mi));
%             end
%         else
%             f_mi=find(MI+2*round(TH/max_step)>t | MI<MI(1));
%             if ~isempty(f_mi)
%                 I=find(MI(f_mi)>t/2);
%                 if ~isempty(I)
%                     sof=min(MI(f_mi(I)));
%                 else
%                     sof=min(MI(f_mi));
%                 end
%             end
%         end
%     else
%         start=1;sof=length(x);
%     end
% else
%     start=1;sof=length(x);
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % subplot(1,2,1);
% % plot(x,y,'b.');axis equal;hold on
% if start<t/2
%     if sof>t/2
%         x=x(start:sof);
%         y=y(start:sof);
%         qual=qual(start:sof);
%     else
%         x=[x(start:end) x(1:sof)];
%         y=[y(start:end) y(1:sof)];
%         qual=[qual(start:end) qual(1:sof)];
%     end
% else
%     x=[x(start:end) x(1:sof)];        
%     y=[y(start:end) y(1:sof)];
%     qual=[qual(start:end) qual(1:sof)];
% end

% plot(x,y,'k',x,y,'k.',x(1),y(1),'ro',x(end),y(end),'mo');axis equal
% subplot(1,2,2);
% draw_2_sides_rim(x,y,[],0,0,1);
%%%%%%%%%%%%%%%%%%%%%%
% subplot(1,2,1);plot(x,y,x,y,'.',x(start),y(start),'ro',x(sof),y(sof),'mo');axis equal
% subplot(1,2,2);plot(C,'.');
%%%%%%%%%%%%%%%%%%%%%%  










