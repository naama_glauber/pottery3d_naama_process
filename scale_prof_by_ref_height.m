function scaled_prof = scale_prof_by_ref_height( ref_height, prof )

Ys = prof(:,2);
max_y = max(Ys);
min_y = min(Ys);
height = max_y - min_y;

scaled_prof = zeros(length(Ys), 2);
if height ~= 0
    factor = ref_height/height;
    scaled_prof = prof.*factor;
end

end