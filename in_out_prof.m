function [ xy_out, xy_in, all_max_s, min_s, max_s, min_s_mean_prof, max_s_mean_prof, max_mean_s_idx, max_all_s_idx, diff_in_out] = in_out_prof( all_xy_prof, mean_xy_prof, sample_num_y )
% INPUT:
% @all_x_Prof, @all_y_Prof = contain all cuts of profile, size (num_of_girls x num_of_object x num_of_cuts)
% @X_meanProf, @Y_meanProf = contain mean (full) profile, size (num_of_girls x num_of_object)
% @sample_num_y = the number of points we wants to sample ALL of the profiles' cuts
%
% OUTPUT: This function do all of this:
% 1. split the profile to in and out profiles (@x_out <-> @x_in, @y_out <-> @y_in), size (num_of_girls x num_of_object x num_of_cuts)
% 2. calculate the max and min vals for all cuts object(@min_s, @max_s). and the list of max for each cut (@all_max_s)
% 3. calculate the max and min vals of the mean profile (@min_s_mean_prof, @max_s_mean_prof)
% 4. calculate the different between @in_x and @out_x, both sharing the same y (@diff_in_out)

numOfGirls = length(all_xy_prof);

% initialize output data cells
xy_out = cell(1,numOfGirls);
xy_in = cell(1,numOfGirls);

max_s = cell(1,numOfGirls);
min_s = cell(1,numOfGirls);
all_max_s = cell(1,numOfGirls);
min_s_mean_prof = cell(1,numOfGirls);
max_s_mean_prof = cell(1,numOfGirls);
max_mean_s_idx = cell(1,numOfGirls);
max_all_s_idx = cell(1,numOfGirls);
diff_in_out = cell(1,numOfGirls);

for j=1:numOfGirls
    N = length(all_xy_prof{j});
       
    xy_out{j} = cell(1,N);
    xy_in{j} = cell(1,N);
    
    max_s{j} = cell(1,N);
    min_s{j} = cell(1,N);
    all_max_s{j} = cell(1,N);
    min_s_mean_prof{j} = cell(1,N);
    max_s_mean_prof{j} = cell(1,N);
    max_mean_s_idx{j} = cell(1,N);
    max_all_s_idx{j} = cell(1,N);
    diff_in_out{j} = cell(1,N);
    
    for i = 1:N
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 1. find max_s of MEAN PROFILE.                              %%%
        %%% we get first profile's highest point as a reference point!  %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        diff_in_out{j}{i} = [];
        x = mean_xy_prof{j}{i}(:,1);
        y = mean_xy_prof{j}{i}(:,2);
        [max_s_mean_prof{j}{i}, idx_max_s_mean_prof] = max(y);
        max_mean_s_idx{j}{i} = idx_max_s_mean_prof;
        % take max of mins, to have the shared lowest point
        min_s_mean_prof{j}{i} = max( min(y(end:-1:idx_max_s_mean_prof)), min(y(1:idx_max_s_mean_prof)) );
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 2. find max y value of EACH OBJECT                          %%%
        %%% min among all cuts, same point for inner and outer          %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        xy_s = all_xy_prof{j}{i};
        numOfCuts = length(xy_s); 
        max_all_s_idx{j}{i} = cell(1, numOfCuts);
        [max_for_obj, idx_for_obj1] = max(xy_s{1}(:,2));
        max_all_s_idx{j}{i}{1} = idx_for_obj1;
        
%         figure; plot(all_x_Prof{j}{i}{1}, all_y_Prof{j}{i}{1}, 'c'); hold on;
%         scatter(all_x_Prof{j}{i}{1}(idx_for_obj1), all_y_Prof{j}{i}{1}(idx_for_obj1), 'y*');
        
        all_max_s{j}{i} = max_for_obj;
        idx_for_obj = idx_for_obj1;
        for k = 2:numOfCuts
            y_s = xy_s{k}(:,2);
            [t_max, t_idx] = max(y_s);
            max_all_s_idx{j}{i}{k} = t_idx;
            all_max_s{j}{i} = [all_max_s{j}{i}, t_max];
            if t_max < max_for_obj  % min over the max of different cuts (1 obj)
                max_for_obj = t_max;
            end
            idx_for_obj = [idx_for_obj, t_idx];
%             figure; plot(all_x_Prof{j}{i}{k}, all_y_Prof{j}{i}{k}, 'c'); hold on;
%             scatter(all_x_Prof{j}{i}{k}(t_idx), all_y_Prof{j}{i}{k}(t_idx), 'y*');
        end
        max_s{j}{i} = max_for_obj;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 3. split all profiles' points(x,y) to inner and outer, for each cut. %%%
        %%% Note: for each cut, it is different.                                 %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        xy_out{j}{i} = cell(1, numOfCuts);
        xy_in{j}{i} = cell(1, numOfCuts);
        for k = 1:numOfCuts
           x_s = xy_s{k}(:,1);
           y_s = xy_s{k}(:,2);
           
           xy_out{j}{i}{k} = zeros(idx_for_obj(k),2);
           xy_out{j}{i}{k}(:,1) = x_s(1:idx_for_obj(k));
           xy_out{j}{i}{k}(:,2) = y_s(1:idx_for_obj(k));
           lng = length(x_s(end:-1:idx_for_obj(k)));
           xy_in{j}{i}{k} = zeros(lng,2);
           xy_in{j}{i}{k}(:,1) = x_s(end:-1:idx_for_obj(k));
           xy_in{j}{i}{k}(:,2) = y_s(end:-1:idx_for_obj(k));
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 4. sample mean profile from min to max points. we be done sample_num_y times.   %%%
        %%% sample for inner and outer profiles separently.                                 %%%         
        %%% we find the y steps and using 'linear_interpolatation_x_ginen_y', find x        %%%
        %%% 5. find the 'diff_in_out' on those samples.                                     %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        all_space = abs(max_s_mean_prof{j}{i} - min_s_mean_prof{j}{i});
        for height = min_s_mean_prof{j}{i} : (all_space)/(sample_num_y - 1) : max_s_mean_prof{j}{i}
            out = linear_interpolatation_x_ginen_y(y(1:idx_max_s_mean_prof), x(1:idx_max_s_mean_prof), height);
            in  = linear_interpolatation_x_ginen_y(y(end:-1:idx_max_s_mean_prof), x(end:-1:idx_max_s_mean_prof), height);
            diff_in_out{j}{i} = [diff_in_out{j}{i}, abs(out-in)];
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 6. find min y value of object                                  %%%
        %%% max among all cuts, and max among inner and outer of each cut  %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        min_for_obj = max(min(xy_in{j}{i}{1}(:,2)), min(xy_out{j}{i}{1}(:,2)));
        for k = 2:numOfCuts
            t_min1 = min(xy_in{j}{i}{k}(:,2));
            t_min2 = min(xy_out{j}{i}{k}(:,2));
            t_min = max(t_min1, t_min2);
            if t_min > min_for_obj  % max over the min of different cuts (1 obj)
                min_for_obj = t_min;
            end
        end
        min_s{j}{i} = min_for_obj;
    end
end
end

