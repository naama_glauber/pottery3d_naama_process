function [R_ylen, N_ylen_1, N_ylen_2, N_xdepth, N_xtotdepth, RN_loc, NB_loc, Rright_loc] = length_location_statistics(potters_names, RN_pts, NB_pts, Rtop_pts, Rright_pts, Nleft_pts)
    n_potters = length(potters_names);
    % LENGTH
    % y
    R_ylen = zeros(n_potters, 3);    % 3 for: mean, std, med
    N_ylen_1 = zeros(n_potters, 3);
    N_ylen_2 = zeros(n_potters, 3);
    % x
    N_xdepth = zeros(n_potters, 3);
    N_xtotdepth = zeros(n_potters, 3);
    
    % LOCATION
    RN_loc = zeros(n_potters, 6);    % 6 for: x-mean, x-std, x-med, y-mean, y-std, y-med
    NB_loc = zeros(n_potters, 6);
    Rright_loc = zeros(n_potters, 6);
    
    for j = 1:length(RN_pts)
       n_objects = length(RN_pts{j});
       A = zeros(5, n_objects);
       for i = 1:n_objects
           % X DEPTH and Y LENGTH VALUES
           % y
           A(1,i) = abs(Rtop_pts{j}{i}(2) - RN_pts{j}{i}(2));  % R_ylen
           A(2,i) = abs(RN_pts{j}{i}(2) - Nleft_pts{j}{i}(2)); % N_ylen_1
           A(3,i) = abs(Nleft_pts{j}{i}(2) - NB_pts{j}{i}(2)); % N_ylen_2
           % x
           A(4,i) = abs(RN_pts{j}{i}(1) - Nleft_pts{j}{i}(1)); % N_xdepth
           A(5,i) = abs(Rright_pts{j}{i}(1) - Nleft_pts{j}{i}(1)); % N_xtotdepth
           
           % LOCATION POINTS VALUES
           A(6,i) = RN_pts{j}{i}(1); % RN x
           A(7,i) = RN_pts{j}{i}(2); % RN y
           A(8,i) = NB_pts{j}{i}(1); % NB x
           A(9,i) = NB_pts{j}{i}(2); % NB y
           A(10,i) = Rright_pts{j}{i}(1); % rim's most right point x
           A(11,i) = Rright_pts{j}{i}(2); % rim's most right point y
       end
       % calculate mean. std, med for all the obects of the same potter!
       mean_vec = mean(A,2);
       std_vec = std(A,[],2);
       med_vec = median(A,2);
       
       %%% SORT TO VALUES:
       R_ylen(j,1) = mean_vec(1);
       R_ylen(j,2) = std_vec(1);
       R_ylen(j,3) = med_vec(1);       
       
       N_ylen_1(j,1) = mean_vec(2);
       N_ylen_1(j,2) = std_vec(2);
       N_ylen_1(j,3) = med_vec(2);
       
       N_ylen_2(j,1) = mean_vec(3);
       N_ylen_2(j,2) = std_vec(3);
       N_ylen_2(j,3) = med_vec(3);
       
       N_xdepth(j,1) = mean_vec(4);
       N_xdepth(j,2) = std_vec(4);
       N_xdepth(j,3) = med_vec(4);
       
       N_xtotdepth(j,1) = mean_vec(5);
       N_xtotdepth(j,2) = std_vec(5);
       N_xtotdepth(j,3) = med_vec(5);
       
       RN_loc(j,1) = mean_vec(6);
       RN_loc(j,2) = std_vec(6);
       RN_loc(j,3) = med_vec(6);
       RN_loc(j,4) = mean_vec(7);
       RN_loc(j,5) = std_vec(7);
       RN_loc(j,6) = med_vec(7);
       
       NB_loc(j,1) = mean_vec(8);
       NB_loc(j,2) = std_vec(8);
       NB_loc(j,3) = med_vec(8);
       NB_loc(j,4) = mean_vec(9);
       NB_loc(j,5) = std_vec(9);
       NB_loc(j,6) = med_vec(9);
       
       Rright_loc(j,1) = mean_vec(10);
       Rright_loc(j,2) = std_vec(10);
       Rright_loc(j,3) = med_vec(10);
       Rright_loc(j,4) = mean_vec(11);
       Rright_loc(j,5) = std_vec(11);
       Rright_loc(j,6) = med_vec(11);       
    end
    
    %'R_ylen'
    %[min_std, idx] = min(R_ylen(:,2))
    
    %'N_ylen_1'
    %[min_std, idx] = min(N_ylen_1(:,2))
    
    %'N_ylen_2'
    %[min_std, idx] = min(N_ylen_2(:,2))
    
    %'N_xdepth'
    %[min_std, idx] = min(N_xdepth(:,2))
    
    %'N_xtotdepth'
    %[min_std, idx] = min(N_xtotdepth(:,2))
    
    %'RN_loc'
    %[min_std, idx] = min(RN_loc(:,2))
    
    %'NB_loc'
    %[min_std, idx] = min(NB_loc(:,2))
    
    %'Rright_loc'
    %[min_std, idx] = min(Rright_loc(:,2))
    
end