function success = plot_profiles_histograms(all_x_Prof, all_y_Prof, X_meanProf, Y_meanProf, pt_inner, pt_outer, all_inner_hist, all_outer_hist, min_s, max_s, sample_num_y, girlsNames)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
success = 0;

numOfGirls = length(all_inner_hist);
numOfCuts = length(all_x_Prof{1}{1});

max_for_girl = zeros(1,numOfGirls);
min_for_girl = zeros(1,numOfGirls);

for j=1:numOfGirls
    
    max_for_girl(j) = max_s{1}{1};
    min_for_girl(j) = min_s{1}{1};
    
    N = length(all_inner_hist{j});
    for i = 1:N
        if max_s{j}{i} < max_for_girl(j)
             max_for_girl(j) = max_s{j}{i};
        end
        if min_s{j}{i} > min_for_girl(j)
             min_for_girl(j) = min_s{j}{i};
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%% UI SPLIT WINDOW %%%%%%%%%%%%%%%%%%%%%%%%%%
        prof_fig = figure('units','normalized','outerposition',[0 0 1 1]);
        hold on;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%% PROFILE PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%
        subplot(1,3,1)
        str = strcat(girlsNames{j}, '-', num2str(i));
        all_y = min_s{j}{i} + [0:1/sample_num_y:1]*max_s{j}{i};
        all_y = all_y(1:sample_num_y)';
        for cutIdx = 1:numOfCuts         
           plot (all_x_Prof{j}{i}{cutIdx}, all_y_Prof{j}{i}{cutIdx},'b');
           hold on;
           
%           x = [min(pt_inner{j}{i}{cutIdx}(:,1)), max(pt_inner{j}{i}{cutIdx}(:,1))];
%           scatter(pt_inner{j}{i}{cutIdx}(:,1), pt_inner{j}{i}{cutIdx}(:,2), 'k.');
%           scatter(pt_outer{j}{i}{cutIdx}(:,1), pt_outer{j}{i}{cutIdx}(:,2), 'k.');
%           for y_idx = 1:sample_num_y
%                plot(x), all_y(y_idx)*ones(size(x))
%           end
        end
        scatter(X_meanProf{j}{i}, Y_meanProf{j}{i}, 'r.'); % plot mean profile:
        title(strcat('prof: ', str));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%% HISTOGRAM %%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%% IN:
        subplot(1,3,2)       % add second plot in 2 x 1 grid
        histogram(all_inner_hist{j}{i}(:,1), sample_num_y/10, 'normalization', 'probability');
        % Convert y-axis values to percentage values by multiplication
        norm_x = get(gca,'xtick')'/max(get(gca,'xtick'));
        height_x = min_s{j}{i} + (norm_x * (max_s{j}{i} - min_s{j}{i}));
        %height_x = min_global + (norm_x * (max_s{j}{i} - min_global));
        a=cellstr(num2str(round(height_x)));
        % 'Reflect the changes on the plot
        pct = char(ones(size(a,1),1)*'[cm]'); 
        % Append the '%' signs after the percentage values
        new_xticks = [char(a),pct];
        set(gca,'xticklabel',new_xticks);
        
        pct_y = get(gca,'ytick')'*100;
        b=cellstr(num2str(pct_y));
        % 'Reflect the changes on the plot
        pct = char(ones(size(b,1),1)*'%'); 
        % Append the '%' signs after the percentage values
        new_yticks = [char(b),pct];
        set(gca,'yticklabel',new_yticks);
        title(strcat('hist In: ',str))
        
        %%%% OUT:
        subplot(1,3,3)       % add second plot in 2 x 1 grid
        histogram(all_outer_hist{j}{i}(:,1), sample_num_y/10, 'normalization', 'probability');
        % Convert y-axis values to percentage values by multiplication
        norm_x = get(gca,'xtick')'/max(get(gca,'xtick'));
        height_x = min_s{j}{i} + (norm_x * (max_s{j}{i} - min_s{j}{i}));
        %height_x = min_global + (norm_x * (max_s{j}{i} - min_global));
        a=cellstr(num2str(round(height_x)));
        % 'Reflect the changes on the plot
        pct = char(ones(size(a,1),1)*'[cm]'); 
        % Append the '%' signs after the percentage values
        new_xticks = [char(a),pct];
        set(gca,'xticklabel',new_xticks);
        
        pct_y = get(gca,'ytick')'*100;
        b=cellstr(num2str(pct_y));
        % 'Reflect the changes on the plot
        pct = char(ones(size(b,1),1)*'%'); 
        % Append the '%' signs after the percentage values
        new_yticks = [char(b),pct];
        set(gca,'yticklabel',new_yticks);
        title(strcat('hist Out: ',str))
        
        %%%%%%%%%%%%%%% different plot for each object %%%%%%%%%%%%%%%%%
        saveas(gcf,['hist-prof-', str, '.png']);
        hold off;
        close(prof_fig)
    end
end

fig = figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
for j=1:numOfGirls
    str = girlsNames{j};
    % IN:
    subplot(2,5,1,j);
    histogram(all_inner_hist{j}{:}(:,1), sample_num_y/10, 'normalization', 'probability');
    norm_x = get(gca,'xtick')'/max(get(gca,'xtick'));
    height_x = min_for_girl{j} + (norm_x * (max_for_girl(j) - min_for_girl{j}));
    %height_x = min_global + (norm_x * (max_for_girl(j) - min_global));
    a=cellstr(num2str(round(height_x)));
    % 'Reflect the changes on the plot
    pct = char(ones(size(a,1),1)*'[cm]'); 
    % Append the '%' signs after the percentage values
    new_xticks = [char(a),pct];
    set(gca,'xticklabel',new_xticks);
    title(strcat('hist Out: ',str))
    
    % OUT
    subplot(2,5,2,j)
    histogram(all_outer_hist{j}{:}(:,1), sample_num_y/10, 'normalization', 'probability');
    norm_x = get(gca,'xtick')'/max(get(gca,'xtick'));
    height_x = min_for_girl{j} + (norm_x * (max_for_girl(j) - min_for_girl{j}));
    %height_x = min_global + (norm_x * (max_for_girl(j) - min_global));
    a=cellstr(num2str(round(height_x)));
    % 'Reflect the changes on the plot
    pct = char(ones(size(a,1),1)*'[cm]'); 
    % Append the '%' signs after the percentage values
    new_xticks = [char(a),pct];
    set(gca,'xticklabel',new_xticks);
    title(strcat('hist Out: ',str))
end
saveas(gcf,[str 'final_histograms.png']);
hold off;
close(prof_fig)
    
success = 1;
end

