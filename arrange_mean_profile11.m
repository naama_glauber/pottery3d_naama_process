function [x,y,qual]=arrange_mean_profile11(v,n,min_step,max_step)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function should be a combination of the two series of functions:
% 1)draw_mean_profile 
% 2)arrange_profile

% Within this set of functions from this version on, there is a try to have
% a quality factor of the shard alignment. This quality is measured in
% terms of the distribution of the points within each rectangle, which
% means the spread of points along the normal of each point.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isvector(min_step)
    min_step=min_step;
else
    min_step=0.5;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ignoring irrelevant points for which the normals are far from the z-axis
L=length(v(:,1));
ax=[zeros(L,2) ones(L,1)];
q1=cross(n,ax);
q2=sum(-v'.*q1');
D=abs(q2)./norm(q1);
% if prctile(D,70)<0.05
%     D_par=0.05;
% else
%     D_par=prctile(D,40);
% end
% D_par=prctile(D,60);
D_par=0.07;
D_I=find(D<D_par);
original_index=1:length(v);
original_index=original_index(D_I);
%%%%%%%%%%%%%%%%%%%%%%
X=sqrt(v(D_I,1).^2+v(D_I,2).^2);
Y=v(D_I,3);
% X2=sqrt(v(D_I2,1).^2+v(D_I2,2).^2);
% Y2=v(D_I2,3);
NY=n(D_I,3);
NX=sqrt(n(D_I,1).^2+n(D_I,2).^2);

XI=[];
for i=1:length(NX)
    if norm(v(D_I(i),1:2)+n(D_I(i),1:2))<norm(v(D_I(i),1:2))
        NX(i)=-NX(i);
        XI(end+1)=i;
    end
end
% plot(X,Y,'k.');axis equal;hold on
line=zeros(2,length(X));
%%%%%%%%%%%%%%%%%%%%%%
% defind the first (highest) point
x_profile=X;
y_profile=Y;
L=length(x_profile);
[ysort,I]=sort(y_profile);
% D=sqrt((x_profile(I(1:end-1))-x_profile(I(end))).^2+(y_profile(I(1:end-1))-y_profile(I(end))).^2);
NNx=0;
NNy=1;
Sx=x_profile(I(end));
Sy=y_profile(I(end));
len=3;wid=2*min_step;
malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
Idel=find(INOUT);
last_norm_right=[mean(NX(Idel));mean(NY(Idel))];
last_norm_right=last_norm_right/norm(last_norm_right);
NNx=last_norm_right(1);
NNy=last_norm_right(2);
Sx=mean(x_profile(Idel));
Sy=mean(y_profile(Idel));
len=2;wid=2*min_step;
last_wid_right=wid;
last_wid_left=wid;
malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
Idel=find(INOUT);
mid_index=fix(L/2);
line(1,mid_index)=mean(x_profile(Idel));
line(2,mid_index)=mean(y_profile(Idel));
% plot(malben_x,malben_y,'k');
% alpha(0.5);
% plot([Sx Sx+3*NNx],[Sy Sy+3*NNy]);
%%%%%%%%%%%%%%%%%%
% The quality of the first rectangle
qual(1,mid_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*last_norm_right);
% qual(2,mid_index)=2*wid;
%%%%%%%%%%%%%%%%%%
% keep the original index of the points for each new point
OI(mid_index).I=original_index(Idel);
%%%%%%%%%%%%%%%%%%
x_profile(Idel)=[];
y_profile(Idel)=[];
NX(Idel)=[];
NY(Idel)=[];
original_index(Idel)=[];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% searching for the following points
right_index=mid_index;
left_index=mid_index;
last_norm_right=[NNx;NNy];
last_norm_left=[NNx;NNy];
right_end=0;left_end=0;
while L>0
    if right_end==0
        right_direction=[last_norm_right(2);-last_norm_right(1)];
        NNx=last_norm_right(1);
        NNy=last_norm_right(2);
        Sx=line(1,right_index)+last_wid_right*right_direction(1);
        Sy=line(2,right_index)+last_wid_right*right_direction(2);
        DO=[];N=[];SX=[];SY=[];IDEL=[];in_right=[];
        wid=max_step;
        while min_step<wid
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                new_norm_right=[mean(NX(Idel));mean(NY(Idel))];
                new_norm_right=new_norm_right/norm(new_norm_right);
                Q=[];
                for i=1:length(Idel)
                    Q(i)=dot(new_norm_right,[NX(Idel(i));NY(Idel(i))])/norm([NX(Idel(i));NY(Idel(i))]);
                end
                IIdel=find(Q>0.9);
                IDEL.Idel=Idel';
                if length(IIdel)>0
                    IIdel=IIdel;
                else
                    IIdel=find(Q>prctile(Q,60));
                end
                new_norm_right=[mean(NX(Idel(IIdel)));mean(NY(Idel(IIdel)))];
                new_norm_right=new_norm_right/norm(new_norm_right);
                DO=dot(last_norm_right,new_norm_right);
                N=new_norm_right;
                SX=mean(x_profile(Idel(IIdel)));
                SY=mean(y_profile(Idel(IIdel)));
                if DO>0.99
                    in_right=wid;
                    break
                else
                    in_right=wid;
                    wid=wid-0.1;
                end
            else
                wid=wid-0.1;
                if length(in_right)>0
                    DO=0;
                else
                    DO=-1;
                end
            end
        end
        if DO>-1
            if wid<min_step
                wid=min_step;
            end
            last_wid_right=wid;
            Sx=SX;
            Sy=SY;
            NNx=N(1);
            NNy=N(2);
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                Idel=Idel;
            else
                Idel=union(Idel,IDEL.Idel);
            end
%             plot(malben_x,malben_y,'r');
%             alpha(0.5);
%             plot([Sx Sx+3*NNx],[Sy Sy+3*NNy])
            right_index=right_index-1;
            line(1,right_index)=Sx;
            line(2,right_index)=Sy;
            OI(right_index).I=original_index(Idel);
            %%%%%%%%%%%%%%%%%%
            % The quality of the current rectangle
            qual(1,right_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*new_norm_right);
            perp=[-new_norm_right(2);new_norm_right(1)];
            %%%%%%%%%%%%%%%%%%
            x_profile(Idel)=[];
            y_profile(Idel)=[];
            NX(Idel)=[];
            NY(Idel)=[];
            original_index(Idel)=[];
            last_norm_right=new_norm_right;
        else
            right_end=1;
        end
    end

    %%%%%%%%%%%%%%%%%
    if left_end==0
        left_direction=[-last_norm_left(2);last_norm_left(1)];
        NNx=last_norm_left(1);
        NNy=last_norm_left(2);
        Sx=line(1,left_index)+last_wid_left*left_direction(1);
        Sy=line(2,left_index)+last_wid_left*left_direction(2);
        DO=[];N=[];SX=[];SY=[];IDEL=[];in_left=[];
        wid=max_step;
        while min_step<wid
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                new_norm_left=[mean(NX(Idel));mean(NY(Idel))];
                new_norm_left=new_norm_left/norm(new_norm_left);
                Q=[];
                for i=1:length(Idel)
                    Q(i)=dot(new_norm_left,[NX(Idel(i));NY(Idel(i))])/norm([NX(Idel(i));NY(Idel(i))]);
                end
                IIdel=find(Q>0.9);
                IDEL.Idel=Idel';
                if length(IIdel)>0
                    IIdel=IIdel;
                else
                    IIdel=find(Q>prctile(Q,60));
                end
                new_norm_left=[mean(NX(Idel(IIdel)));mean(NY(Idel(IIdel)))];
                new_norm_left=new_norm_left/norm(new_norm_left);
                DO=dot(last_norm_left,new_norm_left);
                N=new_norm_left;
                SX=mean(x_profile(Idel(IIdel)));
                SY=mean(y_profile(Idel(IIdel)));
                if DO>0.99
                    in_left=wid;
                    break
                else
                    in_left=wid;
                    wid=wid-0.1;
                end
            else
                wid=wid-0.1;
                if length(in_left)>0
                    DO=0;
                else
                    DO=-1;
                end
            end
        end
        if DO>-1
            if wid<min_step
                wid=min_step;
            end
            last_wid_left=wid;
            Sx=SX;
            Sy=SY;
            NNx=N(1);
            NNy=N(2);
            malben_x=[Sx+len*NNx+wid*NNy,Sx+len*NNx-wid*NNy,Sx-len*NNx-wid*NNy,Sx-len*NNx+wid*NNy,Sx+len*NNx+wid*NNy];
            malben_y=[Sy+len*NNy-wid*NNx,Sy+len*NNy+wid*NNx,Sy-len*NNy+wid*NNx,Sy-len*NNy-wid*NNx,Sy+len*NNy-wid*NNx];
            INOUT=inpolygon(x_profile,y_profile,malben_x,malben_y);
            Idel=find(INOUT);
            if length(Idel)>0
                Idel=Idel;
            else
                Idel=union(Idel,IDEL.Idel);
            end
%             plot(malben_x,malben_y,'k');
%             alpha(0.5);
%             plot([Sx Sx+3*NNx],[Sy Sy+3*NNy])
            left_index=left_index+1;
            line(1,left_index)=Sx;
            line(2,left_index)=Sy;
            OI(left_index).I=original_index(Idel);
            %%%%%%%%%%%%%%%%%%
            % The quality of the current rectangle
            qual(1,left_index)=std([x_profile(Idel)-Sx y_profile(Idel)-Sy]*new_norm_left);
            perp=[-new_norm_left(2);new_norm_left(1)];
            %%%%%%%%%%%%%%%%%%
            x_profile(Idel)=[];
            y_profile(Idel)=[];
            NX(Idel)=[];
            NY(Idel)=[];
            original_index(Idel)=[];
            last_norm_left=new_norm_left;
        else
            left_end=1;
        end
    end
    y1=line(2,left_index);
    y2=line(2,right_index);
    Idel=find(y_profile>max(y1,y2)+5);
    x_profile(Idel)=[];
    y_profile(Idel)=[];
    NX(Idel)=[];
    NY(Idel)=[];
    original_index(Idel)=[];
    if right_end==1 & left_end==1
        L=0;
    else
        L=length(x_profile);
    end
end
I=find(line(1,:)>0);
x=line(1,I);
y=line(2,I);
qual=qual(:,I);
OI=OI(I);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% adding artificial points in case that the two ends of the profile are far
% from each other
% end_dist=sqrt((x(1)-x(end))^2+(y(1)-y(end))^2);
% add_num=fix(end_dist/max_step)+1;
% add_flag=0;
% if add_num>1
%     x_add=x(end):(x(1)-x(end))/add_num:x(1);
%     y_add=y(end):(y(1)-y(end))/add_num:y(1);
%     x=[x x_add];
%     y=[y y_add];
%     x(end)=[];
%     y(end)=[];
%     add_num=length(x_add)-1;
%     add_flag=1;
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sx=smooth_fourier(x,fix(length(x)/12),0.5);
% sy=smooth_fourier(y,fix(length(y)/12),0.5);
% s=New_arc_length(sx,sy);
% sx=N_index(sx,s,length(sx)-1);
% sy=N_index(sy,s,length(sy)-1);
% sx=smooth_fourier(sx,fix(length(sx)/12),0.5);
% sy=smooth_fourier(sy,fix(length(sy)/12),0.5);
% s=New_arc_length(sx,sy);
% k=akmumiyut_fourier(sx,sy);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Deleting the artifical points
% if add_flag==1
%     k(end-add_num+1:end)=[];
%     s(end-add_num+1:end)=[];
%     sx(end-add_num+1:end)=[];
%     sy(end-add_num+1:end)=[];
%     x(end-add_num+1:end)=[];
%     y(end-add_num+1:end)=[];
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [z1,z2]=max(sy);
% s=s-s(z2);
% dend=sqrt((sx(1)-sx(end))^2+(sy(1)-sy(end))^2);
% s1(1:z2-1)=s(end)+dend-s(1:z2-1);
% s1(z2+1:length(s))=s(z2+1:end)-s(end)-dend+s(1);
% ss=s;
% I=find(abs(s)>abs(s1));
% ss(I)=s1(I);
% K=imregionalmax(k).*k;
% if k(1)>k(end)
%     K(end)=0;
% end
% if k(end)>k(1)
%     K(1)=0;
% end
% TH=thickness2(sx,sy);
% I1=find([TH.thickness]<prctile([TH.thickness],75) & [TH.thickness]>prctile([TH.thickness],15));
% I2=find([TH.distance]<prctile([TH.distance],75));
% I=intersect(I1,I2);
% TH=mean([TH.thickness(I)]);
% qual=100*qual./TH;
% I=find(abs(ss)<max(abs(ss))-2.5*TH);
% K(I)=zeros(1,length(I));
% % [z1,z2]=sort(K);disp(z1(end-1:end));
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for i=1:length(OI)
%     W=OI(i).I;
%     try
%         a2(i)=mean(D(W));
%     catch
%         a2(i)=NaN;
%     end
% end
% a2=a2/std(a2);
% A1=find(a2.*K>min_step+0.25);
% 
% % plot(a2,'.')
% % plot(a2.*k,'.');figure
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % plot(x(A1),y(A1),'r.');axis equal
% if length(A1)>0
%     mid=round(length(x)/2);
%     if min(A1)>mid
%         if max(A1)<length(x)
%             x=[x(max(A1)+1:end) x(1:min(A1)-1)];
%             y=[y(max(A1)+1:end) y(1:min(A1)-1)];
%             qual=[qual(:,max(A1)+1:end) qual(:,1:min(A1)-1)];
%         else
%             x=x(1:min(A1)-1);
%             y=y(1:min(A1)-1);
%             qual=qual(:,1:min(A1)-1);
%         end
%     else
%         if max(A1)<mid
%             if min(A1)>1
%                 x=[x(max(A1)+1:end) x(1:min(A1)-1)];
%                 y=[y(max(A1)+1:end) y(1:min(A1)-1)];
%                 qual=[qual(:,max(A1)+1:end) qual(:,1:min(A1)-1)];
%             else
%                 x=x(max(A1)+1:end);
%                 y=y(max(A1)+1:end);
%                 qual=qual(:,max(A1)+1:end);
%             end
%         else
%             A11=max(A1(find(A1<mid)));
%             A12=min(A1(find(A1>mid)));
%             x=x(A11+1:A12-1);
%             y=y(A11+1:A12-1);
%             qual=qual(:,A11+1:A12-1);
%         end
%     end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% hold on
% plot(x,y,x,y,'.');axis equal