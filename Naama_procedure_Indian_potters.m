 %%%
% indian potters procedure:
is_indians = true;
dir_path = 
% 1. READ DATA
[all_profs, potters_names, potter_filenames] = read_matprofiles_data(is_indians);
if is_indians
    all_indian_profs = all_profs;
    indian_names = potters_names;
    y_threshold = -60; % TOCHECK: CAN I DO IT AUTOMATICALLY
else % BEZALEL
    all_bez_profs = all_profs;
    bez_names = potters_names;
    y_threshold = -5; % TOCHECK: CAN I DO IT AUTOMATICALLY
end

% 2.1 CUT THE LEFT SIDE TO HAVE ONLY THE OUSIDE PROFILE
% 2.2 CUT THE BOTTOM OF THE PROFILE (just a bit lower from the neck)
% to check where to cut, first we plot all profiles of the same potters
n_potters = length(potters_names);
for j=1:n_potters
    figure; hold on;
    n_objects = length(all_profs{j});
    for i=1:n_objects
        plot(all_profs{j}{i}(:,1),all_profs{j}{i}(:,2))
    end
end
trim_profs = cut_trim_profiles(all_profs, potters_names, y_threshold);

% 3. RESAMPLE CURVE - EDGES NOT WORKING WELL ???
filter_size = 9; % Gaussian window
desired_size = 99;
is_ploting = false;
sampled_prof = resample_profiles(trim_profs, potters_names, desired_size, filter_size, is_ploting);

% 3. TO IMPROVE: CHECK MORE ABOUT THE CURVTURE for each profile!!
% Based on code "LineNormals2D" and "LineCurvature2D" from here:
% https://www.mathworks.com/matlabcentral/fileexchange/32696-2d-line-curvature-and-normals
% TODO: change this range to be automatically???
% range_PEMARAM = -20; %[-33, -20];
% range_RUDAKALY = -22; %[-35,-22];
% cutrim_y_ranges = [range_PEMARAM; range_RUDAKALY];
% is_ploting = true;
% [RN_pts,NB_pts] = calc_plot_curv_norm(trim_profs, sampled_prof, cutrim_y_ranges,is_ploting); %sampled_all_mean_prof

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 4.ALIGN ALL PROFILES USING ICP ALGORITHM
is_ploting = false;
if is_indians
    dir_path = '.\indian_potters_files\results\icp\no_rot_trim_groups\';
    [all_Dicp, all_Ricp, all_Ticp, all_ERs] = ICP_align_profs(all_profs, potters_names, is_ploting, dir_path);
    [trimed_Dicp, trimed_Ricp, trimed_Ticp, trimed_ERs] = ICP_align_profs(trim_profs, potters_names, is_ploting, dir_path);
else
    dir_path = '.\BEZ\';
    if ~exist('ref_prof', 'var')
        load('.\mat_files\ref_profile.mat');
        ref_prof = mean_xy_prof_ref;
        clear 'mean_xy_prof_ref'
    end
    [all_Dicp, all_Ricp, all_Ticp, all_ERs] = ICP_align_profs(all_profs, potters_names, is_indians, is_ploting, dir_path, ref_prof);
    [trimed_Dicp, trimed_Ricp, trimed_Ticp, trimed_ERs_icp] = ICP_align_profs(trim_profs, potters_names, is_indians, is_ploting, dir_path, ref_prof);
end

%4. RESAMPLE, CALCULATE STATISTICS ABOUT THE DIVISION POINTS and RANGES!
trm_icp_smp9_prof = resample_profiles(trimed_Dicp, potters_names, desired_size, filter_size, is_ploting);
trm_noicp_smp9_prof = resample_profiles(trim_profs, potters_names, desired_size, filter_size, false);

is_ploting = true;
cutrim_y_ranges = [-20,-25,-20,-20,-20,-20,-30,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20,-20];

[trm_icp_RN_pts,trm_icp_NB_pts, trm_icp_Rtop_pts, trm_icp_Rright_pts, trm_icp_Nleft_pts] = calc_plot_curv_norm(trimed_Dicp, trm_icp_smp9_prof, potters_names, cutrim_y_ranges, is_ploting);
[R_ylen, N_ylen_1, N_ylen_2, N_xdepth, N_xtotdepth, RN_loc, NB_loc, Rright_loc] = length_location_statistics(potters_names, trm_icp_RN_pts,trm_icp_NB_pts, trm_icp_Rtop_pts, trm_icp_Rright_pts, trm_icp_Nleft_pts);

[trm_noicp_RN_pts,trm_noicp_NB_pts, trm_noicp_Rtop_pts, trm_noicp_Rright_pts, trm_noicp_Nleft_pts] = calc_plot_curv_norm(trim_profs, trm_noicp_smp9_prof, potters_names, cutrim_y_ranges, false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GROUPS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5. For each section: R and N: to calculate the normals of the ref. than cut
% all of the other profile and find for each such cut the points that are
% the most far from each other. same concept as the in-out procedure I made

% for the Bez Girls in: look at: in_out_pts_cuts_prof2.m , but implementation
% is in: out_normals_diffs.m

% TODO:
% 2. Does minimum radius helps?
%
% 3. search by iteration, from rough to better resolution
%
% 4. how to create loft in matlab:
% 
%

% DRAWING NORMALS OF PROFILE POINTS..
X = all_profs{1}{1}(:,1);
Y = all_profs{1}{1}(:,2);
all_norms = LineNormals2D([X,Y]);
pt_norms = [X+all_norms(:,1), Y+all_norms(:,2)];
plot([X, pt_norms(:,1)], [Y, pt_norms(:,2)]); hold on;
plot([X, pt_norms(:,1)]', [Y, pt_norms(:,2)]', 'r');

% TRIM PROFILE BY PEAK (IN USE???)
y = all_norms(:,1);
[peaks, locs] = findpeaks(x, y,'MinPeakProminence',100);
[minval, minidx] = min( x(locs(1):locs(2)) );
%minidx is relative to the subset of values, so move it back to full range
minidx = minidx + locs(1) - 1;

X = trim_profs{1}{1}(1,:);
Y = trim_profs{1}{1}(2,:);
[peaks, locs] = findpeaks(-X);
figure; hold on;
plot(X);
plot(locs, -peaks, 'r*');

figure; hold on;
plot(all_profs{1}{1}(1,:), all_profs{1}{1}(2,:), 'b')
plot(X,Y, 'k');
plot(X(locs),Y(locs),'g*');

%%%%%%%%%%%%%%%%%%%%%%%%  BLOCK: MUSLIM, HINDU  %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 1. Put all Indians and all Muslims in two groups!!
group_names = {'Muslim', 'Hindu'};
potters_names = {'AKSAR', 'AMIN', 'ANWAR KHAN', 'AZRUDDIN', 'DEVILAL', 'FAZRUDIN', 'GANI', 'GOVINDRAM', 'HAJIRAM', 'JERA RAM', 'LASHMAN', 'MANGILAL', 'MAZID', 'PEMARAM', 'RAMDAYAL', 'RAMZAN', 'RAZAK', 'SAKUR', 'SAMIR', 'SOKAT', 'UDARAM', 'YOUSSOUF', 'ZORILAL'};
muslim_names = {'ANWAR KHAN', 'RAMZAN', 'SAKUR', 'SOKAT', 'AKSAR', 'RAZAK', 'SAMIR', 'YOUSSOUF', 'MAZID', 'FAZRUDIN', 'GANI', 'AMIN', 'RAMDAYAL', 'AZRUDDIN'};
hindu_names = {'PEMARAM', 'MANGILAL', 'JERA RAM', 'GOVINDRAM', 'HAJIRAM', 'UDARAM', 'DEVILAL', 'LASHMAN', 'ZORILAL'};
muslim_idxs = [2,15,17,19,0,16,18,21,12,5,6,1,14,3];
hindu_idxs = [13,11,9,7,8,20,4,10,22];

group_profs = cell(1,2);
profile_names = cell(1,2);
% Muslim
counter = 0;
group_fullnames{1} = [];
for i=1:length(muslim_idxs)
    idx = muslim_idxs(i)+1; % starts from one, not 0
    potter_i_profs = trim_profs{idx};
    for j=1:length(potter_i_profs)
        group_profs{1}{j+counter} = potter_i_profs{j}; % muslim, idx = 1
        profile_names{1} = [profile_names{1}, group_names{1} + "-" + potters_names{idx} + "-" + num2str(j)];
    end
    counter = counter + length(potter_i_profs);
end

% Hindu
counter = 0;
for i=1:length(hindu_idxs)
    idx = hindu_idxs(i)+1; % starts from one, not 0
    potter_i_profs = trim_profs{idx};
    for j=1:length(potter_i_profs)
        group_profs{2}{j+counter} = potter_i_profs{j}; % hindu, idx = 1
        profile_names{2} = [profile_names{2} group_names{2} + "-" + potters_names{idx} + "-" + num2str(j)];
    end
    counter = counter + length(potter_i_profs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% 2. Align each group by ICP (always compare to ref)
ref_profiles{1} = group_profs{1}{1};
ref_profiles{2} = group_profs{2}{60};
[groups_icp_profs ,groups_Ricp, groups_Ticp, groups_ERs] = ICP_align_profs(group_profs, group_names, profile_names, false, ref_profiles);

% to see ref prof:
for i=1:length(group_profs)
    figure; hold on;
    title(group_names{i})    
    for j=2:length(group_profs{i})
        X_prof_j = group_profs{i}{j}(:,1);
        Y_prof_j = group_profs{i}{j}(:,2);
        plot(X_prof_j, Y_prof_j, 'b')
    end
    X_prof_j = ref_profiles{i}(:,1);
    Y_prof_j = ref_profiles{i}(:,2);
    plot(X_prof_j, Y_prof_j, 'r')
end

% also applyied ICP without rotation! groups_Ricp{j}{j} = eye(3,3), always!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.Order by error for each group
% Order profiles by the ICP score (1. MIX however, 2.do not mix between villages, or between potters - like blocks)
% CAN BE MADE IN RHINO MORE EASILY!!!
allErr = cell(1,2);
for j=1:length(groups_ERs)
    avgErr = 0.0;
    counter = 0;
    for i=1:length(groups_ERs{j})
        err = groups_ERs{j}{i}(16);
        allErr{j} = [allErr{j}; [i, err, profile_names{j}(i)]];
    end
end

%save('WS_240919_GROUPS.mat', 'allErr', 'group_avgErr, 'group_names', 'groups_noR_trim_icp_profs', 'groups_icp_full_profs', 'groups_trim_noR_ERs' ,'groups_full_Ricp', 'groups_full_Ticp', 'groups_full_ERs', 'groups_icp_profs' ,'groups_Ricp', 'groups_Ticp', 'groups_ERs', 'group_profs', 'group_full_profs', 'ref_profiles', 'ref_full_profiles', 'profile_names', 'hindu_names', 'muslim_names', 'hindu_idxs', 'muslim_idxs')

% 5. Export to Rhino:
%    4.1 calculate for each group its statistics
%    4.2 create lofts!!!



